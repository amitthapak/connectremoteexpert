//
//  Expertise.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/24/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// Model for user's expertise info.
struct Expertise: Codable, Hashable {
    
    /// the related object's ID, e.g. categories ID (depend on `type`)
    let objectId: String
    /// the title of the related object
    let title: String
    /// the type of the related object
    let type: AssetType
    /// total assets number for given object
    var assetsCount: Int = -1 // -1 means not yet calculated
    /// total procedures number for given object
    var proceduresCount: Int = -1
    
    /// the subtitle
    var subtitle: String {
        let proceduresStr = "\(proceduresCount) " + (proceduresCount == 1 ? NSLocalizedString("procedure", comment: "procedure") : NSLocalizedString("procedures", comment: "procedures"))
        if type == .model {
            return proceduresStr
        }
        else {
            let assetsStr = "\(assetsCount) " + (assetsCount == 1 ? NSLocalizedString("asset", comment: "asset") : NSLocalizedString("assets", comment: "assets"))
            return "\(assetsStr) - \(proceduresStr)"
        }
    }
    
    /// Create object from category
    /// - Parameter category: the category
    static func from(category item: Category) -> Expertise {
        return Expertise(objectId: item.id, title: item.name, type: .category, assetsCount: 0, proceduresCount: 0)
    }
    
    /// Create object from type
    /// - Parameter type: the type
    static func from(type item: Type) -> Expertise {
        return Expertise(objectId: item.id, title: item.name, type: .type, assetsCount: 0, proceduresCount: 0)
    }
    
    /// Create object from model
    /// - Parameter model: the model
    static func from(model item: Model) -> Expertise {
        return Expertise(objectId: item.id, title: item.name, type: .model, assetsCount: 0, proceduresCount: 0)
    }
    
    func toPut(procedures: [Procedure]? = nil) -> ExpertisePut {
        return ExpertisePut(assetId: objectId, procedureIds: procedures?.map({$0.id}))
    }
    
    /// Hash object
    func hash(into hasher: inout Hasher) {
        hasher.combine(objectId)
        hasher.combine(type)
    }
    
}

struct ExpertisePut: Codable {
    
    /// the related object's ID, e.g. categories ID (depend on `type`)
    let assetId: String
    var procedureIds: [String]?
    
    func toDic() -> [String: Any] {
        var dic: [String: Any] =  ["assetId": assetId]
        if let list = procedureIds {
            dic["procedureIds"] = list
        }
        return dic
    }
}
