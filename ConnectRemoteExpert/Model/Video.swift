//
//  Video.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SwiftEx

/// Video model
struct Video: Codable {
    
    /// the fields
    let id: String
    var name: String
    let url: String
    let date: String
    
    let duration: Float?
    let thumbnailUrl: String!
    let associatedAssetId: String!
    
    /// date and duration string
    var subtitle: String {
        let dateStr = Date.defaultFormatter.string(from: Date.defaultParser.date(from: date) ?? Date())
        
        var durationString = ""
        if let duration = duration {
            let h = Int(duration / 60).description.addZeros(targetLength: 2)
            let minutes = Int(duration) - Int(duration / 60) * 60
            let m = minutes.description.addZeros(targetLength: 2)
            durationString = "\(h):\(m)"
        }
        return "\(dateStr) - \(durationString)"
    }
}

/// Helpful extension
extension Video {
    
    /// Check if document matches the string
    /// - Parameter searchString: the search string
    func contains(string searchString: String) -> Bool {
        return name.lowercased().contains(searchString)
    }
}
