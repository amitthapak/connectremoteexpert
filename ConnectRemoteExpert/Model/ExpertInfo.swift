//
//  ExpertInfo.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 18.06.2020.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

struct ExpertInfo: Codable {
    
    var avgRating: Float?
    var expertise: [ExpertiseInfo]
    var helpedAllTimeCount: Int
    var helpedTodayCount: Int
    var id: String
    var image: String
    var name: String
    var ratingCount: Int
    var title: String
    var expertAvailability: Bool
}

struct ExpertInfoExtended: Codable {
    
    var avgRating: Float?
    var expertise: [Expertise]
    var helpedAllTimeCount: Int
    var helpedTodayCount: Int
    var id: String
    var image: String
    var name: String
    var ratingCount: Int
    var title: String
    var expertAvailability: Bool
    
    /// the list of selected procedures for models
    var procedures: [String: [String]]
    
    func toUserInfo() -> UserInfo {
        return UserInfo(rating: avgRating ?? 0, numberOfVotes: ratingCount, inboxToday: helpedTodayCount, inboxAll: helpedAllTimeCount)
    }
    
    func toExpert() -> Expert {
        return Expert(id: id, name: name, email: "", expertTitle: title, expertAvailability: expertAvailability, expertStatus: nil, userId: "")
    }
}

struct ExpertiseInfo: Codable {
    
    var id: String // procedure ID
    var name: String
    var assetId: String
    
    static func toExpertise(list: [ExpertiseInfo], models: [String: Model]) -> ([Expertise], [String: [String]]) {
        let map = list.hasharrayWithKey({$0.assetId})
        var expertises = [Expertise]()
        var proceduresMap = [String: [String]]()
        for (assetId, list) in map {
            let item = Expertise(objectId: assetId, title: models[assetId]?.name ?? "", type: .model, assetsCount: 0, proceduresCount: list.count)
            expertises.append(item)
            proceduresMap[assetId] = list.map({$0.id})
        }
        expertises = expertises.sorted(by: {$0.title < $1.title})
        return (expertises, proceduresMap)
    }
}
