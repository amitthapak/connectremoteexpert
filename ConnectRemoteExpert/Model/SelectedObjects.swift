//
//  SelectedObjects.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/24/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// Object storing all selected objects.
class SelectedObjects {
    
    /// the categories
    var categories = [Category]()
    
    /// the types by categories
    var types = [String: [Type]]()
    
    /// the models by types
    var models = [String: [Model]]()
    
    /// the procedures
    var procedures = [String: [Procedure]]()
    
    /// Add category if it's missing
    /// - Parameter category: the category
    func addCategoryIfMissing(_ category: Category) {
        if let _ = categories.firstIndex(of: category) {
        }
        else {
            categories.append(category)
        }
    }
    
    /// Add type if it's missing
    /// - Parameters:
    ///   - type: the type
    ///   - categoryId: the category ID
    func addTypeIfMissing(_ type: Type, forCategory categoryId: String) {
        if var list = types[categoryId] {
            if let _ = list.firstIndex(of: type) {
            }
            else {
                list.append(type)
                types[categoryId] = list
            }
        }
    }
    
    /// Add type if it's missing
    /// - Parameters:
    ///   - model: the model
    ///   - typeId: the type ID
    func addModelIfMissing(_ model: Model, forType typeId: String) {
        if var list = models[typeId] {
            if let _ = list.firstIndex(of: model) {
            }
            else {
                list.append(model)
                models[typeId] = list
            }
        }
    }
    
    /// Deselect category
    /// - Parameter category: the category
    func deselect(category: Category) {
        /// Remove from `categories`
        if let index = categories.firstIndex(of: category) {
            categories.remove(at: index)
        }
        
        // Remove types
        let deselectedTypes = types[category.id] ?? []
        for item in deselectedTypes {
            deselect(type: item, forCategory: category.id)
        }
    }
    
    /// Deselect type
    /// - Parameters:
    ///   - type: the type
    ///   - categoryId: the ID
    func deselect(type: Type, forCategory categoryId: String) {
        if var list = types[categoryId] {
            // Remove from `types`
            if let index = list.firstIndex(of: type) {
                list.remove(at: index)
                types[categoryId] = list
            }
            
            // Remove models
            let deselectedModels = models[type.id] ?? []
            for item in deselectedModels {
                deselect(model: item, forType: type.id)
            }
        }
    }
    
    /// Deselect model
    /// - Parameters:
    ///   - model: the model
    ///   - typeId: the ID
    func deselect(model: Model, forType typeId: String) {
        if var list = models[typeId] {
            // Remove from `models`
            if let index = list.firstIndex(of: model) {
                list.remove(at: index)
                models[typeId] = list
            }
        }
    }
    
    /// Convert to Expertise
    func convertToExpertise() -> [ExpertisePut] {
        var list = [ExpertisePut]()
        for category in categories {
            if let types = types[category.id], !types.isEmpty {
                for type in types {
                    if let models = models[type.id], !models.isEmpty {
                        for model in models {
                            var e = Expertise.from(model: model)
                            // Update procedures count right away for Models
                            if let _ = procedures[model.id] {
                                e.proceduresCount = list.count
                            }
                            let procs = procedures[model.id] ?? []
                            list.append(e.toPut(procedures: procs.isEmpty ? nil : procs))
                        }
                    }
                    else {
                        list.append(Expertise.from(type: type).toPut())
                    }
                }
            }
            else {
                list.append(Expertise.from(category: category).toPut())
            }
        }
        return list
    }
}
