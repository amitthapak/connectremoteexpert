//
//  Document.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SwiftEx

/// Document model
struct Document: Codable {
    
    /// the fields
    let id: String
    let date: String
    let size: String
    let url: String
    
    var name: String!
    let mimeType: String?
    let associatedAssetId: String!
    
    /// date and size string
    var subtitle: String {
        let dateStr = Date.defaultFormatter.string(from: Date.defaultParser.date(from: date) ?? Date())
        let sizeStr = fileSizeNumberFormatter.string(from: NSNumber(value: (Int(size) ?? 0) / 1000000)) ?? "0" // Bytes -> MB
        return "\(dateStr) - \(sizeStr) MB"
    }
    
    /// the file icon
    var icon: UIImage {
        return UIImage(named: "iconFile.\(getMimeType())") ?? #imageLiteral(resourceName: "iconFile.pdf")
    }
    
    var type: DocumentType {
        return DocumentType(rawValue: getMimeType()) ?? .none
    }
    
    func getMimeType() -> String {
        if let mimeType = mimeType {
            return mimeType
        }
        if let ext = url.split(separator: ".").last {
            return String(ext)
        }
        return ""
    }
}

extension String {
    
    func getUrl() -> URL? {
        let url = self
        if url.hasPrefix("http:") || url.hasPrefix("https:") {
            return URL(string: url)
        }
        return URL(string: Configuration.baseUrl + url)
    }
}

/// Helpful extension
extension Document {
    
    /// Check if document matches the string
    /// - Parameter searchString: the search string
    func contains(string searchString: String) -> Bool {
        return name.lowercased().contains(searchString)
    }
}

/// Possible document types
enum DocumentType: String {
    case pdf = "pdf", zip = "zip", none = "none"
}
