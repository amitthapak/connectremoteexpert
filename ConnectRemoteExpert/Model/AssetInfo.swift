//
//  AssetInfo.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// The info to show in UI at the top for selected item
struct AssetInfo {
    
    let id: String
    let title: String
    let icon: String?
    let procedures: Int
    
    // true - if asset is selected, false - else
    let isAssetSelected: Bool
    
    /// Create from asset
    static func from(asset: Asset, procedures: Int) -> AssetInfo {
        return AssetInfo(id: asset.id, title: asset.name, icon: asset.imageUrl, procedures: procedures, isAssetSelected: true)
    }
    
    /// Info for non-select asset state
    static func create(assetsCount: Int, procedures: Int) -> AssetInfo {
        return AssetInfo(id: "", title: NSLocalizedString("Assets", comment: "Assets") + " (\(assetsCount))", icon: nil, procedures: procedures, isAssetSelected: false)
    }
    
    static var empty: AssetInfo {
        return AssetInfo(id: "", title: "Assets (-)", icon: nil, procedures: -1, isAssetSelected: false)
    }
}
