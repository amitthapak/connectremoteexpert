//
//  User.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// User model
struct User: Codable {

    // the fields
    let id: String
    let name: String
    let email: String!
    let role: String!
    
    var isExpert: Bool {
        return role == "expert"
    }
}
