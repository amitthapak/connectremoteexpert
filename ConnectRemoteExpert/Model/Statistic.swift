//
//  Statistic.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// Statistic info model
struct Statistic: Codable {
    
    /// the number of related assets (nil if statistic for a single asset)
    let assetsCount: Int!
    /// the number of procedures for the related assets (nil if statistic for a single asset)
    let procedures: Int!
    /// the total number of related documents
    let documents: Int
    /// the total number of related videos
    let videos: Int
    
    enum CodingKeys: String, CodingKey {
        case assetsCount = "assetCount"
        case procedures = "procedureCount"
        case documents = "documentCount"
        case videos = "videoCount"
    }
}
