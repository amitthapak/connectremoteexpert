//
//  Asset.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// typealiases for category and type
typealias Category = Asset
typealias Type = Asset
typealias Model = Asset

struct AssetResponse: Codable {
    let assets: [Asset]
}

/// Asset model
struct Asset: Codable, Hashable {
    
    let id: String
    let name: String
    let imageUrl: String?
    let isFavorite: Bool!
    let lastUpdated: String?
    let procedures: [Procedure]?
    let videos: [Video]?
    let documents: [Document]?
    let experts: [Expert]?
    
    /// Hash object
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
    }
    
    static func == (lhs: Asset, rhs: Asset) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}
