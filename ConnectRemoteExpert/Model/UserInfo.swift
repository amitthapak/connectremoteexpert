//
//  UserInfo.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/24/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// The user  info
struct UserInfo: Codable {
    
    /// the mean rating
    let rating: Float
    
    /// the number of votes for rating
    let numberOfVotes: Int
    
    /// the number of message today
    let inboxToday: Int
    
    /// the number of all messages
    let inboxAll: Int
    
    /// Convert to string
    func toRatingString() -> String {
        let ratingStr = defaultNumberFormatter.string(from: NSNumber(value: rating)) ?? "0"
        var votesStr: String!
        if numberOfVotes == 1 {
            votesStr = "\(numberOfVotes) " + NSLocalizedString("rating", comment: "rating")
        }
        else {
            votesStr = "\(numberOfVotes) " + NSLocalizedString("ratings", comment: "ratings")
        }
        return "\(ratingStr) (\(votesStr!))"
    }
}
