//
//  VideoCall.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 22.06.2020.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// Video call model
struct VideoCall: Codable {
    
    // the fields
    var callerUid: String
    var channelName: String
    var remoteUid: String
    var token: String
}
