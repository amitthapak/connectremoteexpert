//
//  AuthenticationResponse.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 7/10/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// the authentication response
struct AuthenticationResponse: Codable {
    
    /// the token
    let token: String
}
