//
//  RateQuestion.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/23/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// Model for a question
struct RateQuestion: Codable {
    
    /// the question
    let title: String
}
