//
//  Procedure.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// Procedure model
struct Procedure: Codable {
    
    /// the fields
    let id: String
    let name: String
    let asset: String!
    let isFavorite: Bool?
    
    let experts: [Expert]?
}
