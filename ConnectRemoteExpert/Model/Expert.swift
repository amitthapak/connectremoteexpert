//
//  Expert.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 19.06.2020.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

/// Expert model
struct Expert: Codable {

    // the fields
    let id: String
    let name: String
    let email: String?
    let expertTitle: String!
    let expertAvailability: Bool!
    let expertStatus: String!
    let userId: String?
}
