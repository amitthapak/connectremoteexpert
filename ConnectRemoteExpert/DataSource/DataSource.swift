//
//  DataSource.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import SwiftEx
import RxSwift
import Alamofire
import RxAlamofire
import SwiftyJSON

// alias for RestServiceApi
typealias DataSource = RestServiceApi

struct EmptyResponse: Codable {
}

// API methods
extension RestServiceApi {
    
    static let AUTH_HEADER = "Authorization"
    
    /// the list of user's expertise. Stored in memory for this challenge.
    private static var userExpertises = [Expertise]()
    
    static var lastUser: User?
    
    /// Login
    static func login(email: String, password: String) -> Observable<AuthenticationResponse> {
        return post(url: url("/login"), parameters: [
            "email": email,
            "password": password
        ])
    }
    
    /// Get top statistic
    static func getTopStat() -> Observable<Statistic> {
        return get(url: url("/dashboard"))
    }
    
    /// Get statistic for given model
    static func getStat(for item: Model) -> Observable<Statistic> {
        let statistic = Statistic(assetsCount: nil, procedures: nil, documents: item.documents?.count ?? 0, videos: item.videos?.count ?? 0)
        return Observable.just(statistic)
    }
    
    /// Get experts for given item
    static func getExperts(for item: Model) -> Observable<[Expert]> {
        get(url: url("/assets/\(item.id)")).map({ (asset: Asset) -> [Expert] in
            var map = [String: Expert]()
            for proc in (asset.procedures ?? []) {
                for item in proc.experts ?? [] {
                    map[item.id] = item
                }
            }
            return Array(map.values)
        })
    }
    
    /// Get experts for given item
    static func getAsset(id: String) -> Observable<Model> {
        get(url: url("/assets/\(id)"))
    }
    
    /// Get categories
    static func getCategories() -> Observable<[Category]> {
        get(url: url("/assets")).map { (json: AssetResponse) -> [Category] in
            return json.assets
        }
    }
    
    /// Get types
    static func getTypes(categoryId: String) -> Observable<[Type]> {
        get(url: url("/assets?parent=\(categoryId)")).map { (json: AssetResponse) -> [Type] in
            return json.assets
        }
    }
    
    /// Get models
    static func getModels(typeId: String) -> Observable<[Model]> {
        get(url: url("/assets?parent=\(typeId)")).map { (json: AssetResponse) -> [Model] in
            return json.assets
        }
    }
    
    static func like(_ asset: Asset, liked: Bool) -> Observable<Void> {
        if liked {
            let o: Observable<EmptyResponse> = post(url: url("/assets/\(asset.id)/favorite"), parameters: [:])
            return o.void()
        }
        else {
            return delete(url: url("/assets/\(asset.id)/favorite"))
        }
    }
    
    static func like(_ procedure: Procedure, liked: Bool) -> Observable<Void> {
        if liked {
            let o: Observable<EmptyResponse> = post(url: url("/procedures/\(procedure.id)/favorite"), parameters: [:])
            return o.void()
        }
        else {
            return delete(url: url("/procedures/\(procedure.id)/favorite"))
        }
    }
    
    static func doLike(_ asset: Asset, liked: Bool, in viewController: UIViewController) {
        like(asset, liked: liked)
            .subscribe(onNext: { (_) in
            }, onError: { (error) in
               // dodo post>void showError(errorMessage: error.localizedDescription)
            }).disposed(by: viewController.rx.disposeBag)
    }
    
    static func doLike(_ procedure: Procedure, liked: Bool, in viewController: UIViewController) {
        like(procedure, liked: liked)
            .subscribe(onNext: { (_) in
            }, onError: { (error) in
               // dodo post>void showError(errorMessage: error.localizedDescription)
            }).disposed(by: viewController.rx.disposeBag)
    }
    
    /// Get model procedures
    static func getProcedures(modelId: String) -> Observable<[Procedure]> {
        get(url: url("/assets/\(modelId)/procedures"))
    }
    
    // MARK: - Documents/Videos
    
    /// Get model documents
    static func getDocuments(model: Model) -> Observable<[Document]> {
        get(url: url("/assets/\(model.id)/documents"))
    }
    
    /// Get all documents
    static func getAllDocuments(page: Int = 1, limit: Int = 10, name: String? = nil) -> Observable<Page<Document>> {
        var param: [String: Any] = [
            "page": page,
            "limit": limit
        ]
        if let name = name {
            param["name"] = name
        }
        return get(url: url("/documents"), parameters: param)
    }
    
    /// Get model videos
    static func getVideos(model: Model) -> Observable<[Video]> {
        get(url: url("/assets/\(model.id)/videos"))
    }
    
    /// Get all videos
    static func getAllVideos(offset: Int?, limit: Int, name: String? = nil) -> Observable<([Video], Int)> {
        let page = offset ?? 1
        return getAllVideos(page: page, limit: limit, name: name)
            .map { (list) -> ([Video], Int) in
                return (list, page + 1)
        }
    }
    
    static func getAllVideos(page: Int = 1, limit: Int = 100000, name: String? = nil) -> Observable<[Video]> {
        var param: [String: Any] = [
            "page": page,
            "limit": limit
        ]
        if let name = name {
            param["name"] = name
        }
        return get(url: url("/videos"), parameters: param).map { (page: Page<Video>) -> [Video] in
            return page.item
        }
    }
    
    // MARK: - Settings
    
    /// Get current user
    static func getCurrentUser() -> Observable<User> {
        return get(url: url("/me")).map { (user: User) -> User in
            guard let userClaims = AuthenticationUtil.user else { return user }
            let o = User(id: user.id, name: user.name, email: userClaims.email, role: user.role)
            lastUser = o
            return o
        }
    }
    
    /// Get current user cached
    static func getCurrentUserCached() -> Observable<User> {
        if let user = lastUser {
            return Observable.just(user)
        }
        return get(url: url("/me")).map { (user: User) -> User in
            guard let userClaims = AuthenticationUtil.user else { return user }
            let o = User(id: user.id, name: user.name, email: userClaims.email, role: user.role)
            lastUser = o
            return o
        }
    }
    
    /// Get current expert user info
    static func getCurrentExpertInfo() -> Observable<ExpertInfoExtended> {
        let expertInfo: Observable<ExpertInfo> = get(url: url("/expertProfile"))
        return expertInfo.flatMap { (info: ExpertInfo) -> Observable<ExpertInfoExtended> in
            let map = info.expertise.hashmapWithKey({$0.assetId})
            return Observable.combineLatest(map.keys.map({self.getAsset(id: $0)})).flatMap { (models) -> Observable<ExpertInfoExtended> in
                
                let (expertise, procMap) = ExpertiseInfo.toExpertise(list: info.expertise, models: models.hashmapWithKey({$0.id}))
                let o: ExpertInfoExtended = ExpertInfoExtended(avgRating: info.avgRating,
                                                               expertise: expertise,
                                                               helpedAllTimeCount: info.helpedAllTimeCount,
                                                               helpedTodayCount: info.helpedTodayCount,
                                                               id: info.id,
                                                               image: info.image,
                                                               name: info.name,
                                                               ratingCount: info.ratingCount,
                                                               title: info.title,
                                                               expertAvailability: info.expertAvailability,
                                                               procedures: procMap)
                return Observable.just(o)
            }
        }
    }
    
    /// Submit feedback
    static func submit(feedback: String) -> Observable<EmptyResponse> {
        post(url: url("/feedback"), parameters: ["text": feedback])
    }
    
    /// Get policy
    static func getPolicy() -> Observable<String> {
        getText(url: url("/privacyPolicy"))
    }
    
    // MARK: - Rating
    
    /// Get questions for the rating form
    static func getQuestions() -> Observable<[RateQuestion]> {
        return convertJson("questions")
    }
    
    /// Submit ratings
    /// - Parameter rating: the rating
    static func submitRating(rating: [(RateQuestion, Float)], callInfo: CallInfo) -> Observable<Void> {
        let params: [String : Any] = [
            "callId": callInfo.id,
            "knowledgeable": rating[0].1,
            "satisfied": rating[1].1,
            "callDuration": callInfo.duration,
            "date": Date.dateRequest.string(from: Date()),
            "success": true
        ]
        return post(url: url("/experts/\(callInfo.expertId)/reviewCall"), parameters: params).map({(_: EmptyResponse) -> Void in return () })
    }
    
    // MARK: - Expertise
    
    /// Add expertise
    /// - Parameter expertises: the expertises
    static func addExpertises(_ expertises: [ExpertisePut]) -> Observable<[Expertise]> {
        return putExpertises(expertises)
    }
    
    /// Add expertise
    /// - Parameter expertises: the expertises
    static func deleteExpertise(assetId: String) -> Observable<Void> {
        return delete(url: url("/expertise?assetId=\(assetId)"))
    }
    
    static func putExpertises(_ expertises: [ExpertisePut]) -> Observable<[Expertise]> {
        request(.put, url: url("/expertise"), body: expertises.map({$0.toDic()}))
            .map { (json) -> [Expertise] in
                return try json.decodeIt()
        }
    }
    
    /// Remove expertise
    /// - Parameter expertise: the expertise
    static func removeExpertise(_ expertise: Expertise) -> Observable<Void> {
        delete(url: url("/expertise?assetId=\(expertise.objectId)"))
    }
    
    /// Update expert's availability
    static func enableSupport(_ enable: Bool) -> Observable<Bool> {
        post(url: url("/availability"), parameters: ["availability": enable])
    }
    
    static func updateStatus(inCall: Bool) -> Observable<EmptyResponse> {
        post(url: url("/status"), parameters: ["status": inCall ? "In Call" : "Free"])
    }
    
    // MARK: - Video calls
    
    /// Start a video call to the expert.
    static func videoCall(expertId: String) -> Observable<VideoCall> {
        post(url: url("/experts/\(expertId)/videoCall"), parameters: [:])
    }
    
    // MARK: -
    
    /// Convert JSON to object and return as Observable
    /// - Parameter json: the JSON object
    static private func convertJson<T: Codable>(_ jsonFileName: String) -> Observable<T> {
        guard let json = JSON.resource(named: jsonFileName) else {
            return Observable.error("Incorrect JSON format")
        }
        do {
            return Observable.just(try json.decode(T.self))
        }
        catch {
            return Observable.error(error)
        }
    }
    
    /// Convert JSON to object
    /// - Parameter json: the JSON object
    static private func convertJson<T: Codable>(_ jsonFileName: String) throws -> T {
        guard let json = JSON.resource(named: jsonFileName) else {
            throw "Incorrect JSON format"
        }
        return try json.decode(T.self)
    }
    
    // MARK: - Private
    
    /// Get full URL from endpoint
    private static func url(_ endpoint: String) -> String {
        return Configuration.baseUrl + endpoint
    }
    
    /// Request to API
    ///
    /// - Parameters:
    ///   - method: the method
    ///   - url: the URL
    ///   - parameters: the parameters
    ///   - headers: the headers
    ///   - encoding: the encoding
    /// - Returns: the observable
    public static func getText(
             url: URLConvertible,
             parameters: [String: Any]? = nil,
             headers: [String: String] = [:],
             encoding: ParameterEncoding = URLEncoding.default) -> Observable<String> {
        var headers = headers
        for (k,v) in RestServiceApi.headers {
            headers[k] = v
        }
        return RxAlamofire
            .request(.get, url, parameters: parameters, encoding: encoding, headers: headers)
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .default))
            .validate(contentType: ["text/html"])
            .responseString()
            .flatMap { (response, string) -> Observable<String> in
                if response.statusCode == 401 {
                    if let callback401 = callback401 { DispatchQueue.main.async { callback401() } }
                    return Observable.error(string)
                }
                return Observable.just(string)
            }
    }
}

extension Date {

    /// formatting date for API request
    public static let dateRequest: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return df
    }()
}

typealias Page = PageV4
public struct PageV4<T: Codable>: Codable {
    
    /// fields
    public var item: Array<T>
    public var page: Int?
    public var total: Int = 0
    public var count: Int = 0
    
    public init(item: Array<T>, page: Int?, total: Int = 0, count: Int = 0) {
        self.item = item
        self.page = page
        self.total = total
        self.count = count
    }
}
