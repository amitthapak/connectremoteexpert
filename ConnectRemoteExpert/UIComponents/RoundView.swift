//
//  RoundView.swift
//  UIComponents
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// View with round corners
@IBDesignable public class RoundView: UIView {
    
    /// the corner radius
    @IBInspectable public var corner: CGFloat = 8 { didSet { self.setNeedsLayout() } }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = corner
        layer.masksToBounds = true
    }
}
