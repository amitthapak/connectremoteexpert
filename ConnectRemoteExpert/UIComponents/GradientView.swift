//
//  GradientView.swift
//  UIComponents
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// View with gradient background
@IBDesignable public class GradientView: UIView {
    
    /// the corner radius
    @IBInspectable public var corner: CGFloat = 8 { didSet { self.setNeedsLayout() } }
    /// the main color
    @IBInspectable public var mainColor: UIColor = .white { didSet { self.setNeedsLayout() } }
    /// the second gradient color
    @IBInspectable public var gradirentColor: UIColor? = nil { didSet { self.setNeedsLayout() } }
    /// the gradient layer
    private var gradientLayer: CAGradientLayer?
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if let gradirentColor = gradirentColor {
            if self.gradientLayer == nil {
                self.gradientLayer = CAGradientLayer()
                self.layer.insertSublayer(self.gradientLayer!, at: 0)
            }
            gradientLayer?.colors = [mainColor.cgColor, gradirentColor.cgColor]
            gradientLayer?.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer?.endPoint = CGPoint(x: 0, y: 1)
            self.gradientLayer?.frame = self.bounds
        }
        else if gradientLayer != nil {
            gradientLayer?.removeFromSuperlayer()
            gradientLayer = nil
        }
        layer.cornerRadius = corner
        layer.masksToBounds = true
    }
}
