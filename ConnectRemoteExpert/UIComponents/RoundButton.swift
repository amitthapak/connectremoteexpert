//
//  RoundButton.swift
//  UIComponents
//
//  Created by Volkov Alexander on 5/24/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// View with round corners
@IBDesignable public class RoundButton: UIButton {
    
    /// the corner radius
    @IBInspectable public var corner: CGFloat = 8 { didSet { self.setNeedsLayout() } }
    
    /// Add round corners
    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = corner
        layer.masksToBounds = true
    }
}
