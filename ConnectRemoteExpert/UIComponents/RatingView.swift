//
//  RatingView.swift
//  UIComponents
//
//  Created by Volkov Alexander on 5/24/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// Rating view with 5 stars
public class RatingView: UIView {
    
    /// the number of stars
    private let n = 5
    private var stars = [UIImageView]()
    /// the width of the image
    private let imageWidth: CGFloat = 27
    
    /// the selected rating
    public var selectedRating: Float = 0
    
    /// the callback to notify about selected rating
    public var callback: ((Float)->())?
    
    /// the space between the images
    var space: CGFloat {
        return (self.bounds.width - CGFloat(n) * imageWidth) / max(1, CGFloat(n - 1))
    }
    
    /// Designated initializer
    /// - Parameter frame: frame
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupGestures()
    }
    
    /// Required initializer
    /// - Parameter aDecoder: decoder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupGestures()
    }
    
    /// Setup gestures
    private func setupGestures() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        self.addGestureRecognizer(tap)
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        self.addGestureRecognizer(pan)
    }
    
    /// Add stars
    public override func layoutSubviews() {
        super.layoutSubviews()
        if stars.isEmpty {
            for i in 0..<n {
                let imageView = UIImageView(frame: .zero)
                imageView.image = UIImage(named: "star0")!
                imageView.contentMode = .center
                imageView.tag = i
                stars.append(imageView)
                addSubview(imageView)
            }
        }
        
        // frame
        
        var x = CGFloat(0)
        for i in 0..<n {
            let f = CGRect(x: x, y: 0, width: imageWidth, height: self.bounds.height)
            stars[i].frame = f
            x += imageWidth + space
        }
        
        processTouch(at: selectedRating, notify: false)
    }
    
    /// Tap gesture handler
    /// - Parameter sender: the gesture recognizer
    @objc func tapGesture(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: self)
        var x = point.x
        var index = 0
        while x > (imageWidth + space / 2) {
            index += 1
            x -= imageWidth + space
        }
        processTap(at: index)
    }
    
    /// Pan gesture handler
    /// - Parameter sender: the gesture recognizer
    @objc func panGesture(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let point = sender.location(in: self)
            var x = point.x
            var value = Float(0)
            // the padding from left and right that is recognized as 0 and 1 values. All in the middle is treated as 0.5
            let paddingForGesture = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: imageWidth / 4)
            repeat {
                if x <= paddingForGesture.left {
                    break
                }
                else if x <= imageWidth - paddingForGesture.right { // center
                    value += 0.5
                    break
                }
                value += 1
                x -= imageWidth + space
            } while true
            processTouch(at: value, notify: true)
        }
    }
    
    /// Process tap at star with index
    /// - Parameter index: the index
    private func processTap(at index: Int) {
        selectedRating = Float(index + 1)
        for image in stars {
            image.image = image.tag <= index ? UIImage(named: "star") : UIImage(named: "star0")
        }
        callback?(selectedRating)
    }
    
    /// Process touch at star with value
    /// - Parameters:
    ///   - value: the value
    ///   - notify: true - if need to notify the callback
    private func processTouch(at value: Float, notify: Bool) {
        selectedRating = value
        var index = 1
        for image in stars {
            image.image = value.getStarImage(index: index)
            index += 1
        }
        if notify {
            callback?(value)
        }
    }
}

extension Float {
    
    /// Get start image for current rating
    /// - Parameter index: the start index
    public func getStarImage(index: Int) -> UIImage {
        let value = self
        if value >= Float(index) {
            return UIImage(named: "star")!
        }
        else if value > Float(index - 1) {
            return UIImage(named: "star05")!
        }
        else {
            return UIImage(named: "star0")!
        }
    }
}
