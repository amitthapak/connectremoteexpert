//
//  WidthSelectorViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
//import UIComponents

/// Line width selector
class WidthSelectorViewController: UIViewController {

    /// outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var buttonBgView: UIView!
    @IBOutlet weak var leftMargin: NSLayoutConstraint!
    @IBOutlet weak var gradientImage: UIImageView!
    @IBOutlet weak var alphaSlider: UISlider!
    @IBOutlet weak var gradientView: GradientSlider!
    @IBOutlet weak var arrowLeftPadding: NSLayoutConstraint?
    
    // the selected line width index
    var selectedIndex = 0
    var selectedAlpha: Float = 1
    var callback: ((Int, Float)->())? // line index, alpha
    var callbackAlpha: ((Float)->())? // alpha
    
    /// the selected color
    var selectedColor: UIColor!
    
    /// true - if shown for highlighter tool, false - for brush
    var isHighlighter = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .clear
        gradientImage.roundCorners(9)
        mainView.layer.cornerRadius = 10
        mainView.layer.masksToBounds = true
        buttonBgView.layer.cornerRadius = 10
        buttonBgView.layer.masksToBounds = true
        alphaSlider.value = selectedAlpha
        DispatchQueue.main.async {
            self.updateUI()
        }
        if isHighlighter {
            arrowLeftPadding?.constant = 120
            for i in 0..<5 {
                if let button = buttons.filter({$0.tag == i}).first {
                    button.setImage(UIImage(named: "h\(i+1)"), for: .normal)
                }
            }
        }
        updateGradient()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.updateUI()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        callback?(selectedIndex, alphaSlider.value)
    }
    
    /// Update gradient
    private func updateGradient() {
        gradientView.gradirentColor = selectedColor
    }
    
    /// Button action handler
    ///
    /// - parameter sender: the button
    @IBAction func buttonAction(_ sender: UIButton) {
        selectedIndex = sender.tag
        updateUI()
        callback?(selectedIndex, alphaSlider.value)
    }
    
    // Update UI
    func updateUI() {
        for button in buttons {
            button.tintColor = selectedIndex == button.tag ? .white : .black
            if selectedIndex == button.tag {
                let f = button.superview!.convert(button.frame, to: mainView)
                leftMargin.constant = f.origin.x + f.size.width / 2
            }
        }
    }
}
