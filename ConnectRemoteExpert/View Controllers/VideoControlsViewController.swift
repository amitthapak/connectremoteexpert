//
//  VideoControlsViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/4/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx

// Possible states for the panel with video controls
enum VideoControlsState {
    case opened, openedExtended, closed
    
    /// Convert to height
    func toHeight() -> CGFloat {
        switch self {
        case .opened:
            return 114
        case .openedExtended:
            return 293
        case .closed:
            return 64
        }
    }
    
    /// Convert to top padding
    func toTopPadding() -> CGFloat {
        switch self {
        case .opened:
            return 0
        case .openedExtended:
            return 0
        case .closed:
            return 64
        }
    }
    
    /// Get next state after a swipe
    func applySwipe(up: Bool) -> VideoControlsState? {
        switch self {
        case .opened:
            return up ? .openedExtended : .closed
        case .openedExtended:
            return up ? nil : .opened
        case .closed:
            return up ? .opened : nil
        }
    }
    
    /// Get next state after a tap
    func applyTap() -> VideoControlsState? {
        switch self {
        case .opened:
            return .closed
        case .openedExtended:
            return .opened
        case .closed:
            return .opened
        }
    }
}

/// Panel with controls for the video
class VideoControlsViewController: UIViewController {

    /// outlets
    @IBOutlet weak var topMargin: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var separator: UIView!
    
    @IBOutlet weak var flashLightButton: UIButton!
    @IBOutlet weak var flashLightIcon: UIImageView!
    @IBOutlet weak var disableCameraIcon: UIImageView!
    @IBOutlet weak var disableCameraButton: UIButton!
    @IBOutlet weak var disableCameraLabel: UILabel!
    
    @IBOutlet weak var speedInIcon: UIImageView!
    @IBOutlet weak var speedOutIcon: UIImageView!
    @IBOutlet weak var speedInLabel: UILabel!
    @IBOutlet weak var speedOutIconLabel: UILabel!
    @IBOutlet weak var speedSectionIcon: UIImageView!
    
    /// the button callbacks
    var callbackPainting: (()->())?
    var callbackSwitchCamera: (()->())?
    var callbackPause: (()->())?
    var callbackMicMute: ((Bool)->())?
    var callbackFinish: (()->())?
    var callbackSwipe: ((Bool)->())? // parameter: true - if swipe up, false - if swipe down
    var callbackSwipeButtonTap: (()->())?
    var callbackDisableCamera: ((Bool)->())?
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 32
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        separator.isHidden = true
        
        self.view.backgroundColor = UIColor.clear
        for button in buttons {
            button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        }
        
        // Add gestures from button up to mainView parent view
        var targetView: UIView! = centerButton
        var isLast = false
        while true {
            do {
                let swipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeUpAction(_:)))
                swipe.direction = .up
                targetView?.addGestureRecognizer(swipe)
                
            }
            do {
                let swipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeDownAction(_:)))
                swipe.direction = .down
                targetView?.addGestureRecognizer(swipe)
            }
            if isLast {
                break
            }
            isLast = targetView == mainView
            targetView = targetView?.superview
        }
        setupIcons()
    }
    
    /// Update icons for iOS13
    private func setupIcons() {
        if #available(iOS 13.0, *) {
            flashLightIcon.image = UIImage(systemName: "flashlight.on.fill")!
            disableCameraIcon.image = UIImage(systemName: "video.slash")!
            speedSectionIcon.image = UIImage(systemName: "speedometer")
            speedInIcon.image = UIImage(systemName: "arrow.down.circle")!
            speedOutIcon.image = UIImage(systemName: "arrow.up.circle")!
            flashLightIcon.contentMode = .scaleAspectFit
            disableCameraIcon.contentMode = .scaleAspectFit
            speedSectionIcon.contentMode = .scaleAspectFit
            speedSectionIcon.contentMode = .scaleAspectFit
            speedInIcon.contentMode = .scaleAspectFit
            speedOutIcon.contentMode = .scaleAspectFit
        }
    }
    
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge {
        return .bottom
    }
    
    /// Update UI
    func updateUI(withState state: VideoControlsState) {
        topMargin.constant = state.toTopPadding()
        separator.isHidden = state != .openedExtended
    }
    
    /// Update stats
    /// - Parameters:
    ///   - sent: the number of sent bytes
    ///   - received: the number of received bytes
    func updateStats(sent: Int, received: Int) {
        let rxInfo = Float(received * 1000).roundLetterSeparatedDollar()
        let sentInfo = Float(sent * 1000).roundLetterSeparatedDollar()
        speedInLabel.text = "\(rxInfo.0) \(rxInfo.1.uppercased())B/s"
        speedOutIconLabel.text = "\(sentInfo.0) \(sentInfo.1.uppercased())B/s"
    }
    
    /// Update drawing button
    /// - Parameter enabled: true - if enabled, false - disabled
    func updateDrawingButton(enabled: Bool) {
        guard let drawingButton = buttons.filter({$0.tag == 0}).first else { return }
        drawingButton.isEnabled = enabled
        drawingButton.alpha = enabled ? 1 : 0.5
    }
    
    /// Update mic button
    /// - Parameter enabled: true - if enabled, false - disabled
    func updateMicrophoneButton(enabled: Bool) {
        guard let micBtn = buttons.filter({$0.tag == 3}).first else { return }
        micBtn.isEnabled = enabled
        micBtn.alpha = enabled ? 1 : 0.5
    }
    
    // MARK: - Button actions
    
    /// Buttons action handler
    ///
    /// - parameter sender: the button
    @objc func buttonAction(_ sender: UIButton) {
        switch sender.tag {
        case 0: // Painting
            callbackPainting?()
        case 1: // Camera switch
            callbackSwitchCamera?()
        case 2: // Pause
            callbackPause?()
        case 3: // Mic button
            sender.isSelected = !sender.isSelected
            callbackMicMute?(sender.isSelected)
        case 4: // End
            callbackFinish?()
        default:
            break
        }
    }
    
    /// "Flashlight" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func flashLightAction(_ sender: Any) {
        showStub()
    }
    
    /// "Disable camera" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func disableCameraAction(_ sender: Any) {
        disableCameraButton.isSelected = !disableCameraButton.isSelected
        if !disableCameraButton.isSelected {
            if #available(iOS 13.0, *) {
                disableCameraIcon.image = UIImage(systemName: "video.slash")!
            } else {
                disableCameraIcon.image = #imageLiteral(resourceName: "disableCamera")
            }
        }
        else {
            if #available(iOS 13.0, *) {
                disableCameraIcon.image = UIImage(systemName: "video")!
            }
            else {
                disableCameraIcon.image = #imageLiteral(resourceName: "disableCamera")
            }
        }
        disableCameraLabel.text = disableCameraButton.isSelected ? "Enable\nCamera" : "Disable\nCamera"
        callbackDisableCamera?(disableCameraButton.isSelected)
    }
    
    /// Swipe button action handler
    ///
    /// - parameter sender: the button
    @IBAction func swipeButtonAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.callbackSwipeButtonTap?()
        }
    }
    
    /// Swipe up action handler
    @IBAction func swipeUpAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.callbackSwipe?(true)
        }
    }
    
    /// Swipe down action handler
    @IBAction func swipeDownAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.callbackSwipe?(false)
        }
    }
}
