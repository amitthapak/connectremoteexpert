//
//  MainViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 27.06.2020.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// Main screen
class MainViewController: UITabBarController {

    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()

        if let user = DataSource.lastUser {
            enableExpertTab(user.isExpert)
        }
        else {
            enableExpertTab(false)
            DataSource.getCurrentUser().subscribe(onNext: { [weak self] (user) in
                self?.enableExpertTab(user.isExpert)
            }).disposed(by: self.rx.disposeBag)
        }
    }

    
    /// Enable/disable expert tab
    /// - Parameter enable: true - enable, false - disable
    private func enableExpertTab(_ enable: Bool) {
        self.tabBar.items?[1].isEnabled = enable
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item == self.tabBar.items?.first {
            guard let navVC = viewControllers?.first as? UINavigationController else { return }
            navVC.popToRootViewController(animated: false)
            guard let vc = navVC.viewControllers.first as? DiscoverViewController else { return }
            vc.refresh()
        }
    }
}
