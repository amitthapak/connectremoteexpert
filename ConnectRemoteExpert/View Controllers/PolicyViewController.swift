//
//  PolicyViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx

/// Privacy Policy screen
class PolicyViewController: UIViewController {

    /// outlets
    @IBOutlet weak var textView: UITextView!
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
    }
    
    /// Load data
    private func loadData() {
        self.textView.text = "loading..."
        DataSource.getPolicy()
            .subscribe(onNext: { [weak self] value in
                self?.textView.attributedText = value.convertHtml()
                return
            }, onError: { error in
                DispatchQueue.main.async {
                    showError(errorMessage: error.localizedDescription)
                }
            }).disposed(by: rx.disposeBag)
    }
}
