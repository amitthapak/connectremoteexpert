//
//  CustomLoginViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 7/10/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
//import UIComponents
import AgoraARKit
import MSAL
import SwiftEx
import AgoraRtmKit
import SwiftyJSON

/// Custom login screen
class CustomLoginViewController: FormViewController, CallView, StreamHandler, AgoraRtmInvitertDelegate {

    /// outlets
    @IBOutlet weak var emailField: LoginTextField!
    @IBOutlet weak var passwordField: LoginTextField!
    @IBOutlet weak var loginButton: RoundButton!
    
    var provider: ProviderDelegate? = ProviderDelegate.shared
    private var mainViewController: UIViewController?
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.delegate = self
        passwordField.delegate = self
        AgoraARKit.agoraAppId = Configuration.appId
        
        RestServiceApi.UNAUTHORIZED_CODES = [401, 403]
        RestServiceApi.callback401 = { [weak self] in
            if AuthenticationUtil.isAuthenticated() {
                AuthenticationUtil.cleanUp()
                NotificationCenter.post(LoginNotifications.logout)
                delay(0.3) {
                    self?.moveToLoginScreen()
                }
            }
        }
        NotificationCenter.add(observer: self, selector: #selector(notificationHandler(_:)), name: LoginNotifications.logout)
        updateLoginButtonState(field: nil, text: nil)
    }
    
    /// Move to login screen
    private func moveToLoginScreen() {
        guard let vc = UIViewController.getCurrentViewController() else { return }
        if vc is UIAlertController {
            vc.dismiss(animated: false) { [weak self] in
                self?.moveToLoginScreen()
            }
        }
        else {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    
    /// View will appear
    ///
    /// - Parameter animated: the animation flag
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    /// View did appear
    ///
    /// - Parameter animated: the animation flag
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cleanNavigationStack()
        
        if AuthenticationUtil.isAuthenticated() {
            setAccessToken(UserDefaults.token ?? "")
            openHomeScreen()
        }
        
        if let call = lastCall {
            lastCall = nil
            provider?.end(call: call)
        }
    }
    
    /// Open home screen
    private func openHomeScreen() {
        guard let vc = createInitialViewController(fromStoryboard: "Main") else {
            return
        }
        vc.modalTransitionStyle = .flipHorizontal
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        mainViewController = vc
    }
    
    /// Dismiss keyboard
    ///
    /// - Parameters:
    ///   - touches: the touches
    ///   - event: the event
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    // MARK: - Button actions
    
    /// "Login" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func loginAction(_ sender: Any) {
        self.view.endEditing(true)
        guard emailField.textValue.isValidEmail && !passwordField.textValue.isEmpty else {
            showError(errorMessage: NSLocalizedString("Please provide valid username and password", comment: "Please provide valid username and password"))
            return
        }
        enableForm(false)
        DataSource.login(email: emailField.textValue, password: passwordField.textValue)
            .addActivityIndicator(on: UIViewController.getCurrentViewController() ?? self, skipError: true)
            .subscribe(onNext: { [weak self] value in
                self?.setAccessToken(value.token)
                DispatchQueue.main.async {
                    self?.enableForm(true)
                    self?.cleanUpForm()
                    self?.openHomeScreen()
                }
                return
            }, onError: { [weak self] error in
                showError(errorMessage: "Incorrect email or password.")
                self?.enableForm(true)
            }).disposed(by: rx.disposeBag)
    }
    
    /// Enable/disable form
    ///
    /// - Parameter enable: true - enable, false - disable
    private func enableForm(_ enable: Bool) {
        loginButton.isEnabled = enable
        emailField.isEnabled = enable
        passwordField.isEnabled = enable
    }
    
    /// Clean fields
    func cleanUpForm() {
        self.emailField.text = ""
        self.passwordField.text = ""
    }
    
    /// "Forgot Password?" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func forgotAction(_ sender: Any) {
        self.view.endEditing(true)
        guard emailField.textValue.isValidEmail else {
            showError(errorMessage: NSLocalizedString("Please provide valid email.", comment: "Please provide valid email."))
            return
        }
        // dodo call API
    }
    
    // MARK: - UITextFieldDelegate
    
    /// Switch to next field if "Next" button is tapped
    ///
    /// - Parameter textField: the text field
    /// - Returns: true
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        }
        else if textField == passwordField {
            passwordField.resignFirstResponder()
            loginAction(self)
        }
        return true
    }
    
    /// Enable/disable Login button
    ///
    /// - Parameters:
    ///   - textField: the textField
    ///   - range: the range
    ///   - string: the string
    /// - Returns: true
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text ?? ""
        text.replaceSubrange(range.toRange(string: text), with: string)
        updateLoginButtonState(field: textField, text: text)
        return true
    }
    
    /// Update Login button state
    ///
    /// - Parameters:
    ///   - field: one of the fields
    ///   - text: the text
    private func updateLoginButtonState(field: UITextField? = nil, text: String? = nil) {
        let email = (field == emailField ? text ?? "" : emailField.text) ?? ""
        let password = (field == passwordField ? text ?? "" : passwordField.text) ?? ""
        loginButton.alpha = !email.trim().isEmpty && !password.isEmpty ? 1 : 0.5
    }
    
    // MARK: -
    // MARK: - Agora
    
    /// Logout when user taps "Sign out"
    /// - Parameter notification: the notification
    @objc func notificationHandler(_ notification: Notification) {
        if notification.name.rawValue == LoginNotifications.logout.rawValue {
            // logout on RTM
            guard let kit = AgoraRtm.shared().kit else { return }
            kit.logout { (errorCode) in
                print("rtm logout code: \(errorCode)")
            }
        }
    }
    
    /// Setup RTM
    private func setupRtm() {
        DataSource.getCurrentUser()
            .subscribe(onNext: { value in
                let userId = value.id
                
                let rtm = AgoraRtm.shared()
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/rtm.log"
                rtm.setLogPath(path)
                
                guard let kit = AgoraRtm.shared().kit else {
                    showError(errorMessage: "AgoraRtmKit nil")
                    return
                }
                
                // login
                kit.login(account: userId, token: nil) { (error) in
                    showError(errorMessage: error.localizedDescription)
                }
                return
            }, onError: { error in
                DispatchQueue.main.async {
                    showError(errorMessage: error.localizedDescription)
                }
            }).disposed(by: rx.disposeBag)
    }
    
    /// Shows incoming call from user
    func showIncomingCall(invitation: AgoraRtmInvitation) {
        guard let jsonString = invitation.content else { return }
        let json =  JSON(parseJSON: jsonString)
        guard let channelName = json["channelName"].string,
            let handle = json["callerName"].string else {
                return
        }
        let userId = invitation.caller
        let call = Call(uuid: UUID(), channelName: channelName, userId: userId, userName: handle)
        provider?.startIncomingCall(call: call, phone: nil, email: nil)
    }
    
    // MARK: - CallView
    
    private var lastCall: Call?
    
    func updateCallUI() {
        /// Nothing to do
    }
    
    // MARK: - StreamHandler
    
    func start(call: Call) {
        // nothing to do
    }
    
    /// Show video call screen for "Expert" role
    /// - Parameter call: the call
    func answer(call: Call) {
        lastCall = call
        //        if let channel = sendAcceptCallAndGetChannelName() {
        //            showExpertVideoCall(channel: channel)
        //        }
    }
    
    /// Dismiss video call screen
    /// - Parameter call: the call
    func end(call: Call) {
        if lastCall != nil {
            navigationController?.popToRootViewController(animated: true)
        }
        lastCall = nil
        mainViewController?.dismiss(animated: true, completion: nil)
        mainViewController = nil
    }
    
    func decline(call: Call) {
        if lastCall != nil {
            navigationController?.popToRootViewController(animated: true)
        }
        lastCall = nil
        mainViewController?.dismiss(animated: true, completion: nil)
        mainViewController = nil
        
        guard let inviter = AgoraRtm.shared().inviter else {
            fatalError("rtm inviter nil")
        }
        
        inviter.refuseLastIncomingInvitation { (error) in
            showError(errorMessage: error.localizedDescription)
        }
    }
    
    private func setAccessToken(_ token: String) {
        AuthenticationUtil.processAccessToken(token)
        if !token.isEmpty {
            setupRtm()
            AgoraRtm.shared().inviterDelegate = self
        }
    }
    
    // MARK: - AgoraRtmInvitertDelegate
    
    func inviter(_ inviter: AgoraRtmCallKit, didReceivedIncoming invitation: AgoraRtmInvitation) {
        showIncomingCall(invitation: invitation)
    }
    
    func inviter(_ inviter: AgoraRtmCallKit, remoteDidCancelIncoming invitation: AgoraRtmInvitation) {
        if let call = ProviderDelegate.shared.getLastCall() {
            ProviderDelegate.shared.end(call: call)
        }
        NotificationCenter.post(CallActionNotification.end)
    }
}
