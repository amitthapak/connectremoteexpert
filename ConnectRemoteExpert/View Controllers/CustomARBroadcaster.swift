//
//  CustomARBroadcaster.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import ARKit
import AgoraRtcKit
import AgoraARKit
import ARVideoKit
import Foundation
import SwiftEx

/// Protocol for drawing UI
protocol DrawingView {
    
    /// Set color
    func setColor(_ color: UIColor, sendToRemote: Bool)
    
    /// Get color
    func getColor() -> UIColor
}

/// Constants
struct DrawingConstants {
    static let lineColor = UIColor.systemBlue
    static let lineWidth: CGFloat = 0.005
    static let lineAlpha: CGFloat = 1
    static let highlighterLineWidth: CGFloat = 0.01
    static let buttonSize = CGSize(width: 60, height: 60)
    static let photoCaptureButtonSize = CGSize(width: 50, height: 50)
    static let unactiveButtonColor = UIColor(red: 93/255, green: 83/255, blue: 78/255, alpha: 1)
}

/// Custom broadcaster view controller (for Technician role)
class CustomARBroadcaster: UIViewController, ARSessionDelegate, ARSCNViewDelegate, RenderARDelegate, AgoraRtcEngineDelegate, DrawingView {
    
    // MARK: ARKit properties
    
    /// The `ARWorldTrackingConfiguration`'s setting for plane detection. Defaults to `nil`
    var planeDetection: ARWorldTrackingConfiguration.PlaneDetection? = [.horizontal, .vertical]
    /// Setting to enable default lighting within the ARSCNView
    var enableDefaultLighting: Bool = true
    /// Setting to update lighting information within the ARSCNView
    var autoUpdateLights: Bool = true
    /// Setting to enable light estimation within the `ARTrackingConfiguration`
    var lightEstimation: Bool = true
    /// Debug option for the `ARTrackingConfiguration` to display render stats
    var showStatistics: Bool = true
    /// Debug option for the `ARTrackingConfiguration` to display debug data
    var arSceneDebugOptions: SCNDebugOptions = [.showWorldOrigin, .showFeaturePoints]
    
    // A reference to the `ARSCNView`
    private var sceneView: ARSCNView!
    
    // MARK: Agora Properties
    private var agoraKit: AgoraRtcEngineKit!                    // Agora.io Video Engine reference
    private let arVideoSource: ARVideoSource = ARVideoSource()  // for passing the AR camera as the stream
    var channelProfile: AgoraChannelProfile = .communication
    var frameRate: AgoraVideoFrameRate = .fps60
    var videoDimension: CGSize = AgoraVideoDimension1280x720
    var videoBitRate: Int = AgoraVideoBitrateStandard
    var videoOutputOrientationMode: AgoraVideoOutputOrientationMode = .fixedPortrait
    var audioSampleRate: UInt = 44100
    var audioChannelsPerFrame: UInt = 1
    var defaultToSpeakerPhone: Bool = true
    var channelName: String!                            // name of the channel to join
    
    // MARK: ARVideoKit properties
    internal var arvkRenderer: RecordAR!                         // ARVideoKit Renderer - used as an off-screen renderer
    
    // MARK: UI properties
    
    private var remoteVideoView: UIView!                        // video stream from remote user
    /**
     A `UIButton` that toggles the microphone
     */
    var micBtn: UIButton!
    var micBtnFrame: CGRect?
    var micBtnImage: UIImage?
    var micBtnTextLabel: String = "un-mute"
    var muteBtnImage: UIImage?
    var muteBtnTextLabel: String = "mute"
    
    var viewBackgroundColor: UIColor = .black
    
    // Remote user
    private var remoteUser: UInt?                               // remote user id
    private var dataStreamId: Int! = 27                         // id for data stream
    private var streamIsEnabled: Int32 = -1                     // acts as a flag to keep track if the data stream is enabled
    
    // Drawing
    private var lineColor: UIColor = UIColor.systemBlue {        // color to use when drawing
        didSet { tool.setColor(lineColor) }
    }
    private var lineSettings: LineSettings = LineSettings(width: DrawingConstants.lineWidth, alpha: DrawingConstants.lineAlpha) {
        didSet { tool.set(lineSettings: lineSettings) }
    }
    private var objectSettings: ObjectSettings = ObjectSettings(objectIndex: 0, width: DrawingConstants.lineWidth, alpha: DrawingConstants.lineAlpha) {
        didSet { (tool as? ObjectTool)?.set(settings: objectSettings)}
    }
    private var tool: Tool = PenTool(color: UIColor.systemBlue, lineSettings: LineSettings(width: DrawingConstants.lineWidth, alpha: DrawingConstants.lineAlpha))
    private var commands = [Command]()
    private var undidCommands = [Command]()
    
    /// true - will show debug info on the screen, false - else
    var debug = false
    private var microphoneMuted = false     // true - if sound is off, false - else
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        
        self.view.backgroundColor = viewBackgroundColor
        navigationItem.largeTitleDisplayMode = .never
        setupSceneView()
        createUI()
        setupAgora()
        setupRecorder()
    }
    
    // Agora setup
    private func setupAgora() {
        guard let agoraAppID = AgoraARKit.agoraAppId else { return }
        let agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: agoraAppID, delegate: self) // - init engine
        agoraKit.setChannelProfile(channelProfile) // - set channel profile
        if channelProfile == .liveBroadcasting {
            agoraKit.setClientRole(.broadcaster)
        }
        let videoConfig = AgoraVideoEncoderConfiguration(size: videoDimension, frameRate: frameRate, bitrate: videoBitRate, orientationMode: videoOutputOrientationMode)
        agoraKit.setVideoEncoderConfiguration(videoConfig) // - set video encoding configuration (dimensions, frame-rate, bitrate, orientation)
        agoraKit.enableVideo() // - enable video
        agoraKit.setVideoSource(self.arVideoSource) // - set the video source to the custom AR source
        agoraKit.enableWebSdkInteroperability(true)
        self.agoraKit = agoraKit // set a reference to the Agora engine
    }
    
    // Setup scene view
    private func setupSceneView() {
        // Setup sceneview
        let sceneView = ARSCNView() //instantiate scene view
        self.view.insertSubview(sceneView, at: 0)
        
        // add sceneView layout contstraints
        sceneView.translatesAutoresizingMaskIntoConstraints = false
        sceneView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        sceneView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        sceneView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        sceneView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        // set reference to sceneView
        self.sceneView = sceneView
        
        // set render delegate
        self.sceneView.delegate = self
        self.sceneView.session.delegate = self
        
        if debug {
            self.sceneView.debugOptions = arSceneDebugOptions
            self.sceneView.showsStatistics = showStatistics
        }
        
        // add default lights to the scene
        self.sceneView.autoenablesDefaultLighting = enableDefaultLighting
        self.sceneView.automaticallyUpdatesLighting = autoUpdateLights
    }
    
    // setup ARViewRecorder
    private func setupRecorder() {
        self.arvkRenderer = RecordAR(ARSceneKit: self.sceneView)
        self.arvkRenderer?.renderAR = self // Set the renderer's delegate
        // Configure the renderer to always render the scene
        self.arvkRenderer?.onlyRenderWhileRecording = false
        // Configure ARKit content mode. Default is .auto
        self.arvkRenderer?.contentMode = .aspectFill
        // add environment light during rendering
        self.arvkRenderer?.enableAdjustEnvironmentLighting = lightEstimation
        // Set the UIViewController orientations
        self.arvkRenderer?.inputViewOrientations = [.portrait]
        self.arvkRenderer?.enableAudio = false
    }
    
    /// Setup ARKit configuration
    func setARConfiguration() {
        let configuration = ARWorldTrackingConfiguration()
        if let planeDetection = self.planeDetection {
            configuration.planeDetection = planeDetection
        }
        // TODO: Enable Audio Data when iPhoneX bug is resolved
        //        configuration.providesAudioData = true  // AR session needs to provide the audio data
        configuration.isLightEstimationEnabled = lightEstimation
        // run the config to start the ARSession
        self.sceneView.session.run(configuration)
        self.arvkRenderer?.prepare(configuration)
    }
    
    // MARK: - View states
    
    /// AgoraARKit sets up and runs the ARTracking configuration within the `viewWillAppear`
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
        // Configure ARKit Session
        self.setARConfiguration()
    }
    
    /// AgoraARKit joins the Agora channel within the `viewDidAppear`
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
        if AgoraARKit.agoraAppId == nil {
            popView()
        } else {
            joinChannel() // Agora - join the channel
        }
    }
    
    /// AgoraARKit pauses the AR session within the `viewWillDisappear`
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
        // stop the ARVideoKit renderer
        arvkRenderer.rest()
        // Cleanup the session as the view is removed from hierarchy
        self.sceneView.session.pause()
        leaveChannel()
    }
    
    /// Remove sceneView
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
        sceneView.removeFromSuperview()
        sceneView = nil
    }
    
    // MARK: - Actions
    
    /// Connects to the Agora channel, and sets the default audio route to speakerphone
    func joinChannel() {
        // Set audio route to speaker
        let screenMaxLength = max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
        if UIDevice.current.userInterfaceIdiom == .phone && (screenMaxLength >= 896.0 && screenMaxLength <= 1024) {
            self.agoraKit.setDefaultAudioRouteToSpeakerphone(defaultToSpeakerPhone)
        }
        // Join the channel
        print("Join the channel: \(String(describing: self.channelName))")
        self.agoraKit.joinChannel(byToken: AgoraARKit.agoraToken, channelId: self.channelName, info: nil, uid: 0)
        UIApplication.shared.isIdleTimerDisabled = true     // Disable idle timmer
    }
    
    /// Leave channel
    func leaveChannel() {
        UIApplication.shared.isIdleTimerDisabled = false
        guard self.agoraKit != nil else { return }
        // leave channel and end chat
        self.agoraKit.leaveChannel()
    }
    
    // MARK: - UI
    
    /// Programmatically generated UI, creates the SceneView, and buttons.
    func createUI() {
        addRemoteView()
        addMuteButton()
        updateUI()
    }
    
    // Add remote view
    private func addRemoteView() {
        // add remote video view
        let remoteViewScale = self.view.frame.width * 0.33
        let remoteView = UIView()
        remoteView.frame = CGRect(x: self.view.frame.maxX - (remoteViewScale+15), y: self.view.frame.maxY - (remoteViewScale+25), width: remoteViewScale, height: remoteViewScale)
        remoteView.backgroundColor = UIColor.lightGray
        remoteView.layer.cornerRadius = 25
        remoteView.layer.masksToBounds = true
        self.view.insertSubview(remoteView, at: 1)
        self.remoteVideoView = remoteView
    }
    
    // Add mute button
    private func addMuteButton() {
        let button = UIButton()
        button.frame = CGRect(x: self.view.frame.midX - DrawingConstants.buttonSize.width / 2, y: self.view.frame.maxY-100, width: DrawingConstants.buttonSize.width, height: DrawingConstants.buttonSize.width)
        button.setImage(UIImage(named: "mic")!, for: .normal)
        button.tintColor = .white
        // background color is specified in updateUI
        button.layer.masksToBounds = true
        button.layer.cornerRadius = button.bounds.height / 2
        button.addTarget(self, action: #selector(toggleMic), for: .touchDown)
        self.view.insertSubview(button, at: 3)
        self.micBtn = button
    }
    
    /// Reset tools
    private func resetTools() {
        tool = PenTool(color: UIColor.systemBlue, lineSettings: LineSettings(width: DrawingConstants.lineWidth, alpha: DrawingConstants.lineAlpha))
        lineSettings = LineSettings(width: DrawingConstants.lineWidth, alpha: 1)
        objectSettings = ObjectSettings(objectIndex: 0, width: DrawingConstants.lineWidth, alpha: DrawingConstants.lineAlpha)
        lineColor = DrawingConstants.lineColor
    }
    
    // MARK: Button Actions
    
    /// Dismiss the current view
    @IBAction func popView() {
        leaveChannel()
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Local mirophone control for setting mute or enabled states.
    @IBAction func toggleMic() {
        if !microphoneMuted {
            self.agoraKit.muteLocalAudioStream(true)
            microphoneMuted = true
            lprint("disable active mic")
        } else {
            self.agoraKit.muteLocalAudioStream(false)
            microphoneMuted = false
            lprint("enable mic")
        }
        updateUIMicrophoneButton()
    }
    
    /// Update UI
    private func updateUI() {
        updateUIMicrophoneButton()
    }
    
    /// Update microphone button state
    private func updateUIMicrophoneButton() {
        micBtn?.backgroundColor = microphoneMuted ? .white : DrawingConstants.unactiveButtonColor
        micBtn?.tintColor = microphoneMuted ? .black : .white
    }
    
    // MARK: - DrawingView
    
    /// Set color
    func setColor(_ color: UIColor, sendToRemote: Bool) {
        lineColor = color
        
        if sendToRemote {
            // Send color back to client
            guard let colorString = color.toString() else { return }
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: colorString.data(using: String.Encoding.ascii)!)
        }
    }
    
    /// Get color
    func getColor() -> UIColor {
        return lineColor
    }
    
    /// Set tool
    /// - Parameter byIndex: the index of the tool
    func setTool(byIndex index: Int) {
        switch index {
        case 0: self.tool = PenTool(color: lineColor, lineSettings: lineSettings)
        case 1: self.tool = HighlighterTool(color: lineColor, lineSettings: lineSettings) // dodo DrawingConstants.highlighterLineWidth
        case 2: self.tool = EraserTool()
        case 3: self.tool = ObjectTool(color: lineColor, settings: objectSettings)
        default:
            print("ERROR: unsupported tool")
            break
        }
    }
    
    // MARK: - ARSessionDelegate
    
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        // updated every frame.
        // if we have points - draw one point per frame
        if let command = commands.last as? PaintOnFeaturesCommand {
            command.processNextPoint()
        }
        else if let command = commands.last as? EraseCommand {
            command.processNextPoint()
        }
    }
    
    func session(_ session: ARSession, didOutputAudioSampleBuffer audioSampleBuffer: CMSampleBuffer) {
        self.agoraKit?.pushExternalAudioFrameSampleBuffer(audioSampleBuffer)
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        lprint("session failed with error: \(error)", .Verbose)
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        lprint("sessionWasInterrupted", .Verbose)
    }
    
    // MARK: - RenderARDelegate
    
    func frame(didRender buffer: CVPixelBuffer, with time: CMTime, using rawBuffer: CVPixelBuffer) {
        self.arVideoSource.sendBuffer(buffer, timestamp: time.seconds)
    }
    
    // MARK: - AgoraRtcEngineDelegate
    
    /// Save remote UID
    public func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        if self.remoteUser == nil {
            self.remoteUser = uid // keep track of the remote user
            lprint("remote host added")
            resetTools()
        }
    }
    
    /// Add remote view canvas
    public func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        if uid == self.remoteUser {
            guard let remoteView = self.remoteVideoView else { return }
            let videoCanvas = AgoraRtcVideoCanvas()
            videoCanvas.uid = uid
            videoCanvas.view = remoteView
            videoCanvas.renderMode = .hidden
            agoraKit.setupRemoteVideo(videoCanvas)
            
            // create the data stream
            self.streamIsEnabled = self.agoraKit.createDataStream(&self.dataStreamId, reliable: true, ordered: true)
            lprint("Data Stream initiated - STATUS: \(self.streamIsEnabled)")
        }
        else {
            print("firstRemoteVideoDecodedOfUid: UID=\(uid) != \(String(describing: self.remoteUser))")
        }
    }
    
    /// Clean remote UID
    public func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        lprint("didOfflineOfUid: \(uid) with code: \(reason)")
        if uid == self.remoteUser {
            self.remoteUser = nil
            
            if let call = ProviderDelegate.shared.getLastCall() {
                call.userEnded?()
            }
            // Automatically go back
            super.backButtonAction()
        }
    }
    
    /// Back button action
    override func backButtonAction() {
        if let call = ProviderDelegate.shared.getLastCall() {
            ProviderDelegate.shared.end(call: call)
            delay(2) { // fixing issue with CallKit when it thinks that there are no calls
                super.backButtonAction()
            }
        }
        else {
            super.backButtonAction()
        }
    }
    
    public func rtcEngine(_ engine: AgoraRtcEngineKit, receiveStreamMessageFromUid uid: UInt, streamId: Int, data: Data) {
        guard let dataAsString = String(bytes: data, encoding: String.Encoding.ascii) else { return }
        
        lprint("STREAMID: \(streamId)\n - DATA: \(data)\n - STRING: \(dataAsString)\n")
        
        // check data message
        switch dataAsString {
        case let dataString where dataString.contains("color:"):
            lprint("color msg received\n - \(dataString)")
            
            if let command = ColorCommand.create(fromString: dataAsString, view: self) {
                command.apply()
                self.commands.append(command)
            }
        case let string where string.contains("tool:"):
            let index = Int(string.replacingOccurrences(of: "tool:", with: "")) ?? 0
            self.setTool(byIndex: index)
        
        // Line width
        case let string where LineSettings.match(string):
            self.lineSettings = LineSettings.decode(string)
            
        // Object settings
        case let string where ObjectSettings.match(string):
            let settings = ObjectSettings.decode(string)
            self.objectSettings = settings
        case "undo":
            while !commands.isEmpty {
                let command = self.commands.removeLast()
                command.undo()
                undidCommands.insert(command, at: 0)
                lprint("undo: \(command)")
                if !(command is ColorCommand) { break } // break when non-color command rolled back
            }
            syncUndoButtons()
        case "redo":
            while !undidCommands.isEmpty {
                let command = self.undidCommands.removeFirst()
                command.redo()
                commands.append(command)
                lprint("redo: \(command)")
                if !(command is ColorCommand) { break } // break when non-color command rolled back
            }
            syncUndoButtons()
        case let string where string.contains("touch-start:"):
            undidCommands.removeAll() // clean up rolled back commands
            // touch-starts
            print("touch-start msg received")
            var size = self.sceneView.bounds.size
            let a = string.split(separator: ":")
            if a.count == 3 {
                size = CGSize(width: CGFloat(exactly: generalNumberFormatter.number(from: String(a[1]))!) ?? 0,
                              height: CGFloat(exactly: generalNumberFormatter.number(from: String(a[2]))!) ?? 0)
            }
            DispatchQueue.main.async {
                // Add command
                if self.tool is EraserTool {
                    let command = EraseCommand(sceneView: self.sceneView, tool: self.tool)
                    command.remoteScreenSize = size
                    command.fetchCommandsCallback = { [weak self] ()->([PaintOnFeaturesCommand]) in
                        let list = self?.commands.filter({$0 is PaintOnFeaturesCommand}) ?? []
                        return list.map{$0 as! PaintOnFeaturesCommand}
                    }
                    command.apply()
                    self.commands.append(command)
                }
                else {
                    if let command = PaintOnFeaturesCommand.create(sceneView: self.sceneView, color:
                        self.lineColor, tool: self.tool) {
                        (command as? PaintOnFeaturesCommand)?.remoteScreenSize = size
                        command.apply()
                        self.commands.append(command)
                    }
                }
            }
        case "touch-end":
            lprint("touch-end msg received")
            tool.complete()
            syncUndoButtons()
        default:
            lprint("touch points msg received: \(dataAsString)")
            
            if let command = commands.last as? PaintOnFeaturesCommand {
                command.addPoints(fromString: dataAsString)
            }
            else if let command = commands.last as? EraseCommand {
                
                command.addPoints(fromString: dataAsString)
            }
            else {
                lprint("No commands in history")
            }
        }
    }
    
    /// Synchronize the states of undo/redo buttons
    private func syncUndoButtons() {
        let commandData = "buttons-sync:\(commands.count):\(undidCommands.count)"
        self.agoraKit.sendStreamMessage(self.dataStreamId, data: commandData.data(using: String.Encoding.ascii)!)
    }
}
