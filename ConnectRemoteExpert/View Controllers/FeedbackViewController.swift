//
//  FeedbackViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx

/// Feedback screen
class FeedbackViewController: UIViewController, UITextViewDelegate {

    /// outlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    
    /// placeholder text
    private var textPlaceholder: String = ""
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.contentInsetAdjustmentBehavior = .never
        
        textPlaceholder = textView.text?.trim() ?? ""
           updateUI()
        
        infoLabel.attributedText = infoLabel.createPrivacyPolicyAttributedString()
    }
    
    /// Setup navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .always
        setupLargeTitle(colored: true)
    }
    
    /// Update UI
    func updateUI() {
        setupLargeTitle(colored: true)
        footerView.backgroundColor = Colors.systemGrayBlock
        checkPlaceholder(textView, placeholderText: textPlaceholder, heightConstraint: textViewHeight)
    }
    
    /// "Submit" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func submitAction(_ sender: Any) {
        self.view.endEditing(true)
        let text = getText(textView, placeholder: textPlaceholder) ?? ""
        guard !text.trim().isEmpty else {
            showAlert(NSLocalizedString("Empty feedback", comment: "Empty feedback"), NSLocalizedString("Please enter something for the feedback.", comment: "Please enter something for the feedback.")); return
        }
        
        let complete = {
            // Show alert "Thank You!"
           self.showThanksForFeedback { [weak self] in
               self?.navigationController?.popViewController(animated: true)
           }
        }
        
        DataSource.submit(feedback: text)
            .subscribe(onNext: { value in
                complete()
                return
            }, onError: { _ in
                complete()
            }).disposed(by: rx.disposeBag)
    }
    
    /// "Privacy Policy" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func policyAction(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: "Back"), style: .plain, target: self, action: nil)
        guard let vc = create(PolicyViewController.self) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    /**
     Get text from textView
     
     - parameter textView:    the textView
     - parameter placeholder: the placeholder
     
     - returns: the text or nil if nothing was enterd
     */
    func getText(_ textView: UITextView, placeholder: String) -> String? {
        let text = textView.text.trim()
        if text.isEmpty || text == placeholder {
            return nil
        }
        else {
            return text
        }
    }
    
    /**
     Remove placeholder
     
     - parameter textView: the textView
     */
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.isScrollEnabled = true
        if #available(iOS 13.0, *) {
            textView.textColor = .label
        } else {
            textView.textColor = .black
        }
        if textView.text == textPlaceholder {
            textView.text = ""
        }
    }
    
    /**
     Update placeholders
     
     - parameter textView: the textView
     */
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.isScrollEnabled = false
        checkPlaceholder(textView, placeholderText: textPlaceholder, heightConstraint: textViewHeight)
    }
    
    /// Update height of the field when text is changed
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        delay(0.1) {
            let size = textView.sizeThatFits(textView.bounds.size)
            self.textViewHeight.constant = size.height
            self.applyTextFieldSize(textView, size: size, heightConstraint: self.textViewHeight)
        }
        return true
    }
    
    /**
     Applies size for given textView
     
     - parameter textView:         the textView
     - parameter size:             the size
     - parameter heightConstraint: the height constraint
     - parameter heightConstraint:
     */
    func applyTextFieldSize(_ textView: UITextView, size: CGSize, heightConstraint: NSLayoutConstraint) {
        let height = size.height
        let needToMove = heightConstraint.constant != height
        heightConstraint.constant = height
        if needToMove {
            textView.superview?.layoutIfNeeded()
        }
    }
    
    /**
     Set placeholder if text is empty
     
     - parameter textView:         the textView
     - parameter placeholderText:  the placeholder
     - parameter heightConstraint: the height constraint
     */
    func checkPlaceholder(_ textView: UITextView, placeholderText: String, heightConstraint: NSLayoutConstraint?) {
        if textView.text.trim().isEmpty || textView.text == placeholderText {
            textView.text = placeholderText
            textView.textColor = UIColor(0xc4c4c6)
        }
        else {
            if #available(iOS 13.0, *) {
                textView.textColor = .label
            } else {
                textView.textColor = .black
            }
        }
        let size = textView.sizeThatFits(textView.bounds.size)
        heightConstraint?.constant = size.height
    }
    
    /// Update UI when something is changed
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        updateUI()
    }
}
