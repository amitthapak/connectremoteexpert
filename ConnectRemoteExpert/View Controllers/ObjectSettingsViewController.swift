//
//  ObjectSettingsViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/5/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
//import UIComponents

/// Object Settings
class ObjectSettingsViewController: WidthSelectorViewController {

    /// outlets
    @IBOutlet var objectButtons: [UIButton]!
    @IBOutlet weak var leftMarginObject: NSLayoutConstraint!
    @IBOutlet weak var topMarginObject: NSLayoutConstraint!
    @IBOutlet weak var objectButtonBgView: UIView!
    
    // the selected object index
    var selectedObject = 0
    var callbackObject: ((Int, Int, Float)->())? // object index, line index, alpha
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        for button in objectButtons {
            button.addTarget(self, action: #selector(objectButtonAction(_:)), for: .touchUpInside)
        }
        objectButtonBgView.layer.cornerRadius = 10
        objectButtonBgView.layer.masksToBounds = true
    }
    
    /// Update UI
    override func updateUI() {
        super.updateUI()
        for button in objectButtons {
            button.tintColor = selectedObject == button.tag ? .white : .black
            if selectedObject == button.tag {
                let f = button.superview!.convert(button.frame, to: mainView)
                leftMarginObject.constant = f.origin.x + f.size.width / 2
                topMarginObject.constant = f.origin.y + f.size.height / 2
            }
        }
    }
    
    /// Button action handler
    ///
    /// - parameter sender: the button
    @IBAction override func buttonAction(_ sender: UIButton) {
        selectedIndex = sender.tag
        updateUI()
        callbackObject?(selectedObject, selectedIndex, alphaSlider.value)
    }

    /// Object type button action handler
    ///
    /// - parameter sender: the button
    @IBAction func objectButtonAction(_ sender: UIButton) {
        selectedObject = sender.tag
        updateUI()
        callbackObject?(selectedObject, selectedIndex, alphaSlider.value)
    }
}
