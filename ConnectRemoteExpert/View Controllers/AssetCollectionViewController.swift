//
//  AssetCollectionViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx

/// The asset types
enum AssetType: String, Codable {
    case category, type, model
    
    /// Convert to string
    func toString() -> String {
        switch self {
        case .category: return "Categories"
        case .type: return "Types"
        case .model: return "Models"
        }
    }
}

/// Possible usages of AssetCollectionViewController
enum AssetScreenUsage {
    case forDiscover, forExpertise
    
    func getTitleHeight() -> CGFloat {
        switch self {
        case .forDiscover:
            return 110
        case .forExpertise:
            return 68
        }
    }
}

/// Collection of assets to select
class AssetCollectionViewController: UIViewController {

    /// outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var filterButtons: UISegmentedControl!
    
    private var dataSource: CollectionDataModel<AssetCollectionViewCell>!
    
    // the title and path for navigation var
    var titleString: String?
    var pathString: String?
    
    // the screen type
    var screenType: AssetType = .category
    /// the type of the screen in relation to usage
    var usage: AssetScreenUsage = .forDiscover
    /// the related item. Nil for categories screen, non-nil for all other.
    var relatedItem: Asset!
    /// the parent for the related item
    var relatedParentItem: Asset!
    
    /// the callback
    var callback: FilterCallback!
    /// add expertise callback
    var addExpertiseCallback: (()->())!
    
    /// all selected objects
    var selectedObjects: SelectedObjects!
    
    /// the items to show
    private var items: [Asset]!
    
    /// the selected items
    private var selectedItems = [Asset]()
    
    /// true - if need to deselect parent automatically if all items are deselected, false - else
    /// It's set to true if screen is opened and there are selected items
    private var needToRemoveParentIfAllDeselected = false
    
    private var isFilteredByFavorite: Bool {
        return filterButtons.selectedSegmentIndex == 1
    }
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = CollectionDataModel(collectionView, cellClass: AssetCollectionViewCell.self) { [weak self] (item, indexPath, cell) in
            guard self != nil else { return }
            let item = item as! Asset
            cell.configure(item, isSelected: self!.selectedItems.contains(item), usage: self!.usage)
            cell.parent = self
        }
        dataSource.selected = { [weak self] item, indexPath in
            guard self != nil else { return }
            let item = item as! Asset
            self!.openNextScreen(for: item)
        }
        dataSource.calculateCellSize = { [weak self] _, _ -> CGSize in
            let width: CGFloat = self!.collectionView.cellWidth(forColumns: 2)
            let h = width + 29 // value for the title, as in design
            return CGSize(width: width, height: h)
        }
        loadData()
        if usage == .forExpertise {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done", comment: "Done"), style: .plain, target: self, action: #selector(doneAction(_:)))
        }
    }
    
    // MARK: - Navigation
    
    /// Open next screen
    /// - Parameter item: the only selected item or nil of "Done" button tapped and we need to use all selected items
    private func openNextScreen(for item: Asset) {
        self.view.isUserInteractionEnabled = false
        switch self.screenType {
        case .category: self.openTypesScreen(item)
        case .type: self.openModelsScreen(item)
        case .model: self.openProcedures(item)
        }
        delay(1) {
            self.view.isUserInteractionEnabled = true
        }
    }
    
    /// Open "Types" screen
    /// - Parameter item: the category
    private func openTypesScreen(_ item: Category) {
        applySelection()
        DataSource.getTypes(categoryId: item.id)
            .subscribe(onNext: { [weak self] value in
                self?.openScreen(forType: .type, item: item, items: value)
                return
            }, onError: { error in
                showError(errorMessage: error.localizedDescription)
            }).disposed(by: rx.disposeBag)
    }
    
    /// Open "Models" screen
    /// - Parameter item: the type
    private func openModelsScreen(_ item: Type) {
        applySelection()
        
        DataSource.getModels(typeId: item.id)
            .subscribe(onNext: { [weak self] value in
                self?.openScreen(forType: .model, item: item, items: value)
                return
                }, onError: { error in
                    showError(errorMessage: error.localizedDescription)
            }).disposed(by: rx.disposeBag)
    }
    
    /// Open "Procedures" screen
    /// - Parameter item: the model
    private func openProcedures(_ item: Model) {
        applySelection()
        guard let vc = create(ProceduresViewController.self) else { return }
        vc.item = item
        vc.relatedParentItem = relatedItem
        vc.selectedObjects = selectedObjects
        vc.usage = usage
        if let path = pathString {
            vc.pathString = "\(path)  |  \(item.name)"
        }
        else {
            vc.pathString = item.name
        }
        vc.callback = callback
        if addExpertiseCallback != nil {
            vc.addExpertiseCallback = {
                self.restoreSelection()
                self.applySelection()
                self.addExpertiseCallback()
                vc.addExpertiseCallback = nil
            }
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Open next screen
    /// - Parameters:
    ///   - type: the screen type
    ///   - item: the item to show
    ///   - items: the subitems to show
    private func openScreen(forType type: AssetType, item: Asset, items: [Asset]) {
        guard let vc = create(AssetCollectionViewController.self) else { return }
        vc.screenType = type
        vc.usage = usage
        if let path = pathString {
            vc.pathString = "\(path)  |  \(item.name)"
        }
        else {
            vc.pathString = item.name
        }
        vc.titleString = type.toString()
        
        vc.relatedItem = item
        vc.relatedParentItem = relatedItem
        vc.selectedObjects = self.selectedObjects
        vc.items = items
        if usage == .forDiscover {
            vc.selectedItems = items.filter({$0.isFavorite})
        }
        vc.callback = callback
        if addExpertiseCallback != nil {
            vc.addExpertiseCallback = {
                self.restoreSelection()
                self.applySelection()
                self.addExpertiseCallback()
                vc.addExpertiseCallback = nil
            }
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    
    /// Setup navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        restoreSelection()
        collectionView.reloadData()
        
        titleLabel.text = titleString ?? NSLocalizedString("Categories", comment: "Categories")
        navigationItem.largeTitleDisplayMode = .never
        setupCustomLargeTitle(colored: true, titleView: titleView)
        updateUI()
        subtitleLabel.text = pathString?.uppercased()
        titleView.isHidden = false
    }
    
    /// Update selected objects when moving back
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            applySelection()
        }
    }
    
    /// Update UI
    private func updateUI() {
        setupLargeTitle(colored: false)
    }
    
    /// Load data
    private func loadData() {
        if let _ = items {
            dataSource.setItems(getFilteredItems())
        }
        else { // Show categories if nothing is provided
            DataSource.getCategories()
                .subscribe(onNext: { [weak self] value in
                    self?.items = value
                    if self?.usage == .forDiscover {
                        self?.selectedItems = value.filter({$0.isFavorite})
                    }
                    self?.loadData()
                    return
                }, onError: { _ in
            }).disposed(by: rx.disposeBag)
        }
    }
    
    /// Get filtered items to show
    private func getFilteredItems() -> [Asset] {
        if isFilteredByFavorite {
            return items.filter({self.selectedItems.contains($0)})
        }
        return items
    }
    
    // MARK: - Button actions
    
    @IBAction func filterAction(_ sender: UISegmentedControl) {
        loadData()
    }
    
    /// "Like/checkbox" button action handler
    ///
    /// - parameter sender: the button
    fileprivate func selectAction(_ item: Asset, selected: Bool) {
        if selected {
            if let _ = selectedItems.firstIndex(of: item) {
            }
            else {
                selectedItems.append(item)
            }
        }
        else {
            if let index = selectedItems.firstIndex(of: item) {
                selectedItems.remove(at: index)
            }
        }
        if usage == .forDiscover {
            DataSource.doLike(item, liked: selected, in: self)
        }
    }
    
    /// Done action
    /// - Parameter sender: the button
    @objc func doneAction(_ sender: Any) {
        applySelection()
        let addExpertiseCallback = self.addExpertiseCallback!
        self.navigationController?.popToRootViewController(animated: true)
        DispatchQueue.main.async {
            addExpertiseCallback()
        }
    }
    
    /// Restore selection from `selectedObjects` to `selectedItems`. Used before showing content for the user.
    private func restoreSelection() {
        guard let selectedObjects = selectedObjects else { return }
        switch screenType {
        case .category:
            selectedItems = selectedObjects.categories
        case .type:
            selectedItems = selectedObjects.types[relatedItem.id] ?? []
        case .model:
            selectedItems = selectedObjects.models[relatedItem.id] ?? []
        }
        needToRemoveParentIfAllDeselected = !selectedItems.isEmpty
    }
    
    /// Apply selection on the screen to `selectedObjects`. Removes descendants if some object is unselected
    private func applySelection() {
        guard let selectedObjects = selectedObjects else { return }
        switch screenType {
        case .category:
            let deselected = selectedObjects.categories.filter({!self.selectedItems.contains($0)})
            selectedObjects.categories = selectedItems
            for item in deselected {
                selectedObjects.deselect(category: item)
            }
        case .type:
            let deselected = (selectedObjects.types[relatedItem.id] ?? []).filter({!self.selectedItems.contains($0)})
            selectedObjects.types[relatedItem.id] = selectedItems
            for item in deselected {
                selectedObjects.deselect(type: item, forCategory: relatedItem.id)
            }
        case .model:
            let deselected = (selectedObjects.models[relatedItem.id] ?? []).filter({!self.selectedItems.contains($0)})
            selectedObjects.models[relatedItem.id] = selectedItems
            for item in deselected {
                selectedObjects.deselect(model: item, forType: relatedItem.id)
            }
        }
        
        /// Check if need to add/remove parent item
        switch self.screenType {
        case .category:
            break
        case .type:
            guard relatedItem != nil else { return }
            if !selectedItems.isEmpty {
                selectedObjects.addCategoryIfMissing(relatedItem)
            }
            else if needToRemoveParentIfAllDeselected {
                selectedObjects.deselect(category: relatedItem)
            }
        case .model:
            guard relatedItem != nil else { return }
            if !selectedItems.isEmpty {
                selectedObjects.addTypeIfMissing(relatedItem, forCategory: self.relatedParentItem.id)
            }
            else if needToRemoveParentIfAllDeselected {
                selectedObjects.deselect(category: relatedItem)
            }
        }
    }
    
    // MARK: -

    /// Update UI if dark mode is on/off
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                updateUI()
            }
        }
    }
}

/// Cell for the collection
class AssetCollectionViewCell: UICollectionViewCell {
    
    /// outlets
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var likeIcon: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    
    /// the related item
    private var item: Asset!
    /// the reference to parent view controller
    fileprivate weak var parent: AssetCollectionViewController!
    /// related usage
    private var usage: AssetScreenUsage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconView.layer.cornerRadius = 5
        iconView.layer.masksToBounds = true
        iconView.layer.borderWidth = 0.5
        iconView.layer.borderColor = UIColor(0xd1d1d6).cgColor
        likeView.round()
        likeView.addBorder(color: UIColor(0xe4e4e9), borderWidth: 0.5)
    }
    
    /// Update UI
    /// - Parameters:
    ///   - item: the item to show
    ///   - isSelected: true - if already liked/selected, false - else
    func configure(_ item: Asset, isSelected: Bool, usage: AssetScreenUsage) {
        self.item = item
        self.usage = usage
        titleLabel.text = item.name
        self.iconView.image = nil
        UIImage.load(Configuration.baseUrl + item.imageUrl!) { [weak self] (image) in
            if self?.item.imageUrl == item.imageUrl {
                self?.iconView.image = image
            }
        }
        updateUIButton(isSelected)
        self.layoutIfNeeded()
    }
    
    /// Update like/checkbox button
    private func updateUIButton(_ selected: Bool) {
        likeButton.isSelected = selected
        likeView.backgroundColor = selected ? Colors.blueColor : UIColor.white
        likeView.layer.borderWidth = selected ? 0 : 0.5
        
        switch usage {
        case .forDiscover:
            likeIcon.image = selected ? #imageLiteral(resourceName: "iconLikeSmallSelected") : #imageLiteral(resourceName: "iconLikeSmall")
            likeView.isHidden = false
        case .forExpertise:
            likeView.isHidden = true
            likeButton.setImage(selected ? #imageLiteral(resourceName: "checkbox2Selected") : #imageLiteral(resourceName: "checkbox2") , for: .normal)
        default:
            break
        }
    }
    
    /// "Like" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func likeAction(_ sender: Any) {
        let value = !likeButton.isSelected
        updateUIButton(value)
        parent.selectAction(item, selected: value)
    }
}
