//
//  ProceduresViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx

/// Procedures screen
class ProceduresViewController: UIViewController {

    /// outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var topMargin: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var filterButtons: UISegmentedControl!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    
    /// the table model
    private var table = InfiniteTableViewModel<Procedure, ProceduresCell>()
    
    /// the type of the screen in relation to usage
    var usage: AssetScreenUsage = .forDiscover
    
    /// the related asset
    var item: Asset!
    /// the parent for the related item
    var relatedParentItem: Asset!
    var pathString: String?
    /// the callback
    var callback: FilterCallback!
    /// add expertise callback
    var addExpertiseCallback: (()->())!
    
    /// all selected objects
    var selectedObjects: SelectedObjects!
    
    /// the list of liked items
    var liked = [String]()
    
    /// the selected procedures IDs
    var selected = [String]()
    
    /// true - if need to deselect parent automatically if all items are deselected, false - else
    /// It's set to true if screen is opened and there are selected items
    private var needToRemoveParentIfAllDeselected = false
    
    private var isFilteredByFavorite: Bool {
        return filterButtons.selectedSegmentIndex == 1
    }
    
    private var allItems = [Procedure]()
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.configureCell = { [weak self] indexPath, item, _, cell in
            guard self != nil else { return }
            cell.parent = self
            cell.indexPath = indexPath
            cell.configure(item, isSelected: self!.selected.contains(item.id), isLiked: self!.liked.contains(item.id), showLike: self!.usage == .forDiscover)
        }
        table.onSelect = { indexPath, item in
        }
        table.loadItems = { [weak self] callback, failure in
            guard self != nil else { return }
            DataSource.getProcedures(modelId: self!.item.id)
                .subscribe(onNext: { value in
                    self?.allItems = value
                    callback(self?.getFilteredItems() ?? [])
                    return
                }, onError: { error in
                    failure(error.localizedDescription)
                }).disposed(by: self!.rx.disposeBag)
        }
        table.bindData(to: tableView)
        table.tableHeight = tableHeight
    }
    
    /// Get filtered items to show
    private func getFilteredItems() -> [Procedure] {
        if isFilteredByFavorite {
            return allItems.filter({self.liked.contains($0.id)})
        }
        return allItems
    }
    
    /// Setup navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        restoreSelection()
        tableView.reloadData()
        updateUI()

        navigationItem.largeTitleDisplayMode = .never
        setupNavigationBar(isTransparent: true)
        navigationController!.navigationBar.tintColor = nil
        subtitleLabel.text = pathString?.uppercased()
        titleView.isHidden = false
        titleView.backgroundColor = self.view.backgroundColor
    }
    
    /// Update selected objects when moving back
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            applySelection()
        }
    }
    
    /// Update UI
    private func updateUI() {
        setupLargeTitle(colored: true)
        footerView.backgroundColor = Colors.systemGrayBlock
        filterButtons.isHidden = usage != .forDiscover
        titleHeight.constant = usage.getTitleHeight()
    }
    
    @IBAction func filterAction(_ sender: UISegmentedControl) {
        table.set(items: getFilteredItems())
    }
    
    /// Like/unlike procedure
    fileprivate func likeAction(like: Bool, item: Procedure) {
        if like {
            liked.append(item.id)
        }
        else {
            if let index = liked.firstIndex(of: item.id) {
                liked.remove(at: index)
            }
        }
        DataSource.doLike(item, liked: like, in: self)
    }
    
    /// Checkbox button aciton handler
    /// - Parameters:
    ///   - item: the related item
    ///   - indexPath: the indexPath
    fileprivate func checkboxAction(item: Procedure, indexPath: IndexPath) {
        if let index = self.selected.firstIndex(of: item.id) {
            self.selected.remove(at: index)
        }
        else {
            self.selected.append(item.id)
        }
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    /// "Done" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func doneAction(_ sender: Any) {
        let item = self.item!
        let items = self.table.items.filter({self.selected.contains($0.id)})
        
        applySelection()
        
        let callback = self.callback
        let callbackAdd = self.addExpertiseCallback
        self.navigationController?.popToRootViewController(animated: true)
        DispatchQueue.main.async {
            if let callbackAdd = callbackAdd {
                callbackAdd()
            }
            else if let callback = callback {
                callback(item, items)
            }
        }
    }
    
    /// Restore selection from `selectedObjects` to `selectedItems`. Used before showing content for the user.
    private func restoreSelection() {
        guard let selectedObjects = selectedObjects else { return }
        selected = (selectedObjects.procedures[item.id] ?? []).map({$0.id})
        needToRemoveParentIfAllDeselected = !selected.isEmpty
    }
    
    /// Apply selection on the screen to `selectedObjects`.
    private func applySelection() {
        let item = self.item!
        let items = self.table.items.filter({self.selected.contains($0.id)})
        
        if let objects = selectedObjects {
            objects.procedures[item.id] = items
        }
        
        selectedObjects?.addModelIfMissing(item, forType: self.relatedParentItem.id)
    }
    
    /// Update UI if dark mode is on/off
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                updateUI()
            }
        }
    }
}

/// Cell for table in this view controller
class ProceduresCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var checkboxIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    /// the related item
    private var item: Procedure!
    fileprivate var indexPath: IndexPath!
    
    /// the reference to parent view controller
    fileprivate weak var parent: ProceduresViewController!
    
    /// Setup UI
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    ///   - isSelected: true - if selected
    ///   - isLiked: true - if liked
    ///   - showLike: true - if need to show like button
    func configure(_ item: Procedure, isSelected: Bool, isLiked: Bool, showLike: Bool) {
        self.item = item
        titleLabel.text = item.name
        likeButton.isSelected = isLiked
        checkboxIcon.image = isSelected ? #imageLiteral(resourceName: "iconCheckboxSelected") : #imageLiteral(resourceName: "iconCheckbox")
        likeButton.isHidden = !showLike
    }
    
    /// "Like" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func likeAction(_ sender: Any) {
        likeButton.isSelected = !likeButton.isSelected
        parent.likeAction(like: likeButton.isSelected, item: item)
    }
    
    /// Checkbox button action handler
    ///
    /// - parameter sender: the button
    @IBAction func checkboxAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.parent.checkboxAction(item: self.item, indexPath: self.indexPath)
        }
    }
}

