//
//  ToolsViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

enum ToolTypes: Int {
    
    case pen, marker, eraser
    
    /// Top padding for the selected tool
    func paddingSelected() -> CGFloat {
        switch self {
        case .pen:
            return -6.5
        case .marker:
            return -5.5
        case .eraser:
            return -8
        }
    }
    
    /// Top padding for the unselected tool
    func paddingClosed() -> CGFloat {
        return paddingSelected() + 20
    }
}

/// Tools panel view controller
class ToolsViewController: UIViewController {

    /// outlets
    @IBOutlet weak var tool1Button: UIButton!
    @IBOutlet weak var tool2Button: UIButton!
    @IBOutlet weak var tool3Button: UIButton!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet var toolImages: [UIImageView]!
    @IBOutlet weak var tool1TopMargin: NSLayoutConstraint!
    @IBOutlet weak var tool2TopMargin: NSLayoutConstraint!
    @IBOutlet weak var tool3TopMargin: NSLayoutConstraint!
    
    /// the selected tool index
    private var selectedToolIndex = 0
    
    var selectedColor: UIColor = .black {
        didSet {
            colorView?.backgroundColor = selectedColor
        }
    }
    
    /// the callback used to notify about selected tool
    var toolActionCallback: ((Int)->())?
    
    /// true - if eraser is on, false - else
    var isEraserTool: Bool {
        return selectedToolIndex == 2
    }
    /// true - if highlighter is on, false - else
    var isHighlighter: Bool {
        return selectedToolIndex == 1
    }
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
        
        colorView.layer.cornerRadius = colorView.bounds.width / 2
        colorView.layer.masksToBounds = true
        colorView.backgroundColor = selectedColor
        
        updateUI()
    }
    
    /// Tool button action handler
    ///
    /// - parameter sender: the button
    @IBAction func toolAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            if sender.tag < 4 { // skip color tool
                self.selectedToolIndex = sender.tag
                self.updateUI()
            }
            self.toolActionCallback?(sender.tag)
        }
    }
    
    /// Update UI
    func updateUI() {
        let constants = [tool1TopMargin, tool2TopMargin, tool3TopMargin]
        UIView.defaultAnimation {
            for i in 0..<3 {
                if self.selectedToolIndex == i {
                    constants[i]?.constant = ToolTypes(rawValue: i)!.paddingSelected()
                }
                else {
                    constants[i]?.constant = ToolTypes(rawValue: i)!.paddingClosed()
                }
            }
            self.view.layoutIfNeeded()
        }
    }

}
