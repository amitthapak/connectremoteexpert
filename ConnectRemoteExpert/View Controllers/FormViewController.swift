//
//  FormViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 7/10/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// stores estimated keyboard height
var KeyboardHeight: CGFloat = 403

/// Abstract class used for subclassing. Contains methods that move screen up when keyboard appear.
/// Provides keyboard related features.
class FormViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    /// outlet
    @IBOutlet var formOffsetOy: [NSLayoutConstraint]!
    
    /// flag: true - keyboard is on the screen, false - keyboard is not shown
    var keyboardIsShown = false
    
    var initialOffset: CGFloat = 50
    
    /// the extra offset under the keyboard
    var extraOffsetUnderKeyboard: CGFloat {
        return 0
    }
    
    /// flag: true - will use fixed shift based on min Oy throught all fields, false - will scroll for each field
    var useFixedShiftForAllFields: Bool = false
    
    /// the value updated after focusing on any field that has maximum Oy value
    fileprivate var minValueFieldOy: CGFloat = 4000
    
    /// the reference to last edited field
    var lastEditedField: UIView!
    
    /// true - if constraints are assigned automatically
    private var automaticallyAssigned: Bool {
        return automaticTarget != nil
    }
    
    /// the target view used in automatically assigned constraints
    private var automaticTarget: UIView?
    
    // MARK: Keyboard
    
    /// Update constraints
    internal func setupConstraints() {
        if view.subviews.count == 1 {
            let target = view.subviews[0]
            if formOffsetOy == nil {
                formOffsetOy = [NSLayoutConstraint]()
            }
            automaticTarget = target
            for c in view.constraints {
                if (c.firstItem as? UIView == target || c.secondItem as? UIView == target) && (c.firstAttribute == .top || c.firstAttribute == .bottom) && c.constant == 0 {
                    formOffsetOy.append(c)
                }
            }
        }
    }
    
    /// Add keyboard listeners
    ///
    /// - Parameter animated: the animation flag
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Listen for keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(FormViewController.keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FormViewController.keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /// Remove listeners
    ///
    /// - Parameter animated: the animation flag
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /// Handle keyboard opening
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let lastKeyboardHeight = KeyboardHeight
        let lastKeyboardIsShown = keyboardIsShown
        
        let viewHeight = self.view.frame.size.height
        
        if ((keyboardFrame.origin.y + keyboardFrame.size.height) > viewHeight) {
            KeyboardHeight = keyboardFrame.height
            keyboardIsShown = true
        }
        else {
            KeyboardHeight = 0
            keyboardIsShown = false
        }
        if lastKeyboardHeight != KeyboardHeight || lastKeyboardIsShown != keyboardIsShown {
            // move form up to reveal the field
            if let textField = lastEditedField {
                moveFormUp(textField)
            }
        }
    }
    
    /// Keyboard disappear event handler
    ///
    /// - Parameter notification: the notification object
    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardIsShown = false
        moveFormDown()
    }
    
    /// Close the keyboard or the form when tapped anywhere
    ///
    /// - Parameters:
    ///   - touches: the touches
    ///   - event: the event
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if keyboardIsShown {
            self.view.endEditing(true)
        }
    }
    
    // MARK: UITextFieldDelegate
    
    /// Save reference to last edited view
    ///
    /// - Parameter textField: the textField
    /// - Returns: true
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        lastEditedField = textField
        moveFormUp(textField)
        return true
    }
    
    /// Move form up
    ///
    /// - Parameter fieldView: the editanble field
    func moveFormUp(_ fieldView: UIView) {
        if keyboardIsShown {
            let currentPosition = fieldView.superview!.convert(fieldView.frame, to: self.view)
            let visibleHeight = UIScreen.main.bounds.height - KeyboardHeight
            var fieldOy = self.view.frame.origin.y + currentPosition.origin.y + formOffsetOy[0].constant
            let maxFieldOy = visibleHeight - getMaxFieldHeight(visibleHeight, fieldViewHeight: currentPosition.height) - extraOffsetUnderKeyboard
            if fieldOy > maxFieldOy {
                if useFixedShiftForAllFields {
                    if minValueFieldOy > fieldOy {
                        minValueFieldOy = fieldOy
                    }
                    fieldOy = minValueFieldOy                                       // fixed shift
                }
                let viewAdditionalOffset = fieldOy - maxFieldOy
                for constraint in formOffsetOy {
                    var multipliser: CGFloat = 1
                    if automaticallyAssigned {
                        if constraint.firstItem as? UIView == automaticTarget {
                            multipliser = -1
                        }
                    }
                    constraint.constant = viewAdditionalOffset * multipliser + initialOffset
                }
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
            else {
                moveFormDown()
            }
        }
    }
    
    /// Get max field height
    ///
    /// - Parameters:
    ///   - visibleScreenHeight: the visible screen height
    ///   - fieldViewHeight: the field height
    /// - Returns: the effective field height
    func getMaxFieldHeight(_ visibleScreenHeight: CGFloat, fieldViewHeight: CGFloat) -> CGFloat {
        let hasNavigationBar = self.navigationController != nil && !self.navigationController!.isNavigationBarHidden
        let visibleHeight = visibleScreenHeight - (hasNavigationBar ? (20 + 44) : 0)
        if fieldViewHeight > visibleScreenHeight {
            return visibleHeight
        }
        return fieldViewHeight
    }
    
    /// Move form down
    func moveFormDown() {
        for constraint in formOffsetOy {
            constraint.constant = initialOffset
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    /// Dismiss keyboard
    ///
    /// - Parameter textField: the textField
    /// - Returns: true
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: UITextViewDelegate
    
    /// Move form up when the field is focused
    ///
    /// - Parameter textView: the textView
    /// - Returns: the textView
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        lastEditedField = textView
        moveFormUp(textView)
        return true
    }
}
