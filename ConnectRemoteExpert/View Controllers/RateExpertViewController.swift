//
//  RateExpertViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/23/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
//import UIComponents

struct CallInfo {

    let expertId: String
    let id: String
    let duration: TimeInterval
}

/// "Rate your expert" screen
class RateExpertViewController: UIViewController {

    /// outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    
    /// the table model
    private var table = InfiniteTableViewModel<RateQuestion, RateQuestionCell>()
    
    /// the ratings
    private var ratings = [Float]()
    
    /// the call info
    var call: CallInfo!
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.attributedText = infoLabel.createPrivacyPolicyAttributedString()
        
        self.tableView.contentInsetAdjustmentBehavior = .never
        table.configureCell = { [weak self] indexPath, item, _, cell in
            guard self != nil else { return }
            cell.parent = self
            cell.indexPath = indexPath
            let rating = self!.ratings.count > indexPath.row ? self!.ratings[indexPath.row] : 0
            cell.configure(item, rating: rating)
        }
        table.onSelect = { _, item in
        }
        table.loadItems = { [weak self] callback, failure in
            guard self != nil else { return }
            DataSource.getQuestions()
                .subscribe(onNext: { value in
                    self?.resetRating(value)
                    callback(value)
                    return
                }, onError: { error in
                    failure(error.localizedDescription)
                }).disposed(by: self!.rx.disposeBag)
        }
        table.bindData(to: tableView)
        table.tableHeight = tableHeight
    }
    
    /// Reset rating stored in memory
    /// - Parameter list: the list of questions
    private func resetRating(_ list: [RateQuestion]) {
        ratings.removeAll()
        ratings = Array(repeating: 0, count: list.count)
    }
    
    /// Update UI
    private func updateUI() {
        setupLargeTitle(colored: true)
        footerView.backgroundColor = Colors.systemGrayBlock
    }
    
    /// Remove back button
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
        navigationItem.hidesBackButton = true
    }
    
    // MARK: - Button actions
    
    /// "Submit" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func submitAction(_ sender: Any) {
        var list = [(RateQuestion, Float)]()
        var shouldSend = true
        for i in 0..<ratings.count {
            if table.items.count > i {
                let r = ratings[i]
                if r <= 0 {
                    shouldSend = false
                    break
                }
                list.append((table.items[i], r))
            }
        }
        guard shouldSend else {
            showAlert(NSLocalizedString("Select rating", comment: "Select rating"), NSLocalizedString("Please select rating for each question.", comment: "Please select rating for each question."))
            return
        }
        
        DataSource.submitRating(rating: list, callInfo: call)
            .addActivityIndicator(on: self, skipError: true)
            .subscribe(onNext: { [weak self] value in
                self?.showSuccessSubmision()
                return
            }, onError: { [weak self] _ in
                self?.showSuccessSubmision()
            }).disposed(by: rx.disposeBag)
    }
    
    /// Show success message
    private func showSuccessSubmision() {
        // Show alert "Thank You!"
        self.showThanksForFeedback { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    /// "Privacy Policy" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func policyAction(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: "Back"), style: .plain, target: self, action: nil)
        guard let vc = create(PolicyViewController.self, storyboardName: "Main") else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Set rating
    /// - Parameters:
    ///   - value: the rating
    ///   - item: the question
    ///   - indexPath: the question
    fileprivate func setRating(_ value: Float, for item: RateQuestion, indexPath: IndexPath) {
        guard ratings.count > indexPath.row else { return }
        ratings[indexPath.row] = value
    }
    
    /// Update UI when something is changed
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI()
    }
}

/// Cell for table in this view controller
class RateQuestionCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingView: RatingView!
    
    /// the reference to parent view controller
    fileprivate var parent: RateExpertViewController!
    
    /// the related item
    private var item: RateQuestion!
    fileprivate var indexPath: IndexPath!
    
    /// Setup UI
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        ratingView.callback = { [weak self] value in
            guard self != nil else { return }
            self?.parent.setRating(value, for: self!.item, indexPath: self!.indexPath)
        }
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the question to show
    ///   - rating: the rating
    func configure(_ item: RateQuestion, rating: Float) {
        self.item = item
        titleLabel.text = item.title
    }
}

