//
//  VideoListViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx

/// Video search results
class VideoListViewController: UIViewController {

    /// outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    /// the table model
    internal var table = InfiniteTableViewModel<Video, DocumentCell>()
    
    internal var item: Asset?
    
    /// the items to show
    internal var items: [Video]?
    
    /// the search term
    var searchTerm: String = "" {
        didSet {
            table.loadData()
        }
    }
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()

        table.showLoadingIndicator = false
        self.tableView.contentInsetAdjustmentBehavior = .never
        table.configureCell = { indexPath, item, _, cell in
            cell.configure(item)
        }
        table.onSelect = { [weak self] _, item in
            DispatchQueue.main.async {
                self?.playVideo(item.url)
            }
        }
        if item != nil {
            table.loadItems = { [weak self] callback, failure in
                guard self != nil else { return }
                callback(self?.items ?? [])
            }
        }
        else {
            table.fetchItems = { [weak self] offset, limit, callback, failure in
                guard self != nil else { return }
                DataSource.getAllVideos(offset: offset as? Int, limit: limit, name: self?.searchTerm ?? "")
                    .subscribe(onNext: { (value, newOffset) in
                        callback(value, newOffset)
                        return
                    }, onError: { error in
                        failure(error.localizedDescription)
                    }).disposed(by: self!.rx.disposeBag)
            }
        }
        table.bindData(to: tableView)
    }
}
