//
//  DiscoverViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
import CallKit
import AgoraARKit

/// the typealias for the filter callback
typealias FilterCallback = (Model, [Procedure])->()

enum Storyboards: String {
    case main = "Main"
    case videoCall = "VideoCall"
}

/// Discover screen
class DiscoverViewController: UIViewController, CallView, StreamHandler {

    /// outlets
    @IBOutlet weak var topTableView: UITableView!
    @IBOutlet weak var expertsTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var topTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var expertsTableViewHeight: NSLayoutConstraint!
    
    /// the table model
    private var topTable = InfiniteTableViewModel<Any, UITableViewCell>()
    private var expertTable = SectionInfiniteTableViewModel<Expert, ExpertCell, ExpertSectionHeader>()
    
    /// the content to show
    private var topContent: [Any] = [AssetInfo.empty, (-1,-1)]
    private var noFilterContent: [Any]?
    
    private var selectedAsset: Asset?
    private var selectedProcedures: [Procedure]?
    private var experts: [Expert] = []
    
    private var provider: ProviderDelegate = ProviderDelegate.shared
    private var lastCall: Call?
    
    /// the last shown OutgoingCallViewController
    private var lastOutgoingCallScreen: OutgoingCallViewController?
    
    private var refreshControl: UIRefreshControl? {
        return expertsTableView.refreshControl
    }
    private var refreshTimer: Timer?
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        provider.callView = self
        provider.streamHandler = self
        
        noFilterContent = topContent
        noDataView.superview?.bringSubviewToFront(noDataView)
        
        // Top table
        topTable.preConfigure  = { indexPath, item, _ -> UITableViewCell in
            if indexPath.row == 0 {
                let cell = self.topTableView.dequeueReusableCell(withIdentifier: "TopAssetCell", for: indexPath) as! TopAssetCell
                cell.configure(item as! AssetInfo)
                cell.parent = self
                return cell
            }
            else {
                let cell = self.topTableView.dequeueReusableCell(withIdentifier: "TopFoldersCell", for: indexPath) as! TopFoldersCell
                cell.configure(item as! (Int, Int))
                cell.parent = self
                return cell
            }
        }
        topTable.showLoadingIndicator = false
        topTable.onSelect = { [weak self] indexPath, item in
            DispatchQueue.main.async {
                if indexPath.row == 0 {
                    guard let vc = self?.create(AssetCollectionViewController.self) else { return }
                    vc.callback = { item, procedures in
                        self?.showDataFor(item: item, procedures: procedures)
                    }
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        topTable.loadItems = { [weak self] callback, failure in
            guard self != nil else { return }
            callback(self!.topContent)
        }
        topTable.tableHeight = topTableViewHeight
        topTable.editActions = { indexPath, _ in
            guard indexPath.row == 0 else { return nil }
            let reset = UIContextualAction(style: .destructive, title: NSLocalizedString("Reset", comment: "Reset")) { [weak self] (action, view, callback) in
                self?.resetFilter()
                callback(true)
            }
            let conf = UISwipeActionsConfiguration(actions: [reset])
            conf.performsFirstActionWithFullSwipe = false
            return conf
        }
        topTable.bindData(to: topTableView)
        let pull2refresh = UIRefreshControl()
        expertsTableView.refreshControl = pull2refresh
        refreshTimer = Timer.scheduledTimer(withTimeInterval: Configuration.refreshInterval, repeats: true, block: { [weak self] (_) in
            self?.refresh()
        })

        // Expert table updates
        pull2refresh.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        // Experts
        expertTable.configureCell  = { indexPath, item, _, cell in
            cell.configure(item)
        }
        expertTable.onSelect = { _, item in
        }
        expertTable.sectionHeaderHeight = 48
        expertTable.configureHeader = { index, item, section in
            section.titleLabel.text = item
            section.mainView.backgroundColor = Colors.systemGrayBlock
        }
        expertTable.loadSectionItems = { [weak self] callback, failure in
            guard self != nil else { return }
            callback([self!.experts], [(NSLocalizedString("Available Experts", comment: "Available Experts") + " (\(self!.experts.count))").uppercased()])
        }
        expertTable.showLoadingIndicator = false
        expertTable.noDataLabel = noDataView
        expertTable.tableHeight = expertsTableViewHeight
        expertTable.editActions = { indexPath, item in
            guard item.expertAvailability else { return nil } // show buttons for available expert only
            
            let facetime = UIContextualAction(style: .normal, title: "") { [weak self] (action, view, callback) in
                self?.call(to: item)
                callback(true)
            }
            if #available(iOS 13.0, *) {
                facetime.image = UIImage(systemName: "video.fill")
            } else {
                facetime.image = UIImage(named: "iconFacetime")
            }
            facetime.backgroundColor = UIColor(0x005cbf)
            
            let call = UIContextualAction(style: .normal, title: "") { (action, view, callback) in
                showStub()
                callback(false)
            }
            if #available(iOS 13.0, *) {
                call.image = UIImage(systemName: "phone.fill")
            } else {
                call.image = UIImage(named: "iconCall")
            }
            call.backgroundColor = Colors.blueColor
            
            let conf = UISwipeActionsConfiguration(actions: [facetime, call])
            conf.performsFirstActionWithFullSwipe = false
            return conf
        }
        expertTable.bindData(to: expertsTableView)
        loadData()
    }
    
    /// Notify CallKit
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let call = lastCall {
            lastCall = nil
            provider.end(call: call)
        }
    }
    
    /// Show rate screen
    private func showRateScreen(call: CallInfo) {
        guard let vc = self.create(RateExpertViewController.self) else { return }
        vc.call = call
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Call to given user
    /// - Parameter to: the expert
    private func call(to expert: Expert) {
        guard let _ = expert.userId else { showError(errorMessage: "Cannot call this expert"); return }
        DataSource.getCurrentUserCached()
            .addActivityIndicator(on: UIViewController.getCurrentViewController() ?? self)
            .subscribe(onNext: { [weak self] value in
                guard self != nil else { return }
                guard value.id != expert.userId else { showError(errorMessage: "Cannot call yourself"); return }
                
                DataSource.videoCall(expertId: expert.id)
                    .subscribe(onNext: { (videoCall) in
                        self?.showCall(to: expert, videoCall: videoCall)
                    }, onError: { (error) in
                        DispatchQueue.main.async {
                            showError(errorMessage: error.localizedDescription)
                        }
                    }).disposed(by: self!.rx.disposeBag)
                return
            }, onError: { error in
                showError(errorMessage: error.localizedDescription)
            }).disposed(by: rx.disposeBag)
    }
    
    private var lastCallToExpertId: String?
    
    func showCall(to expert: Expert, videoCall: VideoCall) {
        lastCallToExpertId = expert.id
        guard let userId = expert.userId else {
            showError(errorMessage: "Cannot call this expert")
            return
        }
        // Notify CallKit
        DataSource.getCurrentUserCached()
            .addActivityIndicator(on: UIViewController.getCurrentViewController() ?? self)
            .subscribe(onNext: { [weak self] value in
                self?.provider.startOutgoingCall(userId: userId, userName: value.name, channelName: videoCall.channelName)
                
                // Show outgoing call screen
                guard let vc = self?.create(OutgoingCallViewController.self, storyboardName: Storyboards.videoCall.rawValue) else {
                    return
                }
                vc.modalPresentationStyle = .overFullScreen
                self?.navigationController?.present(vc, animated: true, completion: nil)
                self?.lastOutgoingCallScreen = vc
                return
            }, onError: { error in
                showError(errorMessage: error.localizedDescription)
            }).disposed(by: rx.disposeBag)
        
    }
    
    /// Setup navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    /// Update UI
    private func updateUI() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        setupNavigationBar(isTransparent: true, buttonColor: Colors.blueColor)
        setupLargeTitle(colored: true)
        if UIScreen.main.traitCollection.userInterfaceStyle == .light {
            noDataView.backgroundColor = Colors.footerColor
        }
        else {
            noDataView.backgroundColor = Colors.systemBackground
        }
        topTable.loadData()
    }
    
    /// Initial data loading
    private func loadData() {
        DataSource.getTopStat()
            .addActivityIndicator(on: self)
            .subscribe(onNext: { [weak self] value in
                // Store and show top statistic
                let data = [AssetInfo.create(assetsCount: value.assetsCount, procedures: value.procedures), (value.documents, value.videos)] as [Any]
                self?.topContent = data
                self?.noFilterContent = data // cache for later usage in reset
                self?.topTable.loadData()
                
                // Clean experts table
                self?.experts = []
                self?.expertTable.loadData()
                return
            }, onError: { (error) in
                DispatchQueue.main.async {
                    showError(errorMessage: error.localizedDescription)
                }
            }).disposed(by: rx.disposeBag)
    }
    
    // MARK: - Filter
    
    @objc func refresh() {
        if let item = self.selectedAsset {
            showDataFor(item: item, procedures: self.selectedProcedures ?? [])
        }
    }
    
    /// Show data
    private func showDataFor(item: Model, procedures: [Procedure]) {
        self.selectedAsset = item
        self.selectedProcedures = procedures
        
        // Load statistic
        DataSource.getStat(for: item)
            .subscribe(onNext: { [weak self] value in
                // Store and show top statistic
                let data = [AssetInfo.from(asset: item, procedures: procedures.count), (value.documents, value.videos)] as [Any]
                self?.topContent = data
                self?.topTable.loadData()
                return
            }, onError: { (error) in
                showError(errorMessage: error.localizedDescription)
            }).disposed(by: rx.disposeBag)
        
        // Load experts
        DataSource.getExperts(for: item)
            .subscribe(onNext: { [weak self] value in
                self?.experts = value
                self?.expertTable.loadData()
                self?.refreshControl?.endRefreshing()
                return
            }, onError: { (error) in
                showError(errorMessage: error.localizedDescription)
                return
            }).disposed(by: rx.disposeBag)
    }
    
    /// Reset filter
    func resetFilter() {
        selectedAsset = nil
        topContent = noFilterContent!
        experts = []
        topTable.loadData()
        expertTable.loadData()
    }
    
    // MARK: - Button actions
    
    /// "Documents" button action
    fileprivate func docsAction() {
        guard let vc = create(DocumentsViewController.self) else { return }
        vc.item = selectedAsset
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// "Videos" button action
    fileprivate func videosAction() {
        guard let vc = create(VideosCollectionViewController.self) else { return }
        vc.item = selectedAsset
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Update UI if dark mode is on/off
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                updateUI()
            }
        }
    }
    
    // MARK: - StreamHandler
    
    /// Update UI
    func updateCallUI() {
        if let call = provider.getLastCall() {
            lastOutgoingCallScreen?.updateUI(call: call)
        }
    }
    
    /// Call started (caller)
    /// - Parameter call: the call
    func start(call: Call) {
        lastCall = call
        
        guard let kit = AgoraRtm.shared().kit else { fatalError("rtm kit nil") }
        guard let inviter = AgoraRtm.shared().inviter else { fatalError("rtm inviter nil") }
        
        let closeOutgoingCall = { [weak self] (success: Bool)->() in
            if let popup = self?.lastOutgoingCallScreen {
                popup.dismiss(animated: true) {
                    if success && self?.lastCall != nil {
                        self?.showUserVideo(call: call)
                    }
                }
            }
        }
        // rtm query online status
        kit.queryPeerOnline(call.userId, success: { (onlineStatus) in
            switch onlineStatus {
            case .online:      sendInvitation(call: call)
            case .offline, .unreachable:
                print("remote offline")
                delay(2) {
                    call.userOffline?();
                    closeOutgoingCall(false)
                }
            @unknown default:
                fatalError("queryPeerOnline")
            }
        }) {  (error) in
            print("ERROR: \(error.localizedDescription)")
            delay(2) {
                call.connectionError?()
                closeOutgoingCall(false)
                DispatchQueue.main.async {
                    showError(errorMessage: "Connection error. Please try again in a few minutes.")
                }
            }
        }
        call.hasStartedConnectingDidChange?()
        
        // rtm send invitation
        func sendInvitation(call: Call) {
            DataSource.getCurrentUserCached()
                .subscribe(onNext: { [weak self] value in
                    guard self != nil else { return }
                    let remoteId = call.userId
                    let callerName = value.name
                    let channel = call.channelName
                    let content = "{\"channelName\": \"\(channel)\", \"callerName\": \"\(callerName)\"}"
                    
                    inviter.sendInvitation(peer: remoteId, extraContent: content, accepted: {
                        
                        self?.startCallDate = Date()
                        call.hasConnectedDidChange?()
                        closeOutgoingCall(true)
                    }, refused: {
                        
                        closeOutgoingCall(false)
                        // we should call .end(call: ..) here, but current vc will do that when appear again
                        print("remote refused this invitation")
                    }) { (error) in
                        showError(errorMessage: error.localizedDescription)
                    }
                }, onError: { error in
                    showError(errorMessage: error.localizedDescription)
                }).disposed(by: rx.disposeBag)
        }
        notifyStatusChange(inCall: true)
    }
    
    /// Show video call screen
    /// - Parameter call: the call
    private func showUserVideo(call: Call) {
        self.showUserVideoCall(channel: call.channelName)
    }
    
    /// Show video call screen for "Expert" role.
    /// - Parameter call: the call
    func answer(call: Call) {
        // Automatically accept call.
        lastCall = call
        if let channel = sendAcceptCallAndGetChannelName() {
            showExpertVideoCall(channel: channel)
        }
        notifyStatusChange(inCall: true)
    }
    
    private var startCallDate: Date!
    
    /// End call (celler and callee)
    /// - Parameter call: the call
    func end(call: Call) {
        if let popup = lastOutgoingCallScreen {
            popup.dismiss(animated: true, completion: nil)
        }
        lastOutgoingCallScreen = nil
        lastCall = nil
        
        // Show rating form if call is ended
        if let expertId = lastCallToExpertId, let startCallDate = startCallDate {
            let endCallDate = Date()
            let duration = endCallDate.timeIntervalSince(startCallDate)
            let callInfo = CallInfo(expertId: expertId, id: call.channelName, duration: duration)
            showRateScreen(call: callInfo)
        }
        startCallDate = nil
        notifyStatusChange(inCall: false)
    }
    
    /// cellee
    func decline(call: Call) {
        if let popup = lastOutgoingCallScreen {
            popup.dismiss(animated: true, completion: nil)
        }
        lastOutgoingCallScreen = nil
        lastCall = nil
        
        guard let inviter = AgoraRtm.shared().inviter else { fatalError("rtm inviter nil") }
        
        inviter.refuseLastIncomingInvitation { (error) in
            showError(errorMessage: error.localizedDescription)
        }
        notifyStatusChange(inCall: false)
    }
    
    private func notifyStatusChange(inCall: Bool) {
        DataSource.updateStatus(inCall: inCall)
            .subscribe(onNext: { _ in
                return
            }, onError: { error in
                print("updateStatus: \(error.localizedDescription)")
            }).disposed(by: rx.disposeBag)
    }
    
    // MARK: - RTM
    
    /// Send message that call is accepted
    private func sendAcceptCallAndGetChannelName() -> String? {
        guard let inviter = AgoraRtm.shared().inviter else { fatalError("rtm inviter nil") }
        guard let channelId = inviter.lastIncomingInvitation?.channelName else { fatalError("lastIncomingInvitation content nil") }
        inviter.accpetLastIncomingInvitation()
        return channelId
    }
}

// MARK: -

/// Cell for table in this view controller
class TopAssetCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var leftMargin: NSLayoutConstraint!
    
    /// the related item
    private var item: AssetInfo!

    fileprivate weak var parent: DiscoverViewController!
    
    /// Setup UI
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: AssetInfo) {
        self.item = item
        titleLabel.text = item.title
        let value = item.procedures >= 0 ? defaultNumberFormatter.string(for: item.procedures)! : "-"
        subtitleLabel.text = NSLocalizedString("Procedures", comment: "Procedures") + " (\(value))"
        
        self.iconView.image = nil
        if let icon = item.icon {
            UIImage.load(Configuration.baseUrl + icon) { [weak self] (image) in
                if self?.item.icon == item.icon {
                    self?.iconView.image = image
                }
            }
        }
        leftMargin.constant = item.icon == nil ? 20 : 91
    }
}

/// Cell for table in this view controller
class TopFoldersCell: ClearCell {
    
    @IBOutlet weak var docsCountLabel: UILabel!
    @IBOutlet weak var videoCountLabel: UILabel!
    @IBOutlet weak var blockView: UIView!
    @IBOutlet weak var videosBlock: UIView!
    
    fileprivate weak var parent: DiscoverViewController!
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: (Int, Int)) {
        blockView.backgroundColor = Colors.systemGrayBlock
        videosBlock.backgroundColor = Colors.systemGrayBlock
        blockView.layer.borderWidth = 0.5
        videosBlock.layer.borderWidth = 0.5
        blockView.layer.borderColor = Colors.systemBlockBorder.cgColor
        videosBlock.layer.borderColor = Colors.systemBlockBorder.cgColor
        docsCountLabel.text = item.0 >= 0 ? defaultNumberFormatter.string(from: NSNumber(value: item.0)) : "-"
        videoCountLabel.text = item.1 >= 0 ? defaultNumberFormatter.string(for: item.1) : "-"
    }
    
    /// "Documents" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func docAction(_ sender: Any) {
        parent?.docsAction()
    }
    
    /// "Videos" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func videosAction(_ sender: Any) {
        parent?.videosAction()
    }
}

/// Cell for experts table
class ExpertCell: ClearCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: Expert) {
        
        titleLabel.text = item.name
        subtitleLabel.text = item.expertStatus
    }
}

// Section view for experts table
class ExpertSectionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
}
