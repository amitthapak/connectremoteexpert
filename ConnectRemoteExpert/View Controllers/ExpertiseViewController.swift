//
//  ExpertiseViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/24/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
import RxSwift

/// Possible cells for the table on this screen
enum ExpertiseCellType {

    case user, support, info, button, addExpertise, expertise(Expertise)
}

/// Expertise screen
class ExpertiseViewController: UIViewController {

    /// outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var buttonTopMargin: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var tableView2: UITableView!
    
    /// the table model
    private var table = SectionInfiniteTableViewModel<ExpertiseCellType, UITableViewCell, ExpertiseSectionHeader>()
    private var table2 = SectionInfiniteTableViewModel<ExpertiseCellType, UITableViewCell, ExpertiseSectionHeader>()
    
    /// the list of users's expertise
    private var expertises = [Expertise]()
    private var expertInfo: ExpertInfoExtended!
    /// the related user's info
    private var userInfo: (Expert, UserInfo)!
    /// true - support is enabled, false - else
    private var supportEnabled: Bool = false
    
    /// the items to show in the table
    private var topItems = [[ExpertiseCellType]]()
    private var items = [[ExpertiseCellType]]()
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.setLineSpacing(lineSpacing: 2)
        configure(table: table, with: tableView)
        configure(table: table2, with: tableView2)
        table.tableHeight = tableHeight
        loadData()
        
        NotificationCenter.add(observer: self, selector: #selector(notificationHandler(_:)), name: CallActionNotification.completed)
    }
    
    @objc func notificationHandler(_ notification: NSNotification) {
        if notification.name.rawValue == CallActionNotification.completed.rawValue {
            loadData()
        }
    }
    
    private func configure(table: SectionInfiniteTableViewModel<ExpertiseCellType, UITableViewCell, ExpertiseSectionHeader>, with tableView: UITableView) {
        table.preConfigure = { [weak self] indexPath, item, _ -> UITableViewCell in
            guard self != nil else { return UITableViewCell() }
            switch item {
            case .user:
                let cell = self!.tableView.dequeueReusableCell(withIdentifier: "ExpertiseUserCell", for: indexPath) as! ExpertiseUserCell
                cell.configure(self!.userInfo.0, ratingInfo: self!.userInfo.1)
                return cell
            case .support:
                let cell = self!.tableView.dequeueReusableCell(withIdentifier: "ExpertiseSupportToggleCell", for: indexPath) as! ExpertiseSupportToggleCell
                cell.configure(self?.supportEnabled)
                cell.parent = self
                return cell
            case .info:
                return self!.tableView.dequeueReusableCell(withIdentifier: "ExpertiseInfoCell", for: indexPath) as! ExpertiseInfoCell
            case .button:
                let cell = self!.tableView.dequeueReusableCell(withIdentifier: "ExpertiseAddButtonCell", for: indexPath) as! ExpertiseAddButtonCell
                cell.parent = self
                return cell
            case .addExpertise:
                let cell = self!.tableView.dequeueReusableCell(withIdentifier: "ExpertiseAddItemCell", for: indexPath) as! ExpertiseAddItemCell
                return cell
            case .expertise(let expertise):
                let cell = self!.tableView.dequeueReusableCell(withIdentifier: "ExpertiseItemCell", for: indexPath) as! ExpertiseItemCell
                cell.configure(expertise)
                return cell
            }
        }
        table.showLoadingIndicator = false
        table.onSelect = { [weak self] _, item in
            DispatchQueue.main.async {
                guard self != nil else { return }
                switch item {
                case .addExpertise:
                    self!.addExpertise(self!)
                case .expertise(_):
                    showStub()
                default: break
                }
            }
        }
        table.configureSectionHeaderHeight = { section -> CGFloat in
            return table == self.table2 ? 49 : 0
        }
        table.configureHeader = { index, item, section in
            section.titleLabel.text = item
            section.mainView.backgroundColor = Colors.systemGrayBlock
        }
        table.loadSectionItems = { [weak self] callback, failure in
            guard self != nil else { return }
            if table == self?.table { // top section
                callback(self!.topItems, [""])
            }
            else { // a list of my expertise
                callback(self!.items, [NSLocalizedString("My Expertise", comment: "My Expertise").uppercased()])
            }
            tableView.tableFooterView = UIView(frame: .zero)
        }
        table.showLoadingIndicator = false
        table.extraHeight = -1
        table.editActions = { [weak self] indexPath, _ in
            guard self != nil else { return nil }
            guard table == self?.table2 else { return nil }
            let item = self!.items[indexPath.section][indexPath.row]
            switch item {
            case .expertise(let expertise):
                let delete = UIContextualAction(style: .destructive, title: "") { [weak self] (action, view, callback) in
                    self?.remove(expertise: expertise, indexPath: indexPath, callback: callback)
                }
                if #available(iOS 13.0, *) {
                    delete.image = UIImage(systemName: "xmark.circle.fill")
                } else {
                    delete.image = UIImage(named: "iconDelete")
                }
                
                let edit = UIContextualAction(style: .destructive, title: NSLocalizedString("Edit", comment: "Edit")) { [weak self] (action, view, callback) in
                    self?.edit(expertise: expertise)
                    callback(true)
                }
                edit.backgroundColor = UIColor(r: 175, g: 81, b: 222)
                
                let conf = UISwipeActionsConfiguration(actions: [delete, edit])
                conf.performsFirstActionWithFullSwipe = true
                return conf
            default: return nil
            }
        }
        table.bindData(to: tableView)
    }
    
    /// Update UI
    private func updateUI() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        setupNavigationBar(isTransparent: true)
        setupLargeTitle(colored: true)
        table.loadData()
        table2.loadData()
        buttonTopMargin.constant = expertises.isEmpty ? 92 : 20
        infoLabel.isHidden = !expertises.isEmpty
        footerView.backgroundColor = Colors.systemGrayBlock
    }
    
    /// Remove back button
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.navigationBar.tintColor = nil
        navigationController?.navigationBar.barTintColor = nil
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = #imageLiteral(resourceName: "navShadow")
        updateUI()
    }
    
    /// Load data
    private func loadData() {
        DataSource.getCurrentExpertInfo()
            .subscribe(onNext: { [weak self] (expertInfo: ExpertInfoExtended) in
                self?.userInfo = (expertInfo.toExpert(), expertInfo.toUserInfo())
                self?.expertises = expertInfo.expertise
                self?.expertInfo = expertInfo
                self?.supportEnabled = expertInfo.expertAvailability
                self?.reloadTable()
                return
            }, onError: { error in
                DispatchQueue.main.async {
                    showError(errorMessage: error.localizedDescription)
                }
            }).disposed(by: rx.disposeBag)
    }
    
    /// Reload table
    private func reloadTable() {
        updateItems()
        updateUI()
    }
    
    /// Update items
    private func updateItems() {
        topItems.removeAll()
        items.removeAll()
        let topStaticItems: [ExpertiseCellType] = userInfo != nil ? [.user, .support] : []
        if expertises.isEmpty {
//            supportEnabled = false // Resets support flag.
            topItems.append(topStaticItems)
            items.append([ExpertiseCellType.addExpertise])
        }
        else {
            topItems.append(topStaticItems)
            items.append(self.expertises.map({ExpertiseCellType.expertise($0)}) + [ExpertiseCellType.addExpertise])
        }
    }

    /// Update UI when something is changed
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI()
    }
    
    // MARK: - Button actions
    
    /// Enable/disable support
    /// - Parameter enable: true - if need to enable, false - else
    fileprivate func enableSupport(_ enable: Bool) {
        DataSource.enableSupport(enable).subscribe(onNext: { (_) in
        }, onError: { (error) in
            // dodo showError(errorMessage: error.localizedDescription)
        }).disposed(by: self.rx.disposeBag)
        supportEnabled = enable
    }
    
    /// Start adding expertise info
    @IBAction func addExpertise(_ sender: Any) {
        guard let vc = self.create(AssetCollectionViewController.self) else { return }
        vc.usage = .forExpertise
        vc.selectedObjects = SelectedObjects()
        vc.addExpertiseCallback = { [weak self] in
            guard self != nil else { return }
            let e = vc.selectedObjects.convertToExpertise()
            self?.addExpertises(e)
        }
        vc.callback = { _, procedures in
            vc.addExpertiseCallback()
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func addExpertises(_ list: [ExpertisePut]) {
        let complete = { [weak self] (value: [ExpertisePut])->() in
            self?.loadData()
        }
        DataSource.addExpertises(list)
            .subscribe(onNext: { value in
                complete(list)
                return
            }, onError: { error in
                complete(list)
                // dodo showError(errorMessage: error.localizedDescription)
            }).disposed(by: self.rx.disposeBag)
    }
    
    private var confirm: ConfirmDialog?
    /// Remove expertise
    /// - Parameters:
    ///   - expertise: the expertise
    ///   - indexPath: the index path
    private func remove(expertise: Expertise, indexPath: IndexPath, callback: @escaping (Bool)->()) {
        confirm = ConfirmDialog(title: "Delete?", text: "Are you sure you want to delete the expertise?", action: { [weak self] in
            guard self != nil else { return }
            self?.expertises.remove(at: indexPath.row)
            self?.table2.remove(indexPath: indexPath)
            if self!.expertises.isEmpty {
                self?.tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .none)
            }
            self?.updateItems()
            DataSource.removeExpertise(expertise)
                .subscribe(onNext: { _ in
                    return
                }, onError: { error in
                    showError(errorMessage: error.localizedDescription)
                }).disposed(by: self!.rx.disposeBag)
            callback(true)
        })
        confirm?.cancelled = {
            callback(false)
        }
    }
    
    private func edit(expertise: Expertise) {
        let asset = Asset(id: expertise.objectId, name: expertise.title, imageUrl: nil, isFavorite: nil, lastUpdated: nil, procedures: nil, videos: nil, documents: nil, experts: nil)
        guard let vc = create(ProceduresViewController.self) else { return }
        vc.item = asset
        vc.relatedParentItem = nil
        vc.selectedObjects = nil
        vc.usage = .forExpertise
        vc.pathString = expertise.title
        vc.selected = expertInfo?.procedures[expertise.objectId] ?? []
        vc.callback = { [weak self] model, procedures in
            guard self != nil else { return }
            let e = ExpertisePut(assetId: model.id, procedureIds: procedures.map({$0.id}))
            self?.addExpertises([e])
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}

/// Cell for table in this view controller
class ExpertiseUserCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var iconTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var inboxTodayLabel: UILabel!
    @IBOutlet weak var inboxAllLabel: UILabel!
    @IBOutlet var starImages: [UIImageView]!
    @IBOutlet weak var leftBlockView: UIView!
    @IBOutlet weak var rightBlockView: UIView!
    
    /// the related item
    private var item: Expert!
    
    /// Setup UI
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: Expert, ratingInfo: UserInfo) {
        self.item = item
        
        leftBlockView.backgroundColor = Colors.systemGrayBlock
        rightBlockView.backgroundColor = Colors.systemGrayBlock
        leftBlockView.layer.borderWidth = 0.5
        rightBlockView.layer.borderWidth = 0.5
        leftBlockView.layer.borderColor = Colors.systemBlockBorder.cgColor
        rightBlockView.layer.borderColor = Colors.systemBlockBorder.cgColor
        
        iconTitleLabel.text = item.name.abbreviation
        titleLabel.text = item.name
        subtitleLabel.text = ratingInfo.toRatingString()
        inboxTodayLabel.text = ratingInfo.inboxToday.description
        inboxAllLabel.text = ratingInfo.inboxAll.description
        for image in starImages {
            image.image = ratingInfo.rating.getStarImage(index: image.tag)
        }
    }
    
    /// Button action handler
    ///
    /// - parameter sender: the button
    @IBAction func buttonAction(_ sender: Any) {
        showStub()
    }
}

/// Cell for table in this view controller
class ExpertiseSupportToggleCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    
    /// the reference to parent view controller
    fileprivate weak var parent: ExpertiseViewController!
    
    /// Update UI with given data
    /// - Parameter enabled: true - if enabled, false - disabled, nil - not available
    func configure(_ enabled: Bool?) {
        titleLabel.alpha = enabled != nil ? 1 : 0.2
        switchControl.isOn = enabled ?? false
        switchControl.isEnabled = enabled != nil
    }
    
    /// Switcher  action handler
    ///
    /// - parameter sender: the switch
    @IBAction func switchAction(_ sender: UISwitch) {
        parent.enableSupport(sender.isOn)
    }
}

/// Cell for table in this view controller. Shows notification that need to add info first.
class ExpertiseInfoCell: ClearCell {
}

/// Cell for table in this view controller with "Add.." button
class ExpertiseAddButtonCell: ClearCell {
    
    /// the reference to parent view controller
    fileprivate weak var parent: ExpertiseViewController!
    
    /// Button action handler
    ///
    /// - parameter sender: the button
    @IBAction func buttonAction(_ sender: Any) {
        parent.addExpertise(sender)
    }
}

/// Cell for table in this view controller.
class ExpertiseItemCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var rightArrow: UIImageView!
    
    /// Update UI with given data
    /// - Parameter item: the data tos how
    func configure(_ item: Expertise) {
        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
        if #available(iOS 13.0, *) {
            rightArrow.image = UIImage(systemName: "chevron.right")
        }
    }
}

// Section view for expertise table
class ExpertiseSectionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
}

/// Cell for table in this view controller.
class ExpertiseAddItemCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var plusIcon: UIImageView!
    
    override func awakeFromNib() {
        self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.width, bottom: 0, right: 0)
        
        super.awakeFromNib()
        if #available(iOS 13.0, *) {
            plusIcon.image = UIImage(systemName: "plus.circle", withConfiguration: UIImage.SymbolConfiguration(weight: UIImage.SymbolWeight.ultraLight))
        }
    }
}
