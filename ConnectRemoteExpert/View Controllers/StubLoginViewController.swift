//
//  StubLoginViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 10.07.2020.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import AgoraARKit
import SwiftEx
import AgoraRtmKit

let EXPERT_ID = "1"
let TECH_ID = "2"
let CHANNEL_NAME = "3"

// Sample UI for testing Video calls
class StubLoginViewController: UIViewController, AgoraRtmInvitertDelegate, StreamHandler, CallView {

    var provider: ProviderDelegate? = ProviderDelegate.shared
    
    /// the last shown OutgoingCallViewController
    private var lastOutgoingCallScreen: OutgoingCallViewController?
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()

        AgoraARKit.agoraAppId = Configuration.appId
        provider?.callView = self
        provider?.streamHandler = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let call = lastCall {
            lastCall = nil
            provider?.end(call: call)
        }
    }
    
    /// Shows incoming call from user
    func showIncomingCall(invitation: AgoraRtmInvitation) {
        guard let channelName = invitation.channelName else { return }
        let handle = invitation.caller
        // user ID is not needed here
        let userId = ""
        let call = Call(uuid: UUID(), channelName: channelName, userId: userId, userName: handle)
        provider?.startIncomingCall(call: call, phone: nil, email: nil)
    }
    
     // MARK: - CallView
        
    private var lastCall: Call?
    
//    func updateCallUI() {
//        /// Nothing to do
//    }
    
    // MARK: - StreamHandler
    
//    func start(call: Call) {
//        // nothing to do
//    }
    
    /// Show video call screen for "Expert" role
    /// - Parameter call: the call
    func answer(call: Call) {
        // Callee
        lastCall = call
        
        // Caller
        lastCall = call
        if let channel = sendAcceptCallAndGetChannelName() {
            showExpertVideoCall(channel: channel)
        }
    }
    
    /// Dismiss video call screen
    /// - Parameter call: the call
    func end(call: Call) {
        // Caller
        if let popup = lastOutgoingCallScreen {
            popup.dismiss(animated: true, completion: nil)
            lastOutgoingCallScreen = nil
            lastCall = nil
        }
        // Callee
        else {
            if lastCall != nil {
                navigationController?.popToRootViewController(animated: true)
            }
            lastCall = nil
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func decline(call: Call) {
        /// Caller
        if let popup = lastOutgoingCallScreen {
            popup.dismiss(animated: true, completion: nil)
            lastOutgoingCallScreen = nil
            lastCall = nil
            
            guard let inviter = AgoraRtm.shared().inviter else { fatalError("rtm inviter nil") }
            
            inviter.refuseLastIncomingInvitation { (error) in
                showError(errorMessage: error.localizedDescription)
            }
        }
        // Callee
        else {
            if lastCall != nil {
                navigationController?.popToRootViewController(animated: true)
            }
            lastCall = nil
            self.dismiss(animated: true, completion: nil)
            
            guard let inviter = AgoraRtm.shared().inviter else {
                fatalError("rtm inviter nil")
            }
            
            inviter.refuseLastIncomingInvitation { (error) in
                showError(errorMessage: error.localizedDescription)
            }
        }
    }

    // MARK: - RTM Caller
    
    /// Send message that call is accepted
    private func sendAcceptCallAndGetChannelName() -> String? {
        guard let inviter = AgoraRtm.shared().inviter else { fatalError("rtm inviter nil") }
        guard let channelId = inviter.lastIncomingInvitation?.channelName else { fatalError("lastIncomingInvitation content nil") }
        inviter.accpetLastIncomingInvitation()
        return channelId
    }
    
    // MARK: -
    
    @IBAction func loginExpert(_ sender: Any) {
        setupRtm(userId: EXPERT_ID)
        AgoraRtm.shared().inviterDelegate = self
        showAlert("Wait", "Close this dialog and wait for incoming call from other device.")
    }
    
    @IBAction func loginTech(_ sender: Any) {
        setupRtm(userId: TECH_ID)
        AgoraRtm.shared().inviterDelegate = self
        showCall(to: EXPERT_ID, channelName: CHANNEL_NAME)
    }
    
    /// Setup RTM
    private func setupRtm(userId: String) {
        let rtm = AgoraRtm.shared()
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/rtm.log"
        rtm.setLogPath(path)
        
        guard let kit = AgoraRtm.shared().kit else {
            showError(errorMessage: "AgoraRtmKit nil")
            return
        }
        
        // login
        kit.login(account: userId, token: nil) { (error) in
            showError(errorMessage: error.localizedDescription)
        }
        return
    }
    // MARK: - RTM Callee
    
    func inviter(_ inviter: AgoraRtmCallKit, didReceivedIncoming invitation: AgoraRtmInvitation) {
        showIncomingCall(invitation: invitation)
    }
    
    func inviter(_ inviter: AgoraRtmCallKit, remoteDidCancelIncoming invitation: AgoraRtmInvitation) {
        if let call = ProviderDelegate.shared.getLastCall() {
            ProviderDelegate.shared.end(call: call)
        }
        NotificationCenter.post(CallActionNotification.end)
    }
    
    // MARK: - Outgoing Call initiation
    
    /// Call button action
    /// - Parameters:
    ///   - userId: the expert ID
    ///   - channelName: the name of the channel, goes from backend VideoCall
    func showCall(to userId: String, channelName: String) {
        // Notify CallKit
        provider?.startOutgoingCall(userId: userId, userName: "CALLER NAME", channelName: channelName)
        
        // Show outgoing call screen
        guard let vc = create(OutgoingCallViewController.self, storyboardName: Storyboards.videoCall.rawValue) else {
            return
        }
        vc.modalPresentationStyle = .overFullScreen
        navigationController?.present(vc, animated: true, completion: nil)
        lastOutgoingCallScreen = vc
    }
    
    // MARK: - Caller UI
    
    /// Update UI
    func updateCallUI() {
        if let call = provider?.getLastCall() {
            lastOutgoingCallScreen?.updateUI(call: call)
        }
    }
    
    /// Call started (caller)
    /// - Parameter call: the call
    func start(call: Call) {
        lastCall = call
        
        guard let kit = AgoraRtm.shared().kit else { fatalError("rtm kit nil") }
        guard let inviter = AgoraRtm.shared().inviter else { fatalError("rtm inviter nil") }
        
        let closeOutgoingCall = { [weak self] (success: Bool)->() in
            if let popup = self?.lastOutgoingCallScreen {
                popup.dismiss(animated: true) {
                    if success && self?.lastCall != nil {
                        self?.showUserVideo(call: call)
                    }
                }
            }
        }
        // rtm query online status
        kit.queryPeerOnline(call.userId, success: { (onlineStatus) in
            switch onlineStatus {
            case .online:      sendInvitation(call: call)
            case .offline:     print("remote offline")
            case .unreachable: print("remote unreach")
            @unknown default:
                fatalError("queryPeerOnline")
            }
        }) {  (error) in
            showError(errorMessage: error.localizedDescription)
        }
        call.hasStartedConnectingDidChange?()
        
        // rtm send invitation
        func sendInvitation(call: Call) {
            let remoteId = call.userId
            let callerName = call.userName
            let channel = call.channelName
            let content = "{\"channelName\": \"\(channel)\", \"callerName\": \"\(callerName)\"}"
            
            inviter.sendInvitation(peer: remoteId, extraContent: content, accepted: {
                
                call.hasConnectedDidChange?()
                closeOutgoingCall(true)
            }, refused: {
                
                closeOutgoingCall(false)
                // we should call .end(call: ..) here, but current vc will do that when appear again
                print("remote refused this invitation")
            }) { (error) in
               showError(errorMessage: error.localizedDescription)
            }
        }
    }
    
    /// Show video call screen
    /// - Parameter call: the call
    private func showUserVideo(call: Call) {
        self.showUserVideoCall(channel: call.channelName)
    }
}
