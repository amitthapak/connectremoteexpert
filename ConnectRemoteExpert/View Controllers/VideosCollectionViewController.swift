//
//  VideosCollectionViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
import AVKit

/// Videos screen
class VideosCollectionViewController: UIViewController, UISearchBarDelegate, UISearchResultsUpdating, UISearchControllerDelegate {
    
    /// outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    /// the related item
    var item: Asset?
    
    /// collection view data source
    private var dataSource: InfiniteCollectionDataModel<Video, VideoCollectionViewCell, String, UICollectionReusableView>!
    
    /// the search controller
    private var searchController: UISearchController!
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = InfiniteCollectionDataModel<Video, VideoCollectionViewCell, String, UICollectionReusableView>()
        self.dataSource.useSectionHeaders = false
        self.dataSource.configureCell = { (item, indexPath, cell) in
            cell.configure(item)
        }
        self.dataSource.fetchSectionItems = { (offset, limit, callback, failure) in
            guard offset == nil || self.item == nil else {
                callback([], [], offset ?? 0)
                return
            }
            if self.item != nil {
                DataSource.getVideos(model: self.item!)
                    .subscribe(onNext: { value in
                        callback([value], [""], 0)
                        return
                    }, onError: { error in
                        failure(error.localizedDescription)
                    }).disposed(by: self.rx.disposeBag)
            }
            else {
                DataSource.getAllVideos(offset: offset as? Int, limit: limit)
                    .subscribe(onNext: { (value, newOffset) in
                        callback([value], [""], newOffset)
                        return
                    }, onError: { error in
                        failure(error.localizedDescription)
                    }).disposed(by: self.rx.disposeBag)
            }
        }
        
        dataSource.selected = { [weak self] item, indexPath in
            guard self != nil else { return }
            self?.playVideo(item.url)
        }
        dataSource.calculateCellSize = { [weak self] _, _ -> CGSize in
            let width: CGFloat = self!.collectionView.cellWidth(forColumns: 2)
            let h = width + 44 // value for the title, as in design
            return CGSize(width: width, height: h)
        }
        dataSource.bind(collectionView)
        dataSource.loadData()
        
        if let vc = create(VideoListViewController.self) {
            vc.item = self.item
            searchController = UISearchController(searchResultsController: vc)
            searchController.searchResultsUpdater = self
            searchController.searchBar.sizeToFit()
            searchController.searchBar.delegate = self
            searchController.delegate = self
            definesPresentationContext = true
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    /// Setup navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupLargeTitle(colored: true)
        navigationItem.largeTitleDisplayMode = .always
    }
    
    // MARK: - UISearchResultsUpdating
    
    /// Update search results
    /// - Parameter searchController: UISearchController
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text!
        if self.item != nil {
            filterResults(with: searchString, items: dataSource.sectionItems.first ?? []) { [weak self] (list) in
                self?.setSearchResults(list)
            }
        }
        else {
            let searchResultsController = searchController.searchResultsController as! VideoListViewController
            searchResultsController.searchTerm = searchString
        }
        searchController.searchResultsController?.view.isHidden = false
    }
    
    /// Set search results
    /// - Parameter items: the items
    private func setSearchResults(_ items: [Video]) {
        let searchResultsController = searchController.searchResultsController as! VideoListViewController
        searchResultsController.items = items
        searchResultsController.table.loadData()
    }
    
    /// Filter documents by string
    /// - Parameters:
    ///   - searchString: the search string
    ///   - items: the items to filter
    ///   - callback: the callback to return filtered documents
    private func filterResults(with searchString: String, items: [Video], callback: ([Video])->()) {
        // Show all if nothing is entered
        guard !searchString.trim().isEmpty else { callback(items); return }
        
        let searchString = searchString.lowercased()
        let filtered = items.filter({$0.contains(string: searchString)})
        callback(filtered)
    }
}

/// Cell for the collection
class VideoCollectionViewCell: UICollectionViewCell {
    
    /// outlets
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    /// the related item
    private var item: Video!
    /// the reference to parent view controller
    fileprivate var parent: AssetCollectionViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconView.layer.cornerRadius = 5
        iconView.layer.masksToBounds = true
        iconView.layer.borderWidth = 0.5
        iconView.layer.borderColor = UIColor(0xd1d1d6).cgColor
    }
    
    /// Update UI
    /// - Parameters:
    ///   - item: the item to show
    func configure(_ item: Video) {
        self.item = item
        titleLabel.text = item.name
        subtitleLabel.text = item.subtitle
        self.iconView.image = nil
        UIImage.load(Configuration.baseUrl + item.thumbnailUrl) { [weak self] (image) in
            if self?.item.thumbnailUrl == item.thumbnailUrl {
                self?.iconView.image = image
            }
        }
        self.layoutIfNeeded()
    }
}
