//
//  ColorPickerViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/5/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx

/// Color picker popup
class ColorPickerViewController: UIViewController {

    /// outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    
    /// collection data source
    private var dataSource: CollectionDataModel<ColorCell>!
    
    var selectedColor: UIColor = .clear
    var callback: ((UIColor)->())?
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .clear
        collectionView.roundCorners(8)
        mainView.layer.cornerRadius = 10
        mainView.layer.masksToBounds = true
        
        dataSource = CollectionDataModel(collectionView, cellClass: ColorCell.self) { [weak self] (item, indexPath, cell) in
            guard self != nil else { return }
            let item = item as! UIColor
            cell.configure(item, isSelected: item == self?.selectedColor)
        }
        let heightCallback: (CGFloat)->() = { [weak self]  h in
            self?.collectionHeight.constant = h * 10
        }
        dataSource.calculateCellSize = { [weak self]  item, _ -> CGSize in
            guard self != nil else { return .zero }
            let width = self!.collectionView.cellWidth(forColumns: 12)
            let height = width
            heightCallback(height)
            return CGSize(width: width, height: height)
        }
        dataSource.selected = { [weak self] item, indexPath in
            self?.callback?(item as! UIColor)
        }
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.loadData()
        }
    }
    
    /// Load data
    private func loadData() {
        let n = 12 // columns
        let m = 5 // half of the color rows (ceil(9/2))
        var colors = [UIColor]()
        for i in 0..<n {
            let value: CGFloat = 1 - CGFloat(i) / CGFloat(n-1)
            let color = UIColor(white: value, alpha: 1)
            colors.append(color)
        }
        
        let hues: [CGFloat] = [194, 215, 249, 281, 338, 5, 19, 35, 43, 56, 64, 91].map({$0/360})
        // B: [0.2->1], S=0.8
        for i in 0..<m {
            for j in 0..<n {
                var value: CGFloat = CGFloat(i) / CGFloat(m-1) // 0-1
                value = value * 0.8 + 0.2
                colors.append(UIColor(hue: hues[j], saturation: 0.8, brightness: value, alpha: 1))
            }
        }
        // B=1, S=(0.8->0.2]
        for i in 1..<m {
            for j in 0..<n {
                var value: CGFloat = CGFloat(i) / CGFloat(m-1) // 0-1
                value = 1 - value // 1-0
                value = value * (0.8-0.2) + 0.2
                colors.append(UIColor(hue: hues[j], saturation: value, brightness: 1, alpha: 1))
            }
        }
        dataSource.setItems(colors)
    }
}

/// Cell for the collection in this screen
class ColorCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var height: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        borderView.addBorder(color: .white, borderWidth: 3)
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    ///   - isSelected: true - if selected
    func configure(_ item: UIColor, isSelected: Bool) {
        self.bgView.backgroundColor = item
        borderView.isHidden = !isSelected
        width.constant = self.bounds.width + 3
        height.constant = self.bounds.height + 3
        if isSelected {
            delay(0) {
                self.superview?.bringSubviewToFront(self)
            }
        }
    }
}
