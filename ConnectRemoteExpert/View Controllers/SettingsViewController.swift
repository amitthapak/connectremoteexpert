//
//  SettingsViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
//import UIComponents
import MSAL

/// Model for settings item
struct SettingsItem {
    let title: String
    let icon: UIImage
    let action: ()->()
}

/// Settings screen
class SettingsViewController: UIViewController {

    /// outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    
    /// the table model
    private var table = SectionInfiniteTableViewModel<Any, UITableViewCell, SettingsSectionHeader>()
    
    private var skipReload = false
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        table.preConfigure = { indexPath, item, _ -> UITableViewCell in
            if let user = item as? User {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "SettingsTopCell", for: indexPath) as! SettingsTopCell
                cell.configure(user)
                return cell
            }
            else if let item = item as? SettingsItem {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "SettingsItemCell", for: indexPath) as! SettingsItemCell
                cell.configure(item)
                return cell
            }
            else {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "SettingsLogoutCell", for: indexPath) as! SettingsLogoutCell
                return cell
            }
        }
        table.sectionHeaderHeight = 36
        table.onSelect = { indexPath, item in
            guard indexPath.section > 0 else { return }
            DispatchQueue.main.async {
                switch indexPath.section {
                case 1, 2: // Feedback
                    (item as? SettingsItem)?.action()
                case 3: // Sign out
                    AuthenticationUtil.cleanUp()
                    NotificationCenter.post(LoginNotifications.logout)
                    UIViewController.getCurrentViewController()?.dismiss(animated: true, completion: nil)
                default:
                    showStub()
                }
            }
        }
        table.loadSectionItems = { [weak self] callback, failure in
            guard self != nil else { return }
            DataSource.getCurrentUser()
                .subscribe(onNext: { [weak self] user in
                    var iconFeedback: UIImage!
                    var iconPrivacy: UIImage!
                    if #available(iOS 13.0, *) {
                        iconFeedback = UIImage(systemName: "paperplane.fill")
                        iconPrivacy = UIImage(systemName: "hand.raised.fill")
                    }
                    else {
                        iconFeedback = UIImage(named: "iconFeedback")
                        iconPrivacy = UIImage(named: "iconPrivacy")
                    }
                    callback([[user],
                              [SettingsItem(title: "Feedback", icon: iconFeedback, action: { [weak self] in
                                self?.openFeedback()
                              })],
                              [SettingsItem(title: "About", icon: UIImage(named: "iconLogo")!, action: { [weak self] in
                                self?.openAbout()
                              }),
                               SettingsItem(title: "Privacy Policy", icon: iconPrivacy, action: { [weak self] in
                                self?.openPrivacyPolicy()
                               })],
                              ["logout"]],
                             ["user","feedback","info", "logout"])
                    return
                }, onError: { error in
                    DispatchQueue.main.async {
                        failure(error.localizedDescription)
                    }
                }).disposed(by: self!.rx.disposeBag)
        }
        table.configureHeader = { _, _, section in
            section.mainView.backgroundColor = Colors.systemGrayBlock
        }
        table.tableHeight = tableHeight
    }
    
    /// Setup navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
        if !skipReload {
            table.bindData(to: tableView) // placed here to reload every time
        }
        skipReload = false
    }
        
    /// Update UI
    private func updateUI() {
        setupNavigationBar(isTransparent: true, buttonColor: Colors.blueColor)
        navigationController?.setNavigationBarHidden(false, animated: true)
        setupLargeTitle(colored: true)
        footerView.backgroundColor = Colors.systemGrayBlock
        
        /// Allow scrolling on small screens
        if tableHeight.constant > self.view.bounds.height {
            tableView.isScrollEnabled = true
        }
    }
    
    /// Open Feedback
    private func openFeedback() {
        skipReload = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .plain, target: self, action: nil)
        guard let vc = create(FeedbackViewController.self) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Open About
    private func openAbout() {
        skipReload = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: "Back"), style: .plain, target: self, action: nil)
        guard let vc = create(AboutViewController.self) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Open Privacy Policy
    private func openPrivacyPolicy() {
        skipReload = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: "Back"), style: .plain, target: self, action: nil)
        guard let vc = create(PolicyViewController.self) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Update UI if dark mode is on/off
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                updateUI()
                self.table.loadData()
            }
        }
        else {
            updateUI()
        }
    }
}

/// Cell for table in this view controller
class SettingsTopCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var iconTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    /// the related item
    private var item: User!
    
    /// Setup UI
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: User) {
        self.item = item
        iconTitleLabel.text = item.name.abbreviation
        titleLabel.text = item.name
        subtitleLabel.text = item.email
    }
}

/// Section header
class SettingsSectionHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var mainView: UIView!
}

/// Logout cell for the last row
class SettingsLogoutCell: ClearCell {
}

/// Cell for table in this view controller
class SettingsItemCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconContainerView: GradientView!
    @IBOutlet weak var iconView: UIImageView!
    
    /// the shown item
    private var item: SettingsItem!
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: SettingsItem) {
        self.item = item
        titleLabel.text = item.title
        
        self.iconView.image = item.icon
        self.iconView.tintColor = .white
    }
}

