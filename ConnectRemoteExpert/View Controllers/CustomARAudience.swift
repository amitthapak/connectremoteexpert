//
//  CustomARAudience.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import AgoraARKit
import AgoraRtcKit
import SwiftEx

/// Experts screen. Shows remote camera on a full screen and allows to draw.
class CustomARAudience: UIViewController, AgoraRtcEngineDelegate, UIGestureRecognizerDelegate {
    
    // MARK: UI properties
    
    //  list of colors that user can choose from
    private let uiColors: [UIColor] = [UIColor.systemBlue, UIColor.systemGray, UIColor.systemGreen, UIColor.systemYellow, UIColor.systemRed]
    var lineColor: UIColor! {                 // active color to use when drawing
        didSet { drawingToolsVC?.selectedColor = self.lineColor }
    }
    private let lineSizes: [CGFloat] = [0.0025, 0.005, 0.0075, 0.015, 0.020]
    var lineWidthIndex: Int = 1
    var lineAlpha: Float = 1
    private var objectIndex: Int = 0
    private var objectLineWidthIndex: Int = 0
    private var objectLineAlpha: Float = 1
    private let bgColor: UIColor = .white           // set the view bg color
    
    var drawingView: UIView!                // view to draw all the local touches
    var remoteVideoView: UIView!            // video stream from remote user
    var localVideoView: UIView!             // video stream from local user
    private var capturePhotoButton: UIButton!
    private var drawingToolsVC: ToolsViewController?
    
    // Drawing
    var touchStart: CGPoint!                // keep track of the initial touch point of each gesture
    var touchPoints: [CGPoint]!             // for drawing touches to the screen
    var dataPointsArray: [CGPoint] = []     // batch list of touches to be sent to remote user
    
    var remoteViewBackgroundColor: UIColor = .lightGray // the background color for the UIView until the remote video stream is received. Defaults to `.lightGray`
    
    // MARK: Agora properties
    private var agoraKit: AgoraRtcEngineKit!                            // Agora.io Video Engine reference
    var channelName: String!        // name of the channel to join
    var channelProfile: AgoraChannelProfile = .communication    // Channel profile
    var defaultToSpeakerPhone: Bool = true  // Flag to determine whether the device is enabled to speaker mode when entering the channel. Defaults to `true`
    
    private var sessionIsActive = false             // keep track if the video session is active or not
    private var remoteUser: UInt?                   // remote user id
    var dataStreamId: Int! = 27             // id for data stream
    private var streamIsEnabled: Int32 = -1         // acts as a flag to keep track if the data stream is enabled
    
    private var microphoneMuted = false     // true - if sound is off, false - else
    private var drawingActive = false       // true - if drawing panel is shown
    private var colorSelectorShown = false
    private var numberOfCommands: (Int,Int) = (0,0) // the number of commands to undo/redo
    
    /// the used camera: front/rear
    private var cameraDirection: AgoraCameraDirection = .front
    
    /// true - if eraser is on, false - else
    private var isEraserTool: Bool {
        return drawingToolsVC?.isEraserTool ?? false
    }
    
    /// true - if highlighter is on, false - else
    private var isHighlighter: Bool {
        return drawingToolsVC?.isHighlighter ?? false
    }
    
    private var activityIndicator: ActivityIndicator?
    
    // Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        lprint("AudienceVC - viewDidLoad", .Verbose)
        
        self.lineColor = self.uiColors.first
        self.view.backgroundColor = self.bgColor
        
        createUI()
        setupGestures()
        setupAgora()
        setupLocalVideo()
        setupNavigationBar()
        activityIndicator = ActivityIndicator(parentView: self.view).start()
        delay(30) {
            self.removeLoadingIndicator()
        }
    }
    
    /// Remove loading indicator
    private func removeLoadingIndicator() {
        self.activityIndicator?.stop()
        self.activityIndicator = nil
    }
        
    func setupAgora() {
        guard let agoraAppID = AgoraARKit.agoraAppId else { return }
        // Add Agora setup
        let agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: agoraAppID, delegate: self) // - init engine
        agoraKit.setChannelProfile(channelProfile) // - set channel profile
        if channelProfile == .liveBroadcasting {
            agoraKit.setClientRole(.audience)
        }
        self.agoraKit = agoraKit
    }
    
    /// AgoraARKit joins the Agora channel within the `viewDidAppear`
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lprint("AudienceVC - viewDidAppear", .Verbose)
        joinChannel() // Agora - join the channel
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        leaveChannel()
    }
    
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge {
        return .bottom
    }
    
    // Set video configuration
    private func setupLocalVideo() {
        // Enable the video
        agoraKit.enableVideo()
        
        // Set video configuration
        let videoConfig = AgoraVideoEncoderConfiguration(size: AgoraVideoDimension360x360, frameRate: .fps15, bitrate: AgoraVideoBitrateStandard, orientationMode: .fixedPortrait)
        self.agoraKit.setVideoEncoderConfiguration(videoConfig)
        let configuration = AgoraCameraCapturerConfiguration()
        configuration.cameraDirection = cameraDirection
        self.agoraKit.setCameraCapturerConfiguration(configuration)
        
        // Setup local video
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = 0
        videoCanvas.view = localVideoView
        videoCanvas.renderMode = .hidden
        // Set the local video view.
        agoraKit.setupLocalVideo(videoCanvas)
        
        guard let videoView = localVideoView.subviews.first else { return }
        videoView.layer.cornerRadius = 25
    }
    
    /// Settings can be applied only before user joins the channel
    private func applyNewSettings() {
        leaveChannel()
        setupLocalVideo()
        joinChannel()
    }
    
    // MARK: Agora Interface
    
    /// Conencts to the Agora channel, and sets the default audio route to speakerphone
    func joinChannel() {
        // Set audio route to speaker
        self.agoraKit.setDefaultAudioRouteToSpeakerphone(defaultToSpeakerPhone)
        // Join the channel
        print("Join the channel: \(String(describing: self.channelName))")
        self.agoraKit.joinChannel(byToken: AgoraARKit.agoraToken, channelId: self.channelName, info: nil, uid: 0)
        UIApplication.shared.isIdleTimerDisabled = true     // Disable idle timmer
    }
    
    func leaveChannel() {
        lprint("leaveChannel", .Verbose)
        UIApplication.shared.isIdleTimerDisabled = false
        guard self.agoraKit != nil else { return }
        self.agoraKit.leaveChannel(nil)                     // leave channel and end chat
        self.sessionIsActive = false                        // session is no longer active
        updateUI()
    }
    
    // MARK: - UI
    
    /// Programmatically generated UI, creates the view, and buttons.
    func createUI() {
        lprint("createUI", .Verbose)
        
        addRemoteView()
        addDrawingView()
        addLocalView()
        addDrawingControls()
        addPhotoCaptureButton()
        
        updateUI()
    }
    
    // add remote video view
    private func addRemoteView() {
        let remoteView = UIView()
        remoteView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        remoteView.backgroundColor = remoteViewBackgroundColor
        remoteView.contentMode = .scaleAspectFit
        self.view.loadViewWithConstraints(remoteView)
        self.remoteVideoView = remoteView
    }
    
    // ui view that the finger drawings will appear on
    private func addDrawingView() {
        let drawingView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        self.view.insertSubview(drawingView, at: 1)
        self.drawingView = drawingView
    }
    
    // add local video view
    private func addLocalView() {
        let localViewScale = self.view.frame.width * 0.33
        let localView = UIView()
        localView.frame = CGRect(x: self.view.frame.maxX - (localViewScale+17.5), y: self.view.frame.maxY - (localViewScale+25), width: localViewScale, height: localViewScale)
        localView.contentMode = .scaleAspectFill
        localView.layer.cornerRadius = 25
        localView.layer.masksToBounds = true
        localView.backgroundColor = UIColor.darkGray
        self.view.insertSubview(localView, at: 2)
        self.localVideoView = localView
    }
    
    private var toolsViewController: VideoControlsViewController!
    /// the height of the tools panel
    private var toolsHeightConstraint: NSLayoutConstraint!
    private var toolsPanelState: VideoControlsState = .opened
    
    /// Add drawing controls
    private func addDrawingControls() {
        if let vc = UIStoryboard(name: Storyboards.videoCall.rawValue, bundle: nil).instantiateViewController(withIdentifier: "VideoControlsViewController") as? VideoControlsViewController {
            toolsHeightConstraint = loadAtTheBottom(vc, self.view, bottom: 0, withHeight: 114)
            let applyState: (VideoControlsState)->() = { [weak self] state in
                self?.toolsPanelState = state
                UIView.defaultAnimation {
                    vc.updateUI(withState: state)
                    self?.toolsHeightConstraint.constant = state.toHeight()
                    self?.view.layoutIfNeeded()
                }
                self?.capturePhotoButton.isHidden = state == .closed
            }
            vc.callbackPainting = { [weak self] in
                self?.openDrawingPanel()
                applyState(.opened)
            }
            vc.callbackSwitchCamera = { [weak self] in
                self?.cameraDirection = self!.cameraDirection.switch()
                self?.applyNewSettings()
            }
            vc.callbackPause = {
                showStub()
            }
            vc.callbackMicMute = { [weak self] mute in
                self?.muteMicrophone(muted: mute)
            }
            vc.callbackFinish = {
                self.backButtonAction()
            }
            
            vc.callbackSwipe = { [weak self] up in
                if let state = self?.toolsPanelState.applySwipe(up: up) {
                    applyState(state)
                }
            }
            vc.callbackSwipeButtonTap = { [weak self] in
                if let state = self?.toolsPanelState.applyTap() {
                    applyState(state)
                }
            }
            vc.callbackDisableCamera = { disable in
                showStub()
            }
            toolsPanelState = .opened
            toolsViewController = vc
        }
        
        // Add tools panel
        if let vc = UIStoryboard(name: Storyboards.videoCall.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ToolsViewController") as? ToolsViewController {
            vc.selectedColor = self.lineColor
            vc.toolActionCallback = { [weak self] index in
                self?.removeAllPopups()
                if index <= 3 { // Not color selector
                    self?.sendToolCommand(index)
                }
                else {
                    self?.showColorSelection(show: !self!.colorSelectorShown)
                }
                if index == 0 || index == 1 {
                    self?.showWidthSelector()
                }
                else if index == 2 {
                    self?.showEraserSettings()
                }
                else if index == 3 {
                    self?.showObjectSettings()
                }
            }
            self.loadViewControllerAtTheBottom(vc, self.view)
            self.drawingToolsVC = vc
            self.drawingToolsVC?.view.isHidden = true
        }
    }
    
    /// Add "Photo Capture" button
    /// Must be called after `addDrawingControls`
    private func addPhotoCaptureButton() {
        // Add drawing button
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: DrawingConstants.photoCaptureButtonSize.width, height: DrawingConstants.photoCaptureButtonSize.width)
        button.setImage(#imageLiteral(resourceName: "photoCapture"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(capturePhotoAction), for: .touchDown)
        button.translatesAutoresizingMaskIntoConstraints = false
        self.view.insertSubview(button, at: 3)
        let padding = CGFloat(10)
        button.bottomAnchor.constraint(equalTo: self.toolsViewController.view.topAnchor, constant: -padding).isActive = true
        button.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: padding).isActive = true
        button.widthAnchor.constraint(equalToConstant: DrawingConstants.photoCaptureButtonSize.width).isActive = true
        button.heightAnchor.constraint(equalToConstant: DrawingConstants.photoCaptureButtonSize.height).isActive = true
        self.capturePhotoButton = button
    }
    
    /// Show width selector
    private func showWidthSelector() {
        removeAllPopups()
        if let vc = UIStoryboard(name: Storyboards.videoCall.rawValue, bundle: nil).instantiateViewController(withIdentifier: "WidthSelectorViewController") as? WidthSelectorViewController {
            vc.selectedIndex = self.lineWidthIndex
            vc.selectedAlpha = self.lineAlpha
            vc.selectedColor = self.lineColor
            vc.isHighlighter = self.isHighlighter
            self.loadViewControllerAtTheBottom(vc, self.view, bottom: self.view.bounds.height - (self.drawingToolsVC?.view.frame.origin.y ?? 100))
            vc.callback = { [weak self] index, alpha in
                
                self?.lineWidthIndex = index
                self?.lineAlpha = alpha
                let settings = LineSettings(width: self!.lineSizes[index], alpha: CGFloat(alpha))
                self?.send(lineWidthSettings: settings)
                self?.removeAllPopups()
            }
            vc.callbackAlpha = { [weak self] alpha in
                self?.lineAlpha = alpha
            }
            self.widthSelectorVC = vc
        }
    }
    
    /// Show object settings
    private func showObjectSettings() {
        removeAllPopups()
        if let vc = UIStoryboard(name: Storyboards.videoCall.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ObjectSettingsViewController") as? ObjectSettingsViewController {
            vc.selectedObject = self.objectIndex
            vc.selectedIndex = self.objectLineWidthIndex
            vc.selectedAlpha = self.objectLineAlpha
            vc.selectedColor = self.lineColor
            vc.isHighlighter = true
            
            self.loadViewControllerAtTheBottom(vc, self.view, bottom: self.view.bounds.height - (self.drawingToolsVC?.view.frame.origin.y ?? 100))
            vc.callbackObject = { [weak self] objectIndex, lineIndex, alpha in
                self?.objectIndex = objectIndex
                self?.objectLineWidthIndex = lineIndex
                self?.objectLineAlpha = alpha
                let settings = ObjectSettings(objectIndex: objectIndex, width: self!.lineSizes[lineIndex], alpha: CGFloat(alpha))
                self?.send(objectSettings: settings)
                self?.removeAllPopups()
            }
            self.widthSelectorVC = vc
        }
    }
    
    /// Show color selector
    private func showColorSelector() {
        removeAllPopups()
        if let vc = UIStoryboard(name: Storyboards.videoCall.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ColorPickerViewController") as? ColorPickerViewController {
            vc.selectedColor = self.lineColor
            self.loadViewControllerAtTheBottom(vc, self.view, bottom: self.view.bounds.height - (self.drawingToolsVC?.view.frame.origin.y ?? 100))
            vc.callback = { [weak self] color in
                self?.set(color: color)
                self?.removeAllPopups()
            }
            self.colorPickerVC = vc
        }
    }
    
    /// true - if eraser removes pixels, false - objects
    private var isPixelEraser = true
    
    /// Show Eraser Settings
    private func showEraserSettings() {
        removeAllPopups()
        if let vc = UIStoryboard(name: Storyboards.videoCall.rawValue, bundle: nil).instantiateViewController(withIdentifier: "EraserSettingsViewController") as? EraserSettingsViewController {
            vc.isPixelEraser = isPixelEraser
            self.loadViewControllerAtTheBottom(vc, self.view, bottom: self.view.bounds.height - (self.drawingToolsVC?.view.frame.origin.y ?? 100))
            vc.callback = { [weak self] isPixelEraser in
                self?.isPixelEraser = isPixelEraser
                self?.removeAllPopups()
            }
            self.eraserSettingsVC = vc
        }
    }
    
    /// Remove all popups
    private func removeAllPopups() {
        widthSelectorVC?.remove()
        widthSelectorVC = nil
        colorPickerVC?.remove()
        colorPickerVC = nil
        eraserSettingsVC?.remove()
        eraserSettingsVC = nil
    }
    
    private var eraserSettingsVC: EraserSettingsViewController?
    private var widthSelectorVC: WidthSelectorViewController?
    private var colorPickerVC: ColorPickerViewController?
    
    // MARK: - Update UI
    
    /// Update UI
    private func updateUI() {
        let showDrawingTools = drawingActive && sessionIsActive
        drawingActive = showDrawingTools // update if disconnected while drawing
        updateUINavigationButtons()
        
        // disable/enable button in tools
        toolsViewController?.updateDrawingButton(enabled: sessionIsActive)
        drawingToolsVC?.view.isHidden = !showDrawingTools
        setupGestures()
        
        localVideoView.isHidden = showDrawingTools
        updateUIMicrophoneButton()
        toolsViewController?.view.isHidden = drawingActive
    }
    
    /// Setup navigation bar
    private func setupNavigationBar() {
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .systemBlue
        
    }
    
    /// Update naivgation bar buttons
    private func updateUINavigationButtons() {
        let showDrawingTools = drawingActive
        if showDrawingTools {
            let undo = UIBarButtonItem(image:  #imageLiteral(resourceName: "undo"), style: .plain, target: self, action: #selector(sendUndoCommand))
            let sync = UIBarButtonItem(image:  #imageLiteral(resourceName: "syncButton"), style: .plain, target: self, action: #selector(syncButtonAction))
            let redo = UIBarButtonItem(image:  #imageLiteral(resourceName: "redo"), style: .plain, target: self, action: #selector(sendRedoCommand))
            undo.tintColor = numberOfCommands.0 > 0 ? UIColor.white : .systemGray
            sync.tintColor = .systemGray
            redo.tintColor = numberOfCommands.1 > 0 ? UIColor.white : .systemGray
            navigationItem.leftBarButtonItems = [
                undo,
                sync,
                redo
            ]
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneDrawingAction))
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        else {
            navigationController?.setNavigationBarHidden(true, animated: true)
            navigationItem.leftBarButtonItems = []
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    /// Back button action
    override func backButtonAction() {
        if let call = ProviderDelegate.shared.getLastCall() {
            ProviderDelegate.shared.end(call: call)
            delay(2) { // fixing issue with CallKit when it thinks that there are no calls
                super.backButtonAction()
            }
        }
        else {
            super.backButtonAction()
        }
    }
    
    /// Update microphone button state
    private func updateUIMicrophoneButton() {
        toolsViewController?.updateMicrophoneButton(enabled: sessionIsActive)
    }
    
    // MARK: - Button Actions
    
    /// Dismiss the current view
    @IBAction func popView() {
        leaveChannel()
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Mute/unmute microphone
    /// - Parameter muted: true - need to mute, false - else
    func muteMicrophone(muted: Bool) {
        if muted {
            self.agoraKit.muteLocalAudioStream(true)
            lprint("disable active mic")
        } else {
            self.agoraKit.muteLocalAudioStream(false)
            lprint("enable mic")
        }
        updateUIMicrophoneButton()
    }
    
    /// Show color selectors
    func showColorSelection(show: Bool) {
        self.colorSelectorShown = show
        
        if show {
            showColorSelector()
        }
        else {
            removeAllPopups()
        }
    }
    
    /// Color button action handler
    @IBAction func setColor(_ sender: UIButton) {
        set(color: sender.backgroundColor!)
    }
    
    /// Set color
    /// - Parameter color: the color
    private func set(color: UIColor) {
        self.lineColor = color
        
        showColorSelection(show: false)
        // send data message with color components
        if self.streamIsEnabled == 0 {
            guard let colorString = color.toString() else { return }
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: colorString.data(using: String.Encoding.ascii)!)
        }
    }
    
    /// Sync button action
    @IBAction func syncButtonAction() {
    }
    
    /// Send Undo command
    @IBAction func sendUndoCommand() {
        if self.streamIsEnabled == 0 && numberOfCommands.0 > 0 {
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: "undo".data(using: String.Encoding.ascii)!)
        }
    }
    
    /// Send Redo command
    @IBAction func sendRedoCommand() {
        if self.streamIsEnabled == 0 && numberOfCommands.1 > 0 {
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: "redo".data(using: String.Encoding.ascii)!)
        }
    }
    
    func send(lineWidthSettings settings: LineSettings) {
        if self.streamIsEnabled == 0 {
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: settings.encode().ascii)
        }
    }
    
    func send(objectSettings settings: ObjectSettings) {
        if self.streamIsEnabled == 0 {
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: settings.encode().ascii)
        }
    }
    
    /// "Drawing" button action
    @IBAction func openDrawingPanel() {
        drawingActive = true
        updateUI()
    }
    
    /// "Done" button action
    @IBAction func doneDrawingAction() {
        removeAllPopups()
        drawingActive = false
        updateUI()
    }
    
    /// "Photo capture" button action
    @IBAction func capturePhotoAction() {
        let image = UIImage.imageFromView(view: self.remoteVideoView)
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print("ERROR: \(error)")
        }
        else {
            showAlert("Screenshot saved", "The screenshot is saved in your Photo Library.")
        }
    }
    
    // MARK: -
    
    func sendToolCommand(_ index: Int) {
        if self.streamIsEnabled == 0 {
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: "tool:\(index)".data(using: String.Encoding.ascii)!)
        }
    }
    
    // MARK: - AgoraRtcEngineDelegate
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        // first remote video frame
        lprint("firstRemoteVideoDecoded for Uid: \(uid)")
        removeLoadingIndicator()
        // limit sessions to two users
        if self.remoteUser == uid {
            guard let remoteView = self.remoteVideoView else { return }
            let videoCanvas = AgoraRtcVideoCanvas()
            videoCanvas.uid = uid
            videoCanvas.view = remoteView
            videoCanvas.renderMode = .hidden
            agoraKit.setupRemoteVideo(videoCanvas)
            
            self.sessionIsActive = true
            DispatchQueue.main.async {
                self.updateUI()
            }
            
            // create the data stream
            self.streamIsEnabled = self.agoraKit.createDataStream(&self.dataStreamId, reliable: true, ordered: true)
            lprint("Data Stream initiated - STATUS: \(self.streamIsEnabled)")
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        lprint("error: \(errorCode.rawValue)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        lprint("warning: \(warningCode.rawValue)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        lprint("local user did join channel with uid:\(uid)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        lprint("remote user did joined of uid: \(uid)")
        if self.remoteUser == nil {
            self.remoteUser = uid // keep track of the remote user
            lprint("remote host added")
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        lprint("remote user did offline of uid: \(uid)")
        if uid == self.remoteUser {
            self.remoteUser = nil
            
            // Automatically go back
            self.backButtonAction()
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didAudioMuted muted: Bool, byUid uid: UInt) {
        // TODO add logic to show icon that remote stream is muted
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, receiveStreamMessageFromUid uid: UInt, streamId: Int, data: Data) {
        // successfully received message from user
        guard let dataAsString = String(bytes: data, encoding: String.Encoding.ascii) else { return }
        
        lprint("STREAMID: \(streamId)\n - DATA: \(data)\n - STRING: \(dataAsString)\n")
        
        // check data message
        switch dataAsString {
        case let dataString where dataString.contains("color:"):    // received when expert rolls back the color
            lprint("color msg received\n - \(dataString)")
            guard let color = UIColor.from(string: dataString) else { return }
            lineColor = color
        case let string where string.contains("buttons-sync:"):
            let a = string.split(separator: ":")
            if a.count == 3 {
                numberOfCommands = (Int(a[1]) ?? 0, Int(a[2]) ?? 0)
                self.updateUINavigationButtons()
            }
        default:
            lprint("Unknown command: 👆")
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurStreamMessageErrorFromUid uid: UInt, streamId: Int, error: Int, missed: Int, cached: Int) {
        // message failed to send(
        lprint("STREAMID: \(streamId)\n - ERROR: \(error)")
    }
    
    
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, reportRtcStats stats: AgoraChannelStats) {
        let sent = stats.txKBitrate
        let received = stats.rxKBitrate
        self.toolsViewController.updateStats(sent: sent, received: received)
    }
    
    // MARK: - Gestures
    
    private var panGesture: UIPanGestureRecognizer?
    
    /// Setup gestures
    internal func setupGestures() {
        if drawingActive {
            if panGesture == nil {
                // pan gesture
                let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
                panGesture.delegate = self
                self.view.addGestureRecognizer(panGesture)
                self.panGesture = panGesture
            }
        }
        else if let panGesture = panGesture {
            self.view.removeGestureRecognizer(panGesture)
            self.panGesture = nil
        }
    }
    
    // get the initial touch event
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeAllPopups()
        // get the initial touch event
        if self.sessionIsActive && self.drawingActive, let touch = touches.first {
            let position = touch.location(in: self.remoteVideoView)
            self.touchStart = position
            self.touchPoints = []
            lprint("\(position)", .Verbose)
        }
        if colorSelectorShown {
            showColorSelection(show: false)
        }
    }
    
    /// Pan gesture action
    @IBAction func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        let rView: UIView = remoteVideoView ?? self.view
        if self.sessionIsActive && self.drawingActive && gestureRecognizer.state == .began && self.streamIsEnabled == 0 {
            // send message to remote user that touches have started
            self.agoraKit.sendStreamMessage(self.dataStreamId, data: "touch-start:\(rView.bounds.width):\(rView.bounds.height)".data(using: String.Encoding.ascii)!)
        }
        
        if self.sessionIsActive && self.drawingActive && (gestureRecognizer.state == .began || gestureRecognizer.state == .changed) {
            guard rView.bounds.width > 0 && rView.bounds.height > 0 else { return }
            let translation = gestureRecognizer.translation(in: rView)
            // calculate touch movement relative to the superview
            guard let touchStart = self.touchStart else { return } // ignore accidental finger drags
            let pixelTranslation = CGPoint(x: touchStart.x + translation.x, y: touchStart.y + translation.y)
            
            // normalize the touch point to use view center as the reference point
            let pointToSend = CGPoint(x: pixelTranslation.y / rView.bounds.height, y: 1 - pixelTranslation.x / rView.bounds.width)
            
            self.touchPoints.append(pixelTranslation)
            
            if self.streamIsEnabled == 0 {
                // send data to remote user
                self.dataPointsArray.append(pointToSend)
                if self.dataPointsArray.count == 10 {
                    sendTouchPoints() // send touch data to remote user
                    clearSubLayers() // remove touches drawn to the screen
                }
                
                lprint("streaming data: \(pointToSend)\n - STRING: \(self.dataPointsArray)\n - DATA: \(self.dataPointsArray.description.data(using: String.Encoding.ascii)!)")
            }
            
            DispatchQueue.main.async {
                guard !self.isEraserTool else { return }
                // draw user touches to the DrawView
                guard let drawView = self.drawingView else { return }
                guard let lineColor: UIColor = self.lineColor else { return }
                let layer = CAShapeLayer()
                layer.path = UIBezierPath(roundedRect: CGRect(x:  pixelTranslation.x, y: pixelTranslation.y, width: 25, height: 25), cornerRadius: 25).cgPath
                layer.fillColor = lineColor.cgColor
                drawView.layer.addSublayer(layer)
            }
            
            lprint("\(pointToSend)")
            lprint("\(pixelTranslation)")
        }
        
        if gestureRecognizer.state == .ended {
            // send message to remote user that touches have ended
            if self.streamIsEnabled == 0 {
                // transmit any leftover points
                if self.dataPointsArray.count > 0 {
                    sendTouchPoints() // send touch data to remote user
                    clearSubLayers() // remove touches drawn to the screen
                }
                self.agoraKit.sendStreamMessage(self.dataStreamId, data: "touch-end".data(using: String.Encoding.ascii)!)
            }
            // clear list of points
            if let touchPointsList = self.touchPoints {
                self.touchStart = nil // clear starting point
                lprint("\(touchPointsList)")
            }
        }
    }
    
    /// Send touch points
    func sendTouchPoints() {
        let pointsAsString: String = self.dataPointsArray.description
        self.agoraKit.sendStreamMessage(self.dataStreamId, data: pointsAsString.data(using: String.Encoding.ascii)!)
        self.dataPointsArray = []
    }
    
    /// Clean sublayers
    func clearSubLayers() {
        DispatchQueue.main.async {
            // loop through layers drawn from touches and remove them from the view
            guard let sublayers = self.drawingView.layer.sublayers else { return }
            for layer in sublayers {
                layer.isHidden = true
                layer.removeFromSuperlayer()
            }
        }
    }
}

extension AgoraCameraDirection {
    
    func `switch`() -> AgoraCameraDirection {
        switch self {
        case .front:
            return .rear
        case .rear:
            return .front
        }
    }
}

extension UIView {
    
    /// Apply default animation
    /// - Parameter animaions: the animaitons callback
    static func defaultAnimation(animaions: @escaping ()->()) {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: animaions, completion: nil)
    }
}
