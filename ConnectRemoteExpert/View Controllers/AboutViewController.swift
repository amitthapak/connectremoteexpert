//
//  AboutViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// About screen
class AboutViewController: UIViewController {

    /// outlets
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        iconView.roundCorners(18)
    }
    
    /// Update UI
    private func updateUI() {
        self.view.backgroundColor = Colors.systemBackground
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        versionLabel.text = NSLocalizedString("Version", comment: "Version") + " \(version)"
    }
    
    // dodo GET /about
}
