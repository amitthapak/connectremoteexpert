//
//  EraserSettingsViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/5/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// Eraser Settings
class EraserSettingsViewController: UIViewController {
    
    /// outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    /// true - if pixel eraser is selected, false - else
    var isPixelEraser: Bool = true
    
    /// the callback: true - if pixer eraser
    var callback: ((Bool)->())?
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10
        mainView.layer.masksToBounds = true
        button1.roundCorners(6)
        button2.roundCorners(6)
        button1.superview?.roundCorners(8)
        
        self.view.backgroundColor = UIColor.clear
        updateUI()
    }
    
    /// Update UI
    private func updateUI() {
        button1.backgroundColor = isPixelEraser ? .white : .clear
        button2.backgroundColor = !isPixelEraser ? .white : .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let button = isPixelEraser ? button1 : button2
        let view = button?.addShadowView(opacity: 0.2)
        view?.backgroundColor = .clear
    }
    
    /// button action handler
    ///
    /// - parameter sender: the button
    @IBAction func buttonActions(_ sender: UIButton) {
        callback?(sender.tag == 0)
    }
    
}
