//
//  LoginViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/4/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import AgoraARKit
import MSAL
import SwiftEx
import AgoraRtmKit
import SwiftyJSON

public enum CallActionNotification: String {
    case end, completed
}

public enum LoginNotifications: String {
    case logout
}

/// Login screen
class LoginViewController: UIViewController, CallView, StreamHandler, AgoraRtmInvitertDelegate {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    var provider: ProviderDelegate? = ProviderDelegate.shared
    
    private var mainViewController: UIViewController?
    
    let kClientID = "d947f78a-4914-4491-b980-a6554a967989"
    let kAuthority = "https://login.microsoftonline.com/872c5047-fb6d-4fc2-b81b-f01f82b4a017/v2.0"
    let kScopes: [String] = ["api://d947f78a-4914-4491-b980-a6554a967989/Ios.Read"]
        
    private var applicationContext: MSALPublicClientApplication?
    private var webViewParameters: MSALWebviewParameters?
    private var currentAccount: MSALAccount?
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        AgoraARKit.agoraAppId = Configuration.appId
        
        do {
            try self.initMSAL()
        } catch let error {
            self.updateLogging(text: "Unable to create Application Context \(error)")
        }
        
        self.platformViewDidLoadSetup()
        RestServiceApi.UNAUTHORIZED_CODES = [401, 403]
        RestServiceApi.callback401 = { [weak self] in
            AuthenticationUtil.cleanUp()
            NotificationCenter.post(LoginNotifications.logout)
            DispatchQueue.main.async {
                self?.moveToLoginScreen()
            }
        }
        NotificationCenter.add(observer: self, selector: #selector(notificationHandler(_:)), name: LoginNotifications.logout)
    }
    
    /// Move to login screen
    private func moveToLoginScreen() {
        guard let vc = UIViewController.getCurrentViewController() else { return }
        if vc is UIAlertController {
            vc.dismiss(animated: false) { [weak self] in
                self?.moveToLoginScreen()
            }
        }
        else {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    
    /// Logout when user taps "Sign out"
    /// - Parameter notification: the notification
    @objc func notificationHandler(_ notification: Notification) {
        if notification.name.rawValue == LoginNotifications.logout.rawValue {
            logoutFromAccount()
        }
    }
    
    /// Setup RTM
    private func setupRtm() {
        DataSource.getCurrentUser()
            .subscribe(onNext: { value in
                let userId = value.id
                
                let rtm = AgoraRtm.shared()
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/rtm.log"
                rtm.setLogPath(path)
                
                guard let kit = AgoraRtm.shared().kit else {
                    showError(errorMessage: "AgoraRtmKit nil")
                    return
                }
                
                // login
                kit.login(account: userId, token: nil) { (error) in
                    showError(errorMessage: error.localizedDescription)
                }
                return
            }, onError: { error in
                showError(errorMessage: error.localizedDescription)
            }).disposed(by: rx.disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let call = lastCall {
            lastCall = nil
            provider?.end(call: call)
        }
        
        DispatchQueue.main.async {
            self.signInAction(self)
        }
    }
    
    /// Shows incoming call from user
    func showIncomingCall(invitation: AgoraRtmInvitation) {
        guard let jsonString = invitation.content else { return }
        let json =  JSON(parseJSON: jsonString)
        guard let channelName = json["channelName"].string,
            let handle = json["callerName"].string else {
                return
        }
        let userId = invitation.caller
        let call = Call(uuid: UUID(), channelName: channelName, userId: userId, userName: handle)
        provider?.startIncomingCall(call: call, phone: nil, email: nil)
    }
    
    /// Show main screen
    func showMainScreen() {
        guard let vc = createInitialViewController(fromStoryboard: "Main") else {
            return
        }
        vc.modalTransitionStyle = .flipHorizontal
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: { [weak self] in
            self?.setLoggingState(false)
        })
        mainViewController = vc
    }
    
    // MARK: - CallView
    
    private var lastCall: Call?
    
    func updateCallUI() {
        /// Nothing to do
    }
    
    // MARK: - StreamHandler
    
    func start(call: Call) {
        // nothing to do
    }
    
    /// Show video call screen for "Expert" role
    /// - Parameter call: the call
    func answer(call: Call) {
        lastCall = call
//        if let channel = sendAcceptCallAndGetChannelName() {
//            showExpertVideoCall(channel: channel)
//        }
    }
    
    /// Dismiss video call screen
    /// - Parameter call: the call
    func end(call: Call) {
        if lastCall != nil {
            navigationController?.popToRootViewController(animated: true)
        }
        lastCall = nil
        mainViewController?.dismiss(animated: true, completion: nil)
        mainViewController = nil
    }
    
    func decline(call: Call) {
        if lastCall != nil {
            navigationController?.popToRootViewController(animated: true)
        }
        lastCall = nil
        mainViewController?.dismiss(animated: true, completion: nil)
        mainViewController = nil
        
        guard let inviter = AgoraRtm.shared().inviter else {
            fatalError("rtm inviter nil")
        }
        
        inviter.refuseLastIncomingInvitation { (error) in
            showError(errorMessage: error.localizedDescription)
        }
    }
    
    // MARK: - MSAL
    
    func initMSAL() throws {
        guard let authorityURL = URL(string: kAuthority) else {
            self.updateLogging(text: "Unable to create authority URL")
            return
        }
        let authority = try MSALAADAuthority(url: authorityURL)
        
        let msalConfiguration = MSALPublicClientApplicationConfig(clientId: kClientID, redirectUri: nil, authority: authority)
        self.applicationContext = try MSALPublicClientApplication(configuration: msalConfiguration)
        self.initWebViewParams()
    }
    
    private func initWebViewParams() {
        self.webViewParameters = MSALWebviewParameters(authPresentationViewController: self)
    }
    
    func platformViewDidLoadSetup() {
        NotificationCenter.default.addObserver(self,
                            selector: #selector(appCameToForeGround(notification:)),
                            name: UIApplication.willEnterForegroundNotification,
                            object: nil)
    }
        
    @objc func appCameToForeGround(notification: Notification) {
        self.loadCurrentAccount()
    }
    
    // MARK: - Sign In
    
    @IBAction func signInAction(_ sender: Any) {
        setLoggingState(true)
        self.loadCurrentAccount { (account) in
            guard let currentAccount = account, UserDefaults.isAuthenticated else {
                // We check to see if we have a current logged in account.
                // If we don't, then we need to sign someone in.
                self.acquireTokenInteractively()
                return
            }
            self.acquireTokenSilently(currentAccount)
        }
    }
    
    private func logoutFromAccount() {
        getAccount(callback: { [weak self] (currentAccount) in
            if let currentAccount = currentAccount, let webParams = self?.webViewParameters {
                let params = MSALSignoutParameters(webviewParameters: webParams)
                self?.applicationContext?.signout(with: currentAccount, signoutParameters: params) { (_, error) in
                    if let error = error {
                        print("ERROR: \(error)")
                        return
                    }
                }
            }
        }, failure: { (error) in
            print("ERROR: \(error)")
        })
    }
    
    private func setLoggingState(_ set: Bool) {
        loginButton.isEnabled = !set
        statusLabel.text = set ? "Logging in..." : ""
    }
    
    private func setAccessToken(_ token: String) {
        AuthenticationUtil.processAccessToken(token)
        if !token.isEmpty {
            setupRtm()
            AgoraRtm.shared().inviterDelegate = self
        }
    }

    typealias AccountCompletion = (MSALAccount?) -> Void

    func loadCurrentAccount(completion: AccountCompletion? = nil) {
        getAccount(callback: { [weak self] (account) in
            if let currentAccount = account {
                self?.updateLogging(text: "Found a signed in account \(String(describing: currentAccount.username)). Updating data for that account...")
                self?.updateCurrentAccount(account: currentAccount)
                if let completion = completion {
                    completion(currentAccount)
                }
                return
            }
            self?.updateLogging(text: "Account signed out. Updating UX")
            self?.setAccessToken("")
            self?.updateCurrentAccount(account: nil)
            
            if let completion = completion {
                completion(nil)
            }
        }) { (error) in
            self.updateLogging(text: "Couldn't query current account with error: \(error)")
        }
    }
    
    /// Get account
    private func getAccount(tryAgain: Bool = true, callback: @escaping (MSALAccount?)->(), failure: @escaping (Error)->()) {
        guard let applicationContext = self.applicationContext else { return }
        let msalParameters = MSALParameters()
        msalParameters.completionBlockQueue = DispatchQueue.main
        
        applicationContext.getCurrentAccount(with: msalParameters, completionBlock: { [weak self] (currentAccount, previousAccount, error) in
            if let error1 = error {
                if tryAgain {
                    do {
                        for account in try applicationContext.allAccounts() {
                            try applicationContext.remove(account)
                        }
                        self?.getAccount(tryAgain: false, callback: callback, failure: failure)
                    }
                    catch let error2 {
                        print("ERROR: \(error2)")
                        failure(error1)
                    }
                }
                else {
                    failure(error1)
                }
                return
            }
            callback(currentAccount)
        })
    }
    
    // MARK: - Get token
    
    func acquireTokenInteractively() {
        guard let applicationContext = self.applicationContext else { return }
        guard let webViewParameters = self.webViewParameters else { return }
        // #1
        let parameters = MSALInteractiveTokenParameters(scopes: kScopes, webviewParameters: webViewParameters)
        parameters.promptType = UserDefaults.isAuthenticated ? .selectAccount : .login
        // #2
        applicationContext.acquireToken(with: parameters) { (result, error) in
            // #3
            if let error = error {
                self.setLoggingState(false)
                self.updateLogging(text: "Could not acquire token: \(error)")
                return
            }
            guard let result = result else {
                self.setLoggingState(false)
                self.updateLogging(text: "Could not acquire token: No result returned")
                return
            }
                
            // #4
            self.setAccessToken(result.accessToken)
            self.updateLogging(text: "Access token is \(result.accessToken)")
            self.updateCurrentAccount(account: result.account)
            self.completeTokenUpdate()
        }
    }
    
    func acquireTokenSilently(_ account : MSALAccount!) {
        guard let applicationContext = self.applicationContext else { return }
        /**
         Acquire a token for an existing account silently
         
         - forScopes:           Permissions you want included in the access token received
         in the result in the completionBlock. Not all scopes are
         guaranteed to be included in the access token returned.
         - account:             An account object that we retrieved from the application object before that the
         authentication flow will be locked down to.
         - completionBlock:     The completion block that will be called when the authentication
         flow completes, or encounters an error.
         */
        let parameters = MSALSilentTokenParameters(scopes: kScopes, account: account)
        applicationContext.acquireTokenSilent(with: parameters) { (result, error) in
            if let error = error {
                let nsError = error as NSError
                // interactionRequired means we need to ask the user to sign-in. This usually happens
                // when the user's Refresh Token is expired or if the user has changed their password
                // among other possible reasons.
                if (nsError.domain == MSALErrorDomain) {
                    if (nsError.code == MSALError.interactionRequired.rawValue) {
                        DispatchQueue.main.async {
                            self.acquireTokenInteractively()
                        }
                        return
                    }
                }
                self.updateLogging(text: "Could not acquire token silently: \(error)")
                return
            }
            guard let result = result else {
                self.updateLogging(text: "Could not acquire token: No result returned")
                return
            }
            self.setAccessToken(result.accessToken)
            self.updateLogging(text: "Refreshed Access token is \(result.accessToken)")
            self.updateSignOutButton(enabled: true)
            self.completeTokenUpdate()
        }
    }
    
    private var initialTokenUpdate = true
    
    private func completeTokenUpdate() {
        if initialTokenUpdate {
            initialTokenUpdate = false
            DispatchQueue.main.async {
                self.showMainScreen()
            }
        }
    }
    
    // MARK: - Update UI
    
    func updateLogging(text : String) {
        if Thread.isMainThread {
            print(text)
        } else {
            DispatchQueue.main.async {
                print(text)
            }
        }
    }
    
    func updateSignOutButton(enabled : Bool) {
//        if Thread.isMainThread {
//            self.signOutButton.isEnabled = enabled
//        } else {
//            DispatchQueue.main.async {
//                self.signOutButton.isEnabled = enabled
//            }
//        }
    }
    
    func updateAccountLabel() {
        guard let currentAccount = self.currentAccount else {
//            self.usernameLabel.text = "Signed out"
            print("Username: Signed out")
            return
        }
//        self.usernameLabel.text = currentAccount.username
        print("Username: \(currentAccount.username ?? "-")")
    }
    
    func updateCurrentAccount(account: MSALAccount?) {
        self.currentAccount = account
        self.updateAccountLabel()
        self.updateSignOutButton(enabled: account != nil)
    }
    
    // MARK: - Get device info
    
    @objc func getDeviceMode(_ sender: AnyObject) {
        if #available(iOS 13.0, *) {
            self.applicationContext?.getDeviceInformation(with: nil, completionBlock: { (deviceInformation, error) in
                guard let deviceInfo = deviceInformation else {
                    self.updateLogging(text: "Device info not returned. Error: \(String(describing: error))")
                    return
                }
                let isSharedDevice = deviceInfo.deviceMode == .shared
                let modeString = isSharedDevice ? "shared" : "private"
                self.updateLogging(text: "Received device info. Device is in the \(modeString) mode.")
            })
        } else {
            self.updateLogging(text: "Running on older iOS. GetDeviceInformation API is unavailable.")
        }
    }
    
    // MARK: - AgoraRtmInvitertDelegate
    
    func inviter(_ inviter: AgoraRtmCallKit, didReceivedIncoming invitation: AgoraRtmInvitation) {
        showIncomingCall(invitation: invitation)
    }
    
    func inviter(_ inviter: AgoraRtmCallKit, remoteDidCancelIncoming invitation: AgoraRtmInvitation) {
        if let call = ProviderDelegate.shared.getLastCall() {
            ProviderDelegate.shared.end(call: call)
        }
        NotificationCenter.post(CallActionNotification.end)
    }
}
