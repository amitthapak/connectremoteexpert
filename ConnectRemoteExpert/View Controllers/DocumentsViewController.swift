//
//  DocumentsViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
//import UIComponents

/// Documents screen
class DocumentsViewController: UIViewController, UISearchBarDelegate, UISearchResultsUpdating {

    /// outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    /// the table model
    private var table = InfiniteTableViewModel<Document, DocumentCell>()
    
    /// the related item
    var item: Asset?
    
    /// true - if view controller is embeded (used for search results), false - else
    var isEmbeded = false
    
    /// the search string
    var searchString: String?
    
    /// the search controller
    private var searchController: UISearchController!
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInsetAdjustmentBehavior = .never
        table.configureCell = { indexPath, item, _, cell in
            cell.configure(item)
        }
        table.onSelect = { [weak self] _, item in
            DispatchQueue.main.async {
                self?.view.endEditing(true)
                guard let url = item.url.getUrl() else { showError(errorMessage: "Cannot open URL \"\(String(describing: item.url))\""); return }
                self?.downloadDocument(url: url, name: "\(item.name ?? Int.rand().description).\(item.getMimeType())")
            }
        }
        table.fetchItems = { [weak self] (offset, limit, callback, failure) in
            guard self != nil else { return }
            if let item = self?.item {
                if let offset = offset {
                    callback([], offset)
                }
                else {
                    DataSource.getDocuments(model: item)
                    .subscribe(onNext: { value in
                        callback(value, 0)
                        return
                    }, onError: { error in
                        failure(error.localizedDescription)
                    }).disposed(by: self!.rx.disposeBag)
                }
            }
            else {
                let page = offset as? Int ?? 1
                DataSource.getAllDocuments(page: page, name: self?.searchString)
                .subscribe(onNext: { value in
                    callback(value.item, page + 1)
                    return
                }, onError: { error in
                    failure(error.localizedDescription)
                }).disposed(by: self!.rx.disposeBag)
            }
            
        }
        if isEmbeded {
            table.showLoadingIndicator = false
        }
        table.bindData(to: tableView)
        
        if !isEmbeded {
            if let vc = create(DocumentsViewController.self) {
                vc.isEmbeded = true
                vc.item = item
                searchController = UISearchController(searchResultsController: vc)
                searchController.searchResultsUpdater = self
                searchController.searchBar.sizeToFit()
                searchController.searchBar.delegate = self
                definesPresentationContext = true
                navigationItem.searchController = searchController
                navigationItem.hidesSearchBarWhenScrolling = false
            }
        }
        loadData()
    }
    
    /// Setup navigation bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isEmbeded {
            setupLargeTitle(colored: true)
            navigationItem.largeTitleDisplayMode = .always
        }
    }
    
    /// Remove background color from navigation bar
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        setupLargeTitle(colored: false)
    }
    
    /// Load data
    private func loadData() {
        table.loadData()
    }
    
    // MARK: - UISearchResultsUpdating
    
    /// Update search results
    /// - Parameter searchController: UISearchController
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text!.trim()
        let searchResultsController = searchController.searchResultsController as! DocumentsViewController
        if !searchString.isEmpty {
            searchResultsController.searchString = searchString
        }
        searchResultsController.loadData()
        searchController.searchResultsController?.view.isHidden = false
    }
}

/// Cell for table in this view controller
class DocumentCell: ClearCell {
    
    /// outlets
    @IBOutlet weak var iconContainerView: RoundView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    /// the related item
    private var item: Video!
    
    /// Setup UI
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        iconContainerView.addBorder(color: UIColor(0xdfdfdf), borderWidth: 0.5)
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: Document) {
        titleLabel.text = item.name
        subtitleLabel.text = item.subtitle
        
        self.iconView.image = item.icon
    }
    
    /// Update UI with given data
    ///
    /// - Parameters:
    ///   - item: the data to show in the cell
    func configure(_ item: Video) {
        self.item = item
        titleLabel.text = item.name
        subtitleLabel.text = item.subtitle
        
        self.iconView.image = nil
        UIImage.load(Configuration.baseUrl +  item.thumbnailUrl) { [weak self] (image) in
            if self?.item.thumbnailUrl == item.thumbnailUrl {
                self?.iconView.image = image
            }
        }
    }
}

