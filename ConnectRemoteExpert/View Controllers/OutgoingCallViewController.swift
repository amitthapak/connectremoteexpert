//
//  OutgoingCallViewController.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/4/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
import AgoraRtmKit

/// Outgoing call screen
class OutgoingCallViewController: UIViewController {

    /// outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    /// true - if initial screen opening, false - else
    private var initial: Bool = true
    
    /// Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel?.text = ""
        hidesBottomBarWhenPushed = true
        self.view.backgroundColor = UIColor.clear
    }

    /// Update UI
    func updateUI(call: Call) {
        titleLabel?.text = call.userName
    }

    /// "Cancel" button action handler
    ///
    /// - parameter sender: the button
    @IBAction func cancelButtonAction(_ sender: Any) {
        cancelInvitation()
        if let call = ProviderDelegate.shared.getLastCall() {
            ProviderDelegate.shared.end(call: call)
            delay(1) { // fixing issue with CallKit when it thinks that there are no calls
                if ProviderDelegate.shared.getLastCall()?.channelName == call.channelName {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func cancelInvitation() {
        guard let inviter = AgoraRtm.shared().inviter else {
            fatalError("rtm inviter nil")
        }
        
        let errorHandle: ErrorCompletion = { (error: AGEError) in
            showError(errorMessage: error.localizedDescription)
        }
        
        switch inviter.status {
        case .outgoing:
            inviter.cancelLastOutgoingInvitation(fail: errorHandle)
        default:
            break
        }
    }
    
}
