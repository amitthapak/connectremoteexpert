//
//  Configuration.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// App configuration
class Configuration {
    
    /// data
    var dict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "configuration", ofType: "plist")!)

    /// singleton
    static let sharedInstance = Configuration()
    
    /// the App ID
    static var appId: String {
        return sharedInstance.dict!["appId"] as! String
    }

    /// the baseURL
    static var baseUrl: String {
        return sharedInstance.dict!["baseUrl"] as! String
    }

    /// the interval used to update "Discover" table
    static var refreshInterval: TimeInterval {
        return (sharedInstance.dict!["refreshInterval"] as! NSNumber).doubleValue
    }
    
}
