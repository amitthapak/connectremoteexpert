//
//  UIExtensions.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/16/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import SwiftEx
import AVKit

/// UIViewController extension with helpful methods
extension UIViewController {
    
    /// Setup large title
    func setupLargeTitle(colored: Bool) {
        if UIScreen.main.traitCollection.userInterfaceStyle == .light {
            self.view.backgroundColor = colored ? Colors.backgroundColor : Colors.systemBackgroundLight
            self.navigationController?.navigationBar.backgroundColor = nil
        }
        else {
            self.view.backgroundColor = Colors.systemBackgroundDark
            self.navigationController?.navigationBar.backgroundColor = nil
        }
    }
    
    /// Setup custom large title
    func setupCustomLargeTitle(colored: Bool, titleView: UIView) {
        if colored {
            let bgColor = UIScreen.main.traitCollection.userInterfaceStyle == .light ? Colors.backgroundColor : Colors.systemBackgroundLight
            setupNavigationBar(isTransparent: false, bgColor: bgColor)
            titleView.backgroundColor = bgColor
        }
        else {
            setupNavigationBar(isTransparent: true)
            titleView.backgroundColor = Colors.systemBackgroundDark
        }
        navigationController!.navigationBar.tintColor = nil
    }
    
    /// Show alert "Thank You!"
    func showThanksForFeedback(callback: @escaping ()->()) {
        let alert = UIAlertController(title: NSLocalizedString("Thank You!", comment: "Thank You!"), message: NSLocalizedString("Your feedback will help us improve the experience.", comment: "Your feedback will help us improve the experience."), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: "Close"), style: .cancel,
                                      handler: { (_) -> Void in
                                        alert.dismiss(animated: true, completion: nil)
                                        DispatchQueue.main.async {
                                            callback()
                                        }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

/// default number formetter
var defaultNumberFormatter: NumberFormatter {
    let f = NumberFormatter()
    f.numberStyle = .decimal
    f.groupingSeparator = ","
    f.decimalSeparator = "."
    return f
}

/// file size number formetter
var fileSizeNumberFormatter: NumberFormatter {
    let f = NumberFormatter()
    f.numberStyle = .decimal
    f.groupingSeparator = ","
    f.maximumFractionDigits = 1
    f.decimalSeparator = "."
    return f
}


extension Date {
    
    /// default date parser
    public static let defaultParser: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return df
    }()
    
    /// default date formatter
    public static let defaultFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yy"
        return df
    }()
}

/// Helpful extension for downloading files and playing videos
extension UIViewController: UIDocumentInteractionControllerDelegate {
    
    /// Download file
    ///
    /// - Parameters:
    ///   - url: the URL
    ///   - name: the name
    func downloadDocument(url: URL, name: String) {
        let fileUrl = FileUtil.getLocalFileURL(name)
        if FileManager.default.fileExists(atPath: fileUrl.path) {
            self.showFile(url: fileUrl, loadingView: nil)
        }
        else {
            let loadingView = showActivityIndicator()
            DispatchQueue.global(qos: .background).async {
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    guard let data = data, error == nil else {
                        DispatchQueue.main.async {
                            loadingView?.stop()
                            showError(errorMessage: error?.localizedDescription ?? "Unknown error")
                        }
                        return
                    }
                    if let fileUrl = FileUtil.save(fileName: name, data: data) {
                        self.showFile(url: fileUrl, loadingView: loadingView)
                    }
                }) .resume()
            }
        }
    }
    
    /// Show downloaded file by local URL
    ///
    /// - Parameter url: URL
    private func showFile(url: URL, loadingView: ActivityIndicator?) {
        DispatchQueue.main.async {
            loadingView?.stop()
            let document = UIDocumentInteractionController(url: url)
            document.delegate = self.parent
            if !document.presentPreview(animated: true) {
                self.showAlert(NSLocalizedString("Cannot preview", comment: "Cannot preview"), "\(url) " + NSLocalizedString("file preview cannot be opened", comment: "file preview cannot be opened"), completion: nil)
                return
            }
            
            DispatchQueue.main.async {
                self.navigationController?.navigationBar.barTintColor = .white
                self.navigationController?.navigationBar.isTranslucent = false
            }
        }
    }
    
    // MARK: - UIDocumentInteractionControllerDelegate
    
    /// Preview opened in current view controller
    public func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    /// Play video
    /// - Parameter urlString: the URL
    func playVideo(_ urlString: String) {
        self.view.endEditing(true)
        guard let url = urlString.getUrl() else { showError(errorMessage: "Cannot play video URL \"\(urlString)\""); return }
        playVideo(url)
    }
    
    /// Play video
    /// - Parameter url: the URL
    func playVideo(_ url: URL) {
        let avController = AVPlayerViewController()
        avController.showsPlaybackControls = true
        self.present(avController, animated: true, completion: nil)
        
        var playerUtil: PlayerUtil?
        
        playerUtil = PlayerUtil({ (player) in
            if player != nil {
                avController.player = player
                playerUtil?.destroy()       // remove references from player and playerItem
                player?.play()
            }
        }, uiCallback: {(state, time, total) in
            print("state=\(state)")
        })
        
        playerUtil?.play(url: url)
    }
}

/// Helpful method in String
extension String {
    
    /// Convert HTML to NSAttributedString
    func convertHtml() -> NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
            let string = NSMutableAttributedString(attributedString: attributedString)
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 2
            paragraphStyle.alignment = .left
            string.addAttributes([.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attributedString.length))
            
            // Apply text color
            if #available(iOS 13.0, *) {
                string.addAttributes([.foregroundColor: UIColor.label], range: NSRange(location: 0, length: attributedString.length))
            } else {
                string.addAttributes([.foregroundColor: UIColor.black], range: NSRange(location: 0, length: attributedString.length))
            }
            
            // Update fonts
            let regularFont = UIFont.systemFont(ofSize: 12, weight: .regular)
            let boldFont = UIFont.systemFont(ofSize: 12, weight: .semibold)
            
            string.enumerateAttribute(.font, in: NSMakeRange(0, attributedString.length), options: NSAttributedString.EnumerationOptions(rawValue: 0), using: { (value, range, stop) -> Void in
                
                /// Update to our font
                // Bold font
                if let oldFont = value as? UIFont, oldFont.fontName.lowercased().contains("bold") {
                    string.removeAttribute(.font, range: range)
                    string.addAttribute(.font, value: boldFont, range: range)
                }
                    // Default font
                else {
                    string.addAttribute(.font, value: regularFont, range: range)
                }
            })
            return string
        }
        return NSAttributedString()
    }
}

extension UILabel {
    
    /// Create NSAttributedString with blue link to "privacy policy"
    func createPrivacyPolicyAttributedString() -> NSAttributedString {
        let font = self.font!
        
        let s1 = "Your system information and email address will be sent with your feedback. See the "
        let s2 = "privacy policy"
        let s3 = "."
        
        let string = NSMutableAttributedString(string: s1 + s2 + s3, attributes: [.font : font, .foregroundColor: UIColor(0x838389)])
        let range1 = NSMakeRange(s1.length, s2.length)
        string.addAttributes([.foregroundColor: UIColor.systemBlue], range: range1)
        return string
    }
}

extension UIViewController {
    
    /// Show video call screen for a user
    /// - Parameter channel: the channel name
    func showUserVideoCall(channel: String) {
        let arBroadcastVC = CustomARBroadcaster()
        arBroadcastVC.channelName = channel
        arBroadcastVC.hidesBottomBarWhenPushed = true
        
        getNavigationController()?.pushViewController(arBroadcastVC, animated: true)
    }
    
    
    /// Show expert's video call screen
    /// - Parameter channel: the channel name
    func showExpertVideoCall(channel: String) {
        let arAudienceVC = CustomARAudience()
        arAudienceVC.channelName = channel
        arAudienceVC.hidesBottomBarWhenPushed = true
        getNavigationController()?.pushViewController(arAudienceVC, animated: true)
    }
    
    func getNavigationController() -> UINavigationController? {
        if let navVC = (UIViewController.getCurrentViewController() as? UITabBarController)?.selectedViewController as? UINavigationController {
            return navVC
        }
        else {
            return navigationController
        }
    }
}

extension UITextField {
    
    var textValue: String {
        return text ?? ""
    }
}
