//
//  PlayerUtil.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer

/// Player state
enum PlayerState: String {
    case playing
    case paused
    case stopped
}

var LastPlayerUtil: PlayerUtil?

/// The player utility
class PlayerUtil: NSObject {
    
    private var callback: ((AVPlayer?)->())?
    private var uiCallback: ((PlayerState, TimeInterval?, TimeInterval?)->())?
    
    /// player and player item
    var player: AVPlayer?
    private var playerItem: AVPlayerItem!
    
    /// the player observer
    private var periodicObserver: Any?
    
    /// Player status context. Used for listerning events.
    private var itemStatusContext: String = "itemStatusContext"
    
    /// the completion callback
    var completionCallback: (()->())?
    
    /// the player state
    private var playerState: PlayerState = PlayerState.stopped {
        didSet {
            if oldValue != playerState {
                self.updateUIWithPlayerState(playerState, nil)
            }
        }
    }
    
    init(_ callback: @escaping (AVPlayer?)->(), uiCallback: @escaping (PlayerState, TimeInterval?, TimeInterval?)->()) {
        self.callback = callback
        self.uiCallback = uiCallback
        super.init()
        LastPlayerUtil = self
    }
    
    // loading state
    private var loading = false
    
    /// "Play/Pause" action
    func togglePlay() {
        if playerState == .playing {
            self.pause()
        } else {
            self.play()
        }
    }
    
    /// Stop playback
    func stop() {
        self.player?.pause()
        //        destroy()
        playerState = .stopped
    }
    
    func destroy() {
        if let periodicObserver = periodicObserver {
            self.player?.removeTimeObserver(periodicObserver)
            self.periodicObserver = nil
        }
        self.playerItem?.removeObserver(self, forKeyPath: "status")
        self.playerItem = nil
        self.player = nil
        callback?(nil)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    /// Pause playback
    func pause() {
        player?.pause()
        playerState = .paused
    }
    
    /// Play playback
    func play() {
        if !loading {
            player?.play()
            playerState = .playing
        }
    }
    
    /// Play script record
    ///
    /// - Parameters:
    ///   - irl: the URL to play
    ///   - soundLevel: the sound level
    ///   - start: the start of the playback
    ///   - end: the end of the playback
    func play(url: URL, soundLevel: Float? = nil, start: TimeInterval? = nil, end: TimeInterval? = nil) {
        loadAssetFromFile(url: url, soundLevel: soundLevel, start: start, end: end)
    }
    
    /// Loads file and plays
    ///
    /// - Parameters:
    ///   - url: the URL
    ///   - soundLevel: the sound level
    ///   - start: the start of the playback
    ///   - end: the end of the playback
    func loadAssetFromFile(url: URL, soundLevel: Float? = nil, start: TimeInterval? = nil, end: TimeInterval? = nil) {
        guard !loading else { return }
        loading = true
        let asset = AVURLAsset(url: url, options: nil)
        let tracksKey = "tracks"
        var endTime: CMTime?
        if let end = end {
            endTime = CMTimeMake(value: Int64(end * 600), timescale: 600)
        }
        asset.loadValuesAsynchronously(forKeys: [tracksKey], completionHandler: { () -> Void in
            
            DispatchQueue.main.async {
                var error: NSError?
                let status = asset.statusOfValue(forKey: tracksKey, error: &error)
                
                if status == AVKeyValueStatus.loaded {
                    self.playerItem = AVPlayerItem(asset: asset)
                    self.playerItem.addObserver(self, forKeyPath: "status",
                                                options: NSKeyValueObservingOptions.initial,
                                                context: nil)
                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.playerItem, queue: nil, using: { _ in
                        DispatchQueue.main.async {
                            self.player?.seek(to: CMTime.zero)
                            self.stop()
                            self.completionCallback?()
                        }
                    })
                    self.player = AVPlayer(playerItem: self.playerItem)
                    
                    var startTime = CMTime.zero
                    if let start = start {
                        startTime = CMTimeMake(value: Int64(start * 600), timescale: 600)
                    }
                    self.player?.seek(to: startTime)
                    self.periodicObserver = self.player?.addPeriodicTimeObserver(
                        forInterval: CMTimeMake(value: 150, timescale: 600),
                        queue: DispatchQueue.main,
                        using: {(time: CMTime) in
                            let t: TimeInterval? = self.cmTimeToTimeInterval(time)
                            self.updateUI(t)
                            if let endTime = endTime {
                                let res = CMTimeCompare(time, endTime)
                                if res >= 0 {
                                    self.stop()
                                }
                            }
                    })
                    self.callback?(self.player)
                }
                else {
                    print("The asset's tracks were not loaded:\n" + (error?.localizedDescription ?? "nil"));
                }
                self.loading = false
            }
        })
    }
    
    func seek(_ time: TimeInterval) {
        let startTime = CMTimeMake(value: Int64(time * 600), timescale: 600)
        self.player?.seek(to: startTime)
        if self.playerState != .playing {
            self.play()
        }
    }
    
    /// Player Item event handler
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        DispatchQueue.main.async {
            self.play()
        }
        return
    }
    
    /// Update UI with given current playback progress time
    internal func updateUI(_ time: TimeInterval?) {
        updateUIWithPlayerState(playerState, time)
    }
    
    /// Get the playback time
    ///
    /// - Returns: the time
    func playerItemDuration() -> CMTime {
        if let playerItem: AVPlayerItem = self.player?.currentItem, playerItem.status == .readyToPlay {
            return playerItem.duration
        }
        return CMTime.invalid
    }
    
    /// Convert CMTime to TimeInterval
    ///
    /// - Parameter time: CMTime
    /// - Returns: TimeInterval
    func cmTimeToTimeInterval(_ time: CMTime) -> TimeInterval? {
        let seconds = CMTimeGetSeconds(time)
        if seconds.isNaN {
            return nil
        }
        return TimeInterval(seconds)
    }
    
    /// Update UI with given state
    ///
    /// - Parameter state: the state
    func updateUIWithPlayerState(_ state: PlayerState, _ time: TimeInterval?) {
        DispatchQueue.main.async { [weak self] in
            var total: TimeInterval?
            if let t = self?.playerItem?.duration {
                total = self?.cmTimeToTimeInterval(t)
            }
            self?.uiCallback?(state, time, total)
        }
    }
    
    /// Set volume level
    func setVolume(_ level: Float) {
        player?.volume = level
    }
}
