//
//  AuthenticationUtil.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/4/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SwiftEx

/// Utility that helps handle authentication and store credentials (will be in future; current just stores the differences between the user roles)
class AuthenticationUtil {
    
    static func processAccessToken(_ token: String) {
        UserDefaults.token = token
        UserDefaults.isAuthenticated = true
        DataSource.headers = [DataSource.AUTH_HEADER: "Bearer \(token)"]
    }
    
    static func isAuthenticated() -> Bool {
        return UserDefaults.isAuthenticated
    }
    
    static var user: User? {
        guard let res = UserDefaults.token else { return nil }
        let list = getClaims(fromToken: res)
        let email = list["preferred_username"] as? String ?? ""
        let name = list["name"] as? String ?? "-"
        let id = ""
        return User(id: id, name: name, email: email, role: nil)
    }
    
    /// Get Claims from the token
    /// - Parameter token: the token
    static func getClaims(fromToken token: String) -> [AnyHashable: Any] {
        let pieces = token.components(separatedBy: ".")
        if pieces.count > 2 {
            var claims: String = String(pieces[1])
            let paddedLength = claims.length + (4 - (claims.length % 4)) % 4
            claims = (claims as NSString).padding(toLength: paddedLength, withPad: "=", startingAt: 0)
            if let claimsData: Data = Data(base64Encoded: claims, options: Data.Base64DecodingOptions.ignoreUnknownCharacters) {
                let result: [AnyHashable: Any] = (try? JSONSerialization.jsonObject(with: claimsData)) as! [AnyHashable: Any]
                return result
            }
            else {
                print("Token is not valid base64")
            }
        }
        return [:]
    }
    
    /// Clean credentials
    static func cleanUp() {
        UserDefaults.token = nil
        DataSource.headers = [:]
        UserDefaults.isAuthenticated = false
    }
}
