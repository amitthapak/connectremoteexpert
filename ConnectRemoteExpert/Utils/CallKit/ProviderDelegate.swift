//
//  ProviderDelegate.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 6/3/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import CallKit
import AVFoundation

struct Call {
    
    let uuid: UUID
    let channelName: String
    let userId: String // remote side id
    let userName: String // remote side name: (the callee name if outgoing, and caller name if incoming)
    
    var hasStartedConnectingDidChange: (()->())?
    var hasConnectedDidChange: (()->())?
    var userRefured: (()->())?
    var userOffline: (()->())?
    var connectionError: (()->())?
    var userEnded: (()->())?
}

protocol CallView: class {
    func updateCallUI()
}

protocol StreamHandler: class {
    
    func start(call: Call)
    func answer(call: Call)
    func end(call: Call)
    func decline(call: Call)
}

protocol CallManager: class {
    
    /// `call` should be created using new UUID()
    func startIncomingCall(call: Call, phone: String?, email: String?)
    func startOutgoingCall(userId: String, userName: String, channelName: String)
    func end(call: Call)
    
    func getCall(_ uuid: UUID) -> Call?
    func getLastCall() -> Call?
    func hasCalls() -> Bool
    func remove(call: Call)
}

 /**
 CallKit delegate
 Possible sequences:
 incoming call:
 - accepted: reportNewIncomingCall -> CXAnswerCallAction -> request(end) -> CXEndCallAction -> streamHandler.end()
 - refused: reportNewIncomingCall -> CXEndCallAction -> streamHandler.decline()
 outgoing call:
 - refused: request(start) -> CXStartCallAction -> request(end) -> CXEndCallAction
 - accepted: request(start) -> CXStartCallAction -> reportOutgoingCall(connectedAt) -> ... > request(end) -> CXEndCallAction
 
 User status should be changed when: CXAnswerCallAction, CXStartCallAction and CXEndCallAction.
 */
final class ProviderDelegate: NSObject, CXProviderDelegate, CallManager {
    
    static var shared = ProviderDelegate()
    
    private let provider: CXProvider?
    private let callController = CXCallController()
    
    var callManager: CallManager? {
        return self
    }
    weak var callView: CallView!
    weak var streamHandler: StreamHandler!
    
    private var calls = [UUID: Call]()
    
    override init() {
        provider = CXProvider(configuration: type(of: self).configuration)
        super.init()
        provider?.setDelegate(self, queue: nil)
    }
    
    static var configuration: CXProviderConfiguration {
        let configuration = CXProviderConfiguration(localizedName: "RemoveExpert")
        configuration.maximumCallsPerCallGroup = 1
        configuration.supportsVideo = true
        configuration.supportedHandleTypes = [.phoneNumber]
        return configuration
    }
    
    func providerDidReset(_ provider: CXProvider) {
        print("providerDidReset")
    }
    
    // MARK: - Incoming Calls
    
    func startIncomingCall(call: Call, phone: String?, email: String?) {
        let update = CXCallUpdate()
        update.localizedCallerName = call.userName
        update.hasVideo = true
        if let phone = phone {
            let remoteHandle = CXHandle(type: .phoneNumber, value: phone)
            update.remoteHandle = remoteHandle
        }
        else if let email = email {
            let remoteHandle = CXHandle(type: .emailAddress, value: email)
            update.remoteHandle = remoteHandle
        }
        
        print("reportIncomingCall")
        provider?.reportNewIncomingCall(with: call.uuid, update: update) { (error) in
            if let error = error {
                print("ERROR: \(error)")
                return
            }
            
            /// Process the call
            self.add(call: call)
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("provider: perform CXAnswerCallAction")
        guard let call = callManager?.getCall(action.callUUID) else {
            print("ERROR: call not found: \(action.callUUID)")
            action.fail()
            return
        }
        
        configureAudioSession()
        
        streamHandler.answer(call: call)
        action.fulfill()
    }
    
    // MARK: - Outgoing Calls
    
    /// the last channel name, and other info
    private var lastOutgoingCall: Call!
    
    /// Start outgoing call
    /// - Parameters:
    ///   - userId: the id
    ///   - userName: the name
    ///   - channelName: the channel name
    func startOutgoingCall(userId: String, userName: String, channelName: String) {
        let call = Call(uuid: UUID(), channelName: channelName, userId: userId, userName: userName)
        self.lastOutgoingCall = call
        
        /// CallKit integration
        let handle = CXHandle(type: .generic, value: userName)
        let action = CXStartCallAction(call: call.uuid, handle: handle)
        action.isVideo = true
        
        let transaction = CXTransaction()
        transaction.addAction(action)
        
        print("CXStartCallAction -> callController")
        callController.request(transaction) { (error) in
            if let error = error { print("ERROR: \(error)") }
        }
    }
    
    /// when system allows outgoing call to happen
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        print("provider: perform CXStartCallAction")
        guard var call = lastOutgoingCall, call.uuid == action.callUUID else {
            action.fail(); return
        }
        
        configureAudioSession()
        call.hasStartedConnectingDidChange = { [weak self] in
            self?.provider?.reportOutgoingCall(with: call.uuid, startedConnectingAt: Date())
        }
        
        call.hasConnectedDidChange = { [weak self] in
            self?.provider?.reportOutgoingCall(with: call.uuid, connectedAt: Date())
        }
        call.userOffline = { [weak self] in
            self?.provider?.reportCall(with: call.uuid, endedAt: Date(), reason: CXCallEndedReason.unanswered)
        }
        call.userRefured = { [weak self] in
            self?.provider?.reportCall(with: call.uuid, endedAt: Date(), reason: CXCallEndedReason.declinedElsewhere)
        }
        call.userEnded = { [weak self] in
            self?.provider?.reportCall(with: call.uuid, endedAt: Date(), reason: CXCallEndedReason.remoteEnded)
        }
        
        call.connectionError = { [weak self] in
            self?.provider?.reportCall(with: call.uuid, endedAt: Date(), reason: CXCallEndedReason.failed)
        }
        
        streamHandler.start(call: call)
        action.fulfill()
        add(call: call)
    }
    
    // MARK: - Call processing
    
    /// Configure audio session
    private func configureAudioSession() {
        // Do not activate session. It's done by the system, then `provider:didActivate` is called
        // Nothing needed because framework does it internally.
    }
    
    // Called after `CXAnswerCallAction.fulfill` and `CXStartCallAction.fulfill`
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        print("provider: didActivate audioSession")
    }
    
    func provider(_ provider: CXProvider, execute transaction: CXTransaction) -> Bool {
        print("provider: execute transaction: \(transaction)")
        return false
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        print("provider: CXSetHeldCallAction")
        action.fail()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        print("provider: CXSetMutedCallAction")
        action.fail()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetGroupCallAction) {
        print("provider: CXSetGroupCallAction")
        action.fail()
    }
    
    // MARK: - CallManager
    
    func getCall(_ uuid: UUID) -> Call? {
        return calls[uuid]
    }
    
    func add(call: Call) {
        /// implement storing
        calls[call.uuid] = call
        callView.updateCallUI()
    }
    
    func remove(call: Call) {
        /// implement storing
        calls.removeValue(forKey: call.uuid)
        callView.updateCallUI()
        
        NotificationCenter.post(CallActionNotification.completed)
    }
    
    func getLastCall() -> Call? {
        if let call = calls.first {
            return call.value
        }
        return nil
    }
    
    func hasCalls() -> Bool {
        return !calls.isEmpty
    }
    
    // MARK: - End call
    
    /// End call manually
    /// - Parameter call: the call
    func end(call: Call) {
        print("CXEndCallAction -> callController")
        let action = CXEndCallAction(call: call.uuid)
        let transaction = CXTransaction()
        transaction.addAction(action)
        callController.request(transaction) { (error) in
            if let error = error {
                print("ERROR: \(error)")
            }
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        print("provider: perform CXEndCallAction")
        guard let call = callManager?.getCall(action.callUUID) else {
            print("ERROR: call not found: \(action.callUUID)")
            action.fail()
            return
        }
        
        if let systemCall = currentCall(of: action.callUUID) {
            if systemCall.isOutgoing || systemCall.hasConnected {
                streamHandler.end(call: call)
            }
            else {
                streamHandler.decline(call: call)
            }
        }
        action.fulfill()
        callManager?.remove(call: call)
    }
    
    func currentCall(of uuid: UUID) -> CXCall? {
        let calls = callController.callObserver.calls
        if let index = calls.firstIndex(where: {$0.uuid == uuid}) {
            return calls[index]
        } else {
            return nil
        }
    }
}
