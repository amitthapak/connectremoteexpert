//
//  HelpfulFunctions.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/18/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SceneKit

/*
 Helpful functions
 */

// Vector Maths helper functions
func + (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x + right.x, left.y + right.y, left.z + right.z)
}

/// the number formatter
var generalNumberFormatter: NumberFormatter {
    let f = NumberFormatter()
    f.locale = Locale(identifier: "us")
    return f
}

extension UIColor {
    
    /// Convert color to string command
    func toString() -> String? {
        guard let colorComponents = self.cgColor.components else { return nil }
        return "color: \(colorComponents)"
    }
    
    /// Parse color from the string
    static func from(string: String) -> UIColor? {
        var dataString = string
        // remove the [ ] characters from the string
        if let closeBracketIndex = dataString.firstIndex(of: "]") {
            dataString.remove(at: closeBracketIndex)
            dataString = dataString.replacingOccurrences(of: "color: [", with: "")
        }
        // convert the string into an array -- using , as delimeter
        let colorComponentsStringArray = dataString.components(separatedBy: ", ")
        // safely convert the string values into numbers
        guard let redColor = generalNumberFormatter.number(from: colorComponentsStringArray[0]) else { return nil }
        guard let greenColor = generalNumberFormatter.number(from: colorComponentsStringArray[1]) else { return nil }
        guard let blueColor = generalNumberFormatter.number(from: colorComponentsStringArray[2]) else { return nil }
        guard let colorAlpha = generalNumberFormatter.number(from: colorComponentsStringArray[3]) else { return nil }
        
        let color = UIColor.init(red: CGFloat(truncating: redColor), green: CGFloat(truncating: greenColor), blue: CGFloat(truncating: blueColor), alpha: CGFloat(truncating: colorAlpha))
        return color
    }
}

extension CGPoint {
    
    // convert data string into an array -- using given pattern as delimeter
    static func from(string: String) -> [CGPoint] {
        var list = [CGPoint]()
        let arrayOfPoints = string.components(separatedBy: "), (")
        let f = generalNumberFormatter
        for pointString in arrayOfPoints {
            let pointArray: [String] = pointString.components(separatedBy: ", ")
            // make sure we have 2 points and convert them from String to number
            if pointArray.count == 2,
                let x = f.number(from: pointArray[0]),
                let y = f.number(from: pointArray[1]) {
                let p: CGPoint = CGPoint(x: CGFloat(truncating: x), y: CGFloat(truncating: y))
                list.append(p)
            }
        }
        return list
    }
}

extension simd_float4x4 {
    
    /// Get point from the transform
    func toPoint() -> simd_float3 {
        return simd_float3(x: self[3, 0], y: self[3, 1], z: self[3, 2])
    }
}

extension simd_float3 {
    
    /// Convert to vector
    func toVector() -> SCNVector3 {
        return SCNVector3(x: x, y: y, z: z)
    }
    
    /// Square distance to the point
    func distance2(to point: simd_float3) -> Float {
        let dx = x - point.x
        let dy = y - point.y
        let dz = z - point.z
        return dx*dx + dy*dy + dz*dz
    }
}

/// Extension for UIViewController
extension UIViewController {
    
    /// Displays alert with specified title & message
    func showAlert(_ title: String, _ message: String, completion: (()->())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .default,
                                      handler: { (_) -> Void in
                                        alert.dismiss(animated: true, completion: nil)
                                        DispatchQueue.main.async {
                                            completion?()
                                        }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    /// Add the view controller. Uses autoconstraints.
    func loadViewController(_ childVC: UIViewController, _ containerView: UIView) {
        let childView = childVC.view
        childView?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,  UIView.AutoresizingMask.flexibleHeight]
        loadViewController(childVC, containerView, withBounds: containerView.bounds)
    }
    
    /// Load view controller and add connstraints
    func loadViewControllerAtTheBottom(_ childVC: UIViewController, _ containerView: UIView, bottom: CGFloat = 0) {
        guard let childView = childVC.view else { return }
        childView.translatesAutoresizingMaskIntoConstraints = false
        loadViewController(childVC, containerView, withBounds: containerView.bounds)
        containerView.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .leading,
                                                       relatedBy: .equal,
                                                       toItem: containerView,
                                                       attribute: .leading,
                                                       multiplier: 1.0,
                                                       constant: 0))
        containerView.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .trailing,
                                                       relatedBy: .equal,
                                                       toItem: containerView,
                                                       attribute: .trailing,
                                                       multiplier: 1.0,
                                                       constant: 0))
        containerView.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .bottom,
                                                       relatedBy: .equal,
                                                       toItem: containerView,
                                                       attribute: .bottom,
                                                       multiplier: 1.0,
                                                       constant: -bottom))
    }
    
    /// Load view controller and add connstraints
    func loadAtTheBottom(_ childVC: UIViewController, _ containerView: UIView, bottom: CGFloat = 0, withHeight: CGFloat) -> NSLayoutConstraint? {
        guard let childView = childVC.view else { return nil }
        childView.translatesAutoresizingMaskIntoConstraints = false
        loadViewController(childVC, containerView, withBounds: containerView.bounds)
        containerView.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .leading,
                                                       relatedBy: .equal,
                                                       toItem: containerView,
                                                       attribute: .leading,
                                                       multiplier: 1.0,
                                                       constant: 0))
        containerView.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .trailing,
                                                       relatedBy: .equal,
                                                       toItem: containerView,
                                                       attribute: .trailing,
                                                       multiplier: 1.0,
                                                       constant: 0))
        containerView.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .bottom,
                                                       relatedBy: .equal,
                                                       toItem: containerView,
                                                       attribute: .bottom,
                                                       multiplier: 1.0,
                                                       constant: -bottom))
        let h = NSLayoutConstraint(item: childView,
                                   attribute: .height,
                                   relatedBy: .equal,
                                   toItem: nil,
                                   attribute: .height,
                                   multiplier: 1.0,
                                   constant: withHeight)
        childView.addConstraint(h)
        return h
    }
    
    /// Add the view controller. Constraints can be added manually or automatically.
    func loadViewController(_ childVC: UIViewController, _ containerView: UIView, withBounds bounds: CGRect) {
        let childView = childVC.view
        
        childView?.frame = bounds
        
        // Adding new VC and its view to container VC
        self.addChild(childVC)
        containerView.addSubview(childView!)
        
        // Finally notify the child view
        childVC.didMove(toParent: self)
    }
    
    /// Remove view controller and view from their parents
    func remove() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
}

extension UIView {
    
    /// Load view with constraints
    func loadViewWithConstraints(_ childView: UIView) {
        childView.translatesAutoresizingMaskIntoConstraints = false
        childView.frame = bounds
        self.insertSubview(childView, at: 0)
        self.addConstraint(NSLayoutConstraint(item: childView,
                                              attribute: .top,
                                              relatedBy: .equal,
                                              toItem: self,
                                              attribute: .top,
                                              multiplier: 1.0,
                                              constant: 0))
        self.addConstraint(NSLayoutConstraint(item: childView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: self,
                                              attribute: .leading,
                                              multiplier: 1.0,
                                              constant: 0))
        self.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .trailing,
                                                       relatedBy: .equal,
                                                       toItem: self,
                                                       attribute: .trailing,
                                                       multiplier: 1.0,
                                                       constant: 0))
        self.addConstraint(NSLayoutConstraint(item: childView,
                                                       attribute: .bottom,
                                                       relatedBy: .equal,
                                                       toItem: self,
                                                       attribute: .bottom,
                                                       multiplier: 1.0,
                                                       constant: 0))
    }
}

extension String {
    
    var ascii: Data {
        self.data(using: String.Encoding.ascii)!
    }
}

func makeTranslationMatrix(tx: Float, ty: Float, tz: Float) -> simd_float4x4 {
    var matrix = matrix_identity_float4x4
    
    //    matrix[0, 3] = tx
    //    matrix[1, 3] = ty
    //    matrix[2, 3] = tz
    matrix[3, 0] = tx
    matrix[3, 1] = ty
    matrix[3, 2] = tz
    
    return matrix
}

func makeRotationXMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4(1,          0,          0, 0),
        simd_float4(0, cos(angle), sin(angle), 0),
        simd_float4(0, -sin(angle), cos(angle), 0),
        simd_float4(0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

func makeRotationYMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4( cos(angle), 0, sin(angle), 0),
        simd_float4( 0,          1,          0, 0),
        simd_float4(-sin(angle), 0, cos(angle), 0),
        simd_float4( 0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

func makeRotationZMatrix(angle: Float) -> simd_float4x4 {
    let rows = [
        simd_float4( cos(angle), sin(angle), 0, 0),
        simd_float4(-sin(angle), cos(angle), 0, 0),
        simd_float4( 0,          0,          1, 0),
        simd_float4( 0,          0,          0, 1)
    ]
    
    return float4x4(rows: rows)
}

func makeScaleMatrix(xScale: Float, yScale: Float, zScale: Float) -> simd_float4x4 {
    let rows = [
        simd_float4(xScale,      0,      0, 0),
        simd_float4(     0, yScale,      0, 0),
        simd_float4(     0,      0, zScale, 0),
        simd_float4(     0,      0,      0, 1)
    ]
    return float4x4(rows: rows)
}

/// Create transform by two points. Ox will start from `p1` and go through `p2`.
func createTransformXByTwoPoints(p1: SCNVector3, p2: SCNVector3) -> simd_float4x4{
    let angleXZ = p1.angleXZ(to: p2)
    let angleXZProj = -1 * p1.angleXZProjection(to: p2)
    print(angleXZProj.radToG())
    let translate = makeTranslationMatrix(tx: p1.x, ty: p1.y, tz: p1.z)
    let rotationY = makeRotationYMatrix(angle: Float(angleXZ)) // dodo optimize using a/b = sin()
    let rotationZ = makeRotationZMatrix(angle: Float(angleXZProj)) // dodo optimize using a/b = sin()
    let d = p1.distance(to: p2)
    let scale = makeScaleMatrix(xScale: d, yScale: d, zScale: d)
    let transform = translate * rotationY * rotationZ * scale
    return transform
}

// Calculate 3rd point of a rectangle (square) defined by two points (left top and right bottom). It will be top right point.
// Also it scales so that 3rd rectangle point matches initial (1,1) coordinates
// If points have equal X and Z, then transform will be created so, than Ox axis will go through these points
func createTransformByRectanglePoints(point1: SCNVector3, point3: SCNVector3) -> simd_float4x4 {
    let p1 = point1
    let p2 = point3
    let projXZ: simd_float4 = simd_float4(x: p2.x - p1.x, y: 0, z: p2.z - p1.z, w: 0) // w:0 - it's a vector, not point
    //    let rotate90: simd_float4 = makeRotationYMatrix(angle: -.pi/2) * projXZ
    let rotate270: simd_float4 = makeRotationYMatrix(angle: .pi/2) * projXZ
    let center = p1.center(between: p2)
    let d1 = rotate270.toSimd3().distance()
    let d2 = p1.distance(to: center)
    if d1 != 0 {
        let k = d2 / d1
        let v3 = simd_float4(x: rotate270.x * k, y: rotate270.y * k, z: rotate270.z * k, w: 1)
        let point3: simd_float3 = (makeTranslationMatrix(tx: center.x, ty: center.y, tz: center.z) * v3).toSimd3()
        let p3 = SCNVector3(point3)
        // addPoint(p3)
        
        //        let v4 = simd_float4(x: rotate90.x * k, y: rotate90.y * k, z: rotate90.z * k, w: 1)
        //        let point4: simd_float3 = (makeTranslationMatrix(tx: center.x, ty: center.y, tz: center.z) * v4).toSimd3()
        //        let p4 = SCNVector3(point4)
        //         addPoint(p4)
        
        let t = createTransformXByTwoPoints(p1: p1, p2: p3)
        return t
    }
    else {
        return createTransformXByTwoPoints(p1: p1, p2: p2)
    }
}


extension SCNVector3 {
    
    /// Distance to vector
    /// - Parameter vector: the vector
    /// - Returns: distance
    func distance(to vector: SCNVector3) -> Float {
        return simd_distance(simd_float3(self), simd_float3(vector))
    }
    
    func translation(to vector: SCNVector3) -> simd_float4x4 {
        return makeTranslationMatrix(tx: Float(vector.x - x), ty: Float(vector.y - y), tz: Float(vector.z - z))
    }
    
    func center(between point: SCNVector3) -> SCNVector3 {
        return SCNVector3(x: x + (point.x - x)/2, y: y + (point.y - y)/2, z: z + (point.z - z)/2)
    }
}

extension SCNVector3 {
    
    /// Angle of rotation in XZ plane
    /// - Parameter point: the point
    /// - Returns: angle
    func angleXZ(to point: SCNVector3) -> CGFloat {
        let dx = point.x - x
        let dz = -1 * (point.z - z) // because in 3D X and Z are clockwise
        if dx == 0 {
            return dz > 0 ? .pi / 2 : -.pi / 2
        }
        return CGFloat(atan(dz/dx) + (dx < 0 ? .pi : 0))
    }
    
    /// Angle of projection to plane crossing p1 and parallel to XZ
    /// - Parameter point: the point
    /// - Returns: angle
    func angleXZProjection(to point: SCNVector3) -> CGFloat {
        let h = point.y - y
        let proj = SCNVector3(point.x, y, point.z)
        let d = proj.distance(to: self)
        if d == 0 {
            return h > 0 ? .pi / 2 : -.pi / 2
        }
        return CGFloat(atan(h/d))
    }
    
    func xy2xz() -> SCNVector3 {
        return SCNVector3(x, z, y)
    }
}

extension CGFloat {
    
    func radToG() -> CGFloat {
        return self / .pi * 180
    }
    
    func toRad() -> CGFloat {
        return self / 180 * .pi
    }
}

extension matrix_float4x4 {
    /**
     Calculate position vector of a pose matrix
     
     - Returns: A SCNVector3 from the translation components of the matrix
     */
    public func position() -> SCNVector3 {
        return SCNVector3(columns.3.x, columns.3.y, columns.3.z)
    }
}

extension simd_float4 {
    
    /// Convert to simd_float3
    func toSimd3() -> simd_float3 {
        return simd_float3(x: x, y: y, z: z)
    }
}

extension simd_float3 {
    
    func distance() -> Float {
        return simd_distance(self, simd_float3.init())
    }
}
