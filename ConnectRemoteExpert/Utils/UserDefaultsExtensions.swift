//
//  UserDefaultsExtensions.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 19.06.2020.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    /// Keys for storing data in UserDefaults
    public struct Key {
        public static let isAuthenticated = "isAuthenticated"
        public static let token = "token"
    }
    
    /// true - if user is authenticated, false - else
    static public var isAuthenticated: Bool {
        get {
            UserDefaults.standard.bool(forKey: Key.isAuthenticated)
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: Key.isAuthenticated)
            UserDefaults.standard.synchronize()
        }
    }
    
    /// access token
    static public var token: String? {
        get {
            UserDefaults.standard.value(forKey: Key.token) as? String
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: Key.token)
            UserDefaults.standard.synchronize()
        }
    }
}
