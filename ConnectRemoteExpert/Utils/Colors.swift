//
//  Colors.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 5/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit

/// Colors from the design
struct Colors {
    
    /// the footer color
    static let footerColor = UIColor(0xEFEFF4)
    
    /// blue color
    static let blueColor = UIColor(0x007AFF)
    
    /// the background color
    static let backgroundColor = UIColor(0xF9F9F9)
    
    /// the system background color
    static var systemBackgroundLight: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemBackground
        } else {
            return UIColor.white
        }
    }
    
    /// the system background color
    static var systemBackgroundDark: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemBackground
        } else {
            return UIColor.white
        }
    }
    
    /// the system background color
    static var systemBackground: UIColor {
        if UIScreen.main.traitCollection.userInterfaceStyle == .light {
            return systemBackgroundLight
        }
        else {
            return systemBackgroundDark
        }
    }
    
    /// the system color for a block
    static var systemGrayBlock: UIColor {
        if UIScreen.main.traitCollection.userInterfaceStyle == .light {
            return footerColor
        }
        else {
            return systemBackgroundDark
        }
    }
    
    /// the border color for a block
    static var systemBlockBorder: UIColor {
        if UIScreen.main.traitCollection.userInterfaceStyle == .light {
            return .clear
        }
        else {
            return footerColor
        }
    }
}
