//
//  PenTool.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

/// Pen tool
class PenTool: Tool {
    
    /// tool settings
    var color: UIColor
    var lineSettings: LineSettings
    
    init(color: UIColor, lineSettings: LineSettings) {
        self.color = color
        self.lineSettings = lineSettings
    }
    
    /// Add line between points
    func processPoint(point1: SCNVector3, point2: SCNVector3, sceneView: ARSCNView) -> ([SCNNode], [SCNNode]) {
        let effectiveColor = color.withAlphaComponent(lineSettings.alpha)
        return (PenTool.createPenLine(point1: point1, point2: point2, inScene: sceneView.scene, lineWidth: lineSettings.width, effectiveColor: effectiveColor), [])
    }
    
    /// Add line between points to the scene and return line and ending circle
    static func createPenLine(point1: SCNVector3, point2: SCNVector3, inScene scene: SCNScene, lineWidth: CGFloat, effectiveColor: UIColor) -> [SCNNode] {
        let last = point1
        let p = point2
        // Add line
        let line = PenTool.createLine(point1: last, point2: p, inScene: scene, lineWidth: lineWidth, color: effectiveColor)
        scene.rootNode.addChildNode(line)
        
        // Add point
        let sphere = SCNSphere(radius: lineWidth)
        PenTool.setupMaterial(on: sphere, color: effectiveColor)
        let circle = SCNNode(geometry: sphere)
        circle.position = p
        scene.rootNode.addChildNode(circle)
        return [line, circle]
    }
    
    func setColor(_ color: UIColor) {
        self.color = color
    }
    
    func set(lineSettings: LineSettings) {
        self.lineSettings = lineSettings
    }
    
    func complete() {}
    
    /// Create line
    static func createLine(point1: SCNVector3, point2: SCNVector3, inScene: SCNScene, lineWidth: CGFloat, color: UIColor) -> SCNNode {
        let vector = SCNVector3(point1.x - point2.x, point1.y - point2.y, point1.z - point2.z)
        let distance = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
        let midPosition = SCNVector3 (x:(point1.x + point2.x) / 2, y:(point1.y + point2.y) / 2, z:(point1.z + point2.z) / 2)
        
        let lineGeometry = SCNCylinder()
        lineGeometry.radius = lineWidth
        lineGeometry.height = CGFloat(distance)
        lineGeometry.radialSegmentCount = 10
        setupMaterial(on: lineGeometry, color: color)
        
        print("line midPosition: \(midPosition)")
        let lineNode = SCNNode(geometry: lineGeometry)
        lineNode.position = midPosition
        lineNode.look(at: point2, up: inScene.rootNode.worldUp, localFront: lineNode.worldUp)
        return lineNode
    }
    
    /// Setup material on the geometry
    /// - Parameter geometry: the geometry
    private static func setupMaterial(on geometry: SCNGeometry, color: UIColor) {
        geometry.firstMaterial!.diffuse.contents = color
        geometry.firstMaterial!.lightingModel = .constant
        geometry.firstMaterial!.blendMode = .replace
    }
}
