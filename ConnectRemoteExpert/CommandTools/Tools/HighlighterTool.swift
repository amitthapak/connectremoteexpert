//
//  HighlighterTool.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

/// Highlighter tool
class HighlighterTool: Tool {
    
    /// tool settings
    var color: UIColor
    var alpha: CGFloat = 0.5
    var lineSettings: LineSettings
    
    init(color: UIColor, lineSettings: LineSettings) {
        self.color = color
        self.lineSettings = lineSettings
    }
    
    /// Add line between points
    func processPoint(point1: SCNVector3, point2: SCNVector3, sceneView: ARSCNView) -> ([SCNNode], [SCNNode]) {
        let effectiveColor = color.withAlphaComponent(lineSettings.alpha)
        return (PenTool.createPenLine(point1: point1, point2: point2, inScene: sceneView.scene, lineWidth: lineSettings.width, effectiveColor: effectiveColor), [])
    }
    
    func setColor(_ color: UIColor) {
        self.color = color
    }
    
    /// Set line's settings
    func set(lineSettings: LineSettings) {
        self.lineSettings = lineSettings
    }
    
    func complete() {}
}
