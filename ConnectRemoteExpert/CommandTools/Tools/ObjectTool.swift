//
//  ObjectTool.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 7/18/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

class ObjectTool: Tool {
    
    /// tool settings
    var color: UIColor
    var settings: ObjectSettings
    
    private var firstPoint: SCNVector3?
    private var lastPoint: SCNVector3!
    private var objects: [SCNNode]?
    
    init(color: UIColor, settings: ObjectSettings) {
        self.color = color
        self.settings = settings
    }
    
    func setColor(_ color: UIColor) {
        self.color = color
    }
    
    func set(lineSettings: LineSettings) { }
    
    func set(settings: ObjectSettings) {
        self.settings = settings
        reset()
    }
    
    func complete() {
        reset()
    }
    
    /// Reset settings
    private func reset() {
        firstPoint = nil
        lastPoint = nil
        objects = nil
    }
    
    func processPoint(point1: SCNVector3, point2: SCNVector3, sceneView: ARSCNView) -> ([SCNNode], [SCNNode]) {
        let effectiveColor = color.withAlphaComponent(settings.alpha)
        lastPoint = point2
        if let _ = objects, let _ = firstPoint {
        }
        else {
            firstPoint = point1
        }
        guard let firstPoint = firstPoint else { return ([], []) }
        
        let removed: [SCNNode] = self.objects ?? []
        print("p1-p2: \(point1) \(point2)")
        switch settings.objectIndex {
        case 0: // ▭
            let path = ObjectTool.createPathReactangle(point1: firstPoint, point2: point2)
            print(path)
            self.objects = ObjectTool.createPath(path: path, inScene: sceneView.scene, lineWidth: settings.width, color: effectiveColor)
        case 1: // o
            if let objects = objects {
                ObjectTool.update(circle: objects.first!, point1: firstPoint, point2: point2, inScene: sceneView.scene)
                return ([], [])
            }
            else {
                let node = ObjectTool.createCircle(point1: point1, point2: point2, inScene: sceneView.scene, lineWidth: settings.width, color: effectiveColor)
                self.objects = [node]
            }
        case 2: // ✓
            let path = ObjectTool.createPathCheckmark(point1: firstPoint, point2: point2)
            self.objects = ObjectTool.createPath(path: path, inScene: sceneView.scene, lineWidth: settings.width, color: effectiveColor)
        case 3: // X
            let path = ObjectTool.createPathX(point1: firstPoint, point2: point2)
            self.objects = ObjectTool.createPath(path: path, inScene: sceneView.scene, lineWidth: settings.width, color: effectiveColor)
        case 4: // ↑
            let path = ObjectTool.createPathArrowUp(point1: firstPoint, point2: point2)
            self.objects = ObjectTool.createPath(path: path, inScene: sceneView.scene, lineWidth: settings.width, color: effectiveColor)
        case 5: // →
            let path = ObjectTool.createPathArrowRight(point1: firstPoint, point2: point2)
            self.objects = ObjectTool.createPath(path: path, inScene: sceneView.scene, lineWidth: settings.width, color: effectiveColor)
        case 6: // ↓
            let path = ObjectTool.createPathArrowDown(point1: firstPoint, point2: point2)
            self.objects = ObjectTool.createPath(path: path, inScene: sceneView.scene, lineWidth: settings.width, color: color)
        case 7: // ←
            let path = ObjectTool.createPathArrowLeft(point1: firstPoint, point2: point2)
            self.objects = ObjectTool.createPath(path: path, inScene: sceneView.scene, lineWidth: settings.width, color: effectiveColor)
        default:
            break
        }
        for item in removed { item.removeFromParentNode() }
        return (self.objects!, removed)
    }
    
    // MARK: - Circle
    /// Create circle
    static func createCircle(point1: SCNVector3, point2: SCNVector3, inScene: SCNScene, lineWidth: CGFloat, color: UIColor) -> SCNNode {
        let vector = SCNVector3(point1.x - point2.x, point1.y - point2.y, point1.z - point2.z)
        let distance = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
        let midPosition = SCNVector3 (x:(point1.x + point2.x) / 2, y:(point1.y + point2.y) / 2, z:(point1.z + point2.z) / 2)
        
        let tor = SCNTorus()
        tor.pipeSegmentCount = 5
        tor.pipeRadius = lineWidth
        tor.ringSegmentCount = 30
        tor.ringRadius = CGFloat(distance)
        setupMaterial(on: tor, color: color)
        
        let node = SCNNode(geometry: tor)
        node.position = midPosition
        inScene.rootNode.addChildNode(node)
        return node
    }
    
    static func update(circle node: SCNNode, point1: SCNVector3, point2: SCNVector3, inScene: SCNScene) {
        let vector = SCNVector3(point1.x - point2.x, point1.y - point2.y, point1.z - point2.z)
        let distance = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
        let midPosition = SCNVector3 (x:(point1.x + point2.x) / 2, y:(point1.y + point2.y) / 2, z:(point1.z + point2.z) / 2)
        (node.geometry as! SCNTorus).ringRadius = CGFloat(distance)
        print("midPosition: \(midPosition)")
        node.position = midPosition
    }
    
    static func createPath(path: [SCNVector3], inScene: SCNScene, lineWidth: CGFloat, color: UIColor) -> [SCNNode] {
        var nodes = [SCNNode]()
        var last: SCNVector3?
        for point in path {
            if let last = last {
                let node = PenTool.createLine(point1: last, point2: point, inScene: inScene, lineWidth: lineWidth, color: color)
                inScene.rootNode.addChildNode(node)
                nodes.append(node)
            }
            last = point
        }
        return nodes
    }
    
    // MARK: - Figure points calculations
    
    static func translatePath(path: [SCNVector3], point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let t = createTransformByRectanglePoints(point1: point1, point3: point2)
        
        //showCoordinateSystem(t, lendth: 2)
        let points = path
            .map({$0.xy2xz()})
            .map({t * simd_float4(x: $0.x, y: $0.y, z: $0.z, w: 1)})
            .map({SCNVector3($0.toSimd3())})
        return points
    }
    
    static func createPathReactangle(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path: [SCNVector3] = [
            SCNVector3(0, 0, 0),
            SCNVector3(1, 0, 0),
            SCNVector3(1, 1, 0),
            SCNVector3(0, 1, 0),
            SCNVector3(0, 0, 0)
        ]
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    static func createPathX(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path: [SCNVector3] = [
            SCNVector3(0, 0, 0),
            SCNVector3(1, 1, 0),
            SCNVector3(0.5, 0.5, 0),
            SCNVector3(0, 1, 0),
            SCNVector3(1, 0, 0)
        ]
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    static func createPathCheckmark(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path: [SCNVector3] = [
            SCNVector3(0, 0.5, 0),
            SCNVector3(0.5, 1, 0),
            SCNVector3(1, 0, 0)
        ]
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    /// Create path that fits into [0,0], [1,1] and reprecent an arrow. Points are used to calculate size of the arrow ending
    static func createInitialPathArrow(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let sizeOfEndInPercent = 0.25
        let path: [SCNVector3] = [
            SCNVector3(0.5 - sizeOfEndInPercent, 1 - sizeOfEndInPercent, 0),
            SCNVector3(0.5, 1, 0),
            SCNVector3(0.5, 0, 0),
            SCNVector3(0.5, 1, 0),
            SCNVector3(0.5 + sizeOfEndInPercent, 1 - sizeOfEndInPercent, 0)
        ]
        return path
    }
    
    static func rotate(path: [SCNVector3], angle: Float) -> [SCNVector3] {
        let t1 = makeTranslationMatrix(tx: -0.5, ty: -0.5, tz: 0)
        let r = makeRotationZMatrix(angle: angle)
        let t2 = makeTranslationMatrix(tx: 0.5, ty: 0.5, tz: 0)
        let t: simd_float4x4 = t2 * r * t1
        return path
            .map({t * simd_float4(x: $0.x, y: $0.y, z: $0.z, w: 1)})
            .map({SCNVector3($0.toSimd3())})
    }
    
    static func createPathArrowUp(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path1 = createInitialPathArrow(point1: point1, point2: point2)
        let path = rotate(path: path1, angle: .pi)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    static func createPathArrowLeft(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path1 = createInitialPathArrow(point1: point1, point2: point2)
        let path = rotate(path: path1, angle: .pi * 3 / 2)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    static func createPathArrowRight(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path1 = createInitialPathArrow(point1: point1, point2: point2)
        let path = rotate(path: path1, angle: .pi / 2)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    static func createPathArrowDown(point1: SCNVector3, point2: SCNVector3) -> [SCNVector3] {
        let path = createInitialPathArrow(point1: point1, point2: point2)
        return translatePath(path: path, point1: point1, point2: point2)
    }
    
    /// Setup material on the geometry
    /// - Parameter geometry: the geometry
    private static func setupMaterial(on geometry: SCNGeometry, color: UIColor) {
        geometry.firstMaterial!.diffuse.contents = color
        geometry.firstMaterial!.lightingModel = .constant
        geometry.firstMaterial!.blendMode = .replace
    }
}
