//
//  Tool.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

/// Protocol for a painting tool
protocol Tool {
    
    /// Add line between two points or draw objects
    /// - Parameters:
    ///   - point1: 1st point
    ///   - point2: 2nd point
    ///   - sceneView: the related scene view
    /// - Returns: (added nodes, removed nodes)
    func processPoint(point1: SCNVector3, point2: SCNVector3, sceneView: ARSCNView) -> ([SCNNode], [SCNNode])
    
    /// Set color for the tool
    func setColor(_ color: UIColor)
    
    /// Set line's settings
    func set(lineSettings: LineSettings)
    
    /// Drawing complete (when user moves finger out)
    func complete()
}
