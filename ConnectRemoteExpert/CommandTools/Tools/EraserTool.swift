//
//  EraserTool.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

/// Eraser tool
class EraserTool: Tool {
    
    func processPoint(point1: SCNVector3, point2: SCNVector3, sceneView: ARSCNView) -> ([SCNNode], [SCNNode]) { return ([], []) }
    func setColor(_ color: UIColor) {}
    func set(lineSettings: LineSettings) {}
    
    func complete() {}
}
