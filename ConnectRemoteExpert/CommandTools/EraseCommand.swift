//
//  EraseCommand.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/19/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

/// Command: erase
class EraseCommand: Command {
    
    /// the scene view
    private let sceneView: ARSCNView
    private let tool: Tool
    var remoteScreenSize: CGSize = .zero
    private var remotePoints: [CGPoint] = []        // list of touches received from the remote user
    
    /// the removed objects
    private var removedObjects = [UUID: [SCNNode]]()
    
    var fetchCommandsCallback: (()->([PaintOnFeaturesCommand]))?
    private var commands: [PaintOnFeaturesCommand]?
    
    init(sceneView: ARSCNView, tool: Tool) {
        self.sceneView = sceneView
        self.tool = tool
    }
    
    /// Add points encoded as a string
    /// - Parameter string: the string from remote device
    func addPoints(fromString string: String) {
        self.remotePoints.append(contentsOf: CGPoint.from(string: string))
    }
    
    /// Process next point
    func processNextPoint() {
        if self.remotePoints.count > 0 {
            var point: CGPoint = self.remotePoints.removeFirst() // pop the first node every frame
            point = PaintOnFeaturesCommand.convertRemotePoint(point: point, remoteViewSize: remoteScreenSize, localViewSize: self.sceneView.bounds.size)
            DispatchQueue.main.async {
                if let (p1, p2) = self.getReferencePoints(for: point) {
                    let distance = p1.distance2(to: p2)
                    self.getCommands { (list) in
                        
                        // Find close points in command
                        for command in list {
                            // Remove nodes from the command and scene
                            let nodes = command.removeNodes(at: p1, atDistance2: distance)
                            /// Store removed nodes for "undo" operation
                            var removedNodes = self.removedObjects[command.id]
                            if removedNodes == nil { removedNodes = [] }
                            removedNodes?.append(contentsOf: nodes)
                            self.removedObjects[command.id] = removedNodes!
                        }
                    }
                }
            }
        }
    }
    
    /// Get commands
    private func getCommands(_ callback: ([PaintOnFeaturesCommand])->()) {
        if let commands = commands {
            callback(commands)
        }
        else {
            self.commands = fetchCommandsCallback?()
            callback(commands!)
        }
    }
    
    /// Get command by ID
    private func getCommand(by id: UUID) -> PaintOnFeaturesCommand? {
        for c in commands ?? [] {
            if c.id == id {
                return c
            }
        }
        return nil
    }
    
    /// Get points in 3D that are close and can be used as a reference to remove all points in the radius that equals |p1-p2|
    /// - Parameter point: the point
    private func getReferencePoints(for point: CGPoint) -> (simd_float3, simd_float3)? {
        let screenPoint = CGPoint(x: self.sceneView.bounds.width * point.x, y: self.sceneView.bounds.height * point.y)
        let nearScreenPoint = CGPoint(x: screenPoint.x + 18, y: screenPoint.y) // width of the eraser to match the finger
        let nearPoint = CGPoint(x: nearScreenPoint.x / self.sceneView.bounds.width, y: nearScreenPoint.y / self.sceneView.bounds.height)
        
        let types: ARHitTestResult.ResultType = [.existingPlane, .estimatedHorizontalPlane, .featurePoint]
        let results1 = self.sceneView.session.currentFrame?.hitTest(point, types: types)
        let results2 = self.sceneView.session.currentFrame?.hitTest(nearPoint, types: types)
        if let closest1 = results1?.filter({$0.type == .existingPlane}).first ?? results1?.first, let closest2 = results2?.filter({$0.type == closest1.type}).first {
            let p1 = closest1.worldTransform.toPoint()
            let p2 = closest2.worldTransform.toPoint()
            return (p1, p2)
        }
        return nil
    }
    
    /// Rollback erased node
    func undo() {
        // Add nodes back
        for (id, list) in removedObjects {
            if let c = getCommand(by: id) {
                c.add(nodes: list)
            }
        }
    }
    
    /// Apply the command
    func apply() { redo() }
    
    /// Erase nodes again
    func redo() {
        for (id, list) in removedObjects {
            if let c = getCommand(by: id) {
                c.remove(nodes: list)
            }
        }
    }

}
