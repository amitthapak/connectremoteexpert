//
//  ColorCommand.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/18/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

/// Command: apply color
class ColorCommand: Command {
    
    private let color: UIColor
    private let view: DrawingView
    private var previousColor: UIColor?
    
    init(color: UIColor, view: DrawingView) {
        self.color = color
        self.view = view
    }
    
    /// Create command from a string
    static func create(fromString string: String, view: DrawingView) -> Command? {
        guard let color = UIColor.from(string: string) else { return nil }
        let command = ColorCommand(color: color, view: view)
        return command
    }
    
    /// Undo the command
    func undo() {
        guard let color = previousColor else { return }
        view.setColor(color, sendToRemote: true)
    }
    
    /// Apply the command, but skip sending color back to client
    func apply() {
        // set line color to UIColor from remote user
        previousColor = view.getColor()
        view.setColor(color, sendToRemote: false)
    }
    
    /// Apply the command (redo)
    func redo() {
        // set line color to UIColor from remote user
        previousColor = view.getColor()
        view.setColor(color, sendToRemote: true)
    }
}
