//
//  PaintOnFeaturesCommand.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/18/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

// Command: Paint on features and planes
class PaintOnFeaturesCommand: Command {
    
    let id = UUID()
    /// the scene view
    private let sceneView: ARSCNView
    private let tool: Tool
    var remoteScreenSize: CGSize = .zero
    
    /// the added objects
    private var addedObjects = [SCNNode]()
    private var remotePoints: [CGPoint] = []        // list of touches received from the remote user
    
    /// the last point
    private var lastPoint: SCNVector3?
    
    // the target type of the painting
    private var paitingTargetType: ARHitTestResult.ResultType?
    
    init(sceneView: ARSCNView, tool: Tool) {
        self.sceneView = sceneView
        self.tool = tool
    }
    
    /// Create command
    static func create(sceneView: ARSCNView?, color: UIColor, tool: Tool) -> Command? {
        guard let sceneView = sceneView else { return nil }
        let command = PaintOnFeaturesCommand(sceneView: sceneView, tool: tool)
        return command
    }
    
    /// Apply the command
    func apply() { redo() }
    
    /// Undo the command
    func undo() {
        for node in addedObjects {
            node.isHidden = true
            node.removeFromParentNode()
        }
    }
    
    /// Redo the command
    func redo() {
        for node in addedObjects {
            node.isHidden = false
            sceneView.scene.rootNode.addChildNode(node)
        }
    }
    
    /// Add points encoded as a string
    /// - Parameter string: the string from remote device
    func addPoints(fromString string: String) {
        self.remotePoints.append(contentsOf: CGPoint.from(string: string))
    }
    
    /// Process next point
    func processNextPoint() {
        if self.remotePoints.count > 0 {
            var point: CGPoint = self.remotePoints.removeFirst() // pop the first node every frame
            let localScreenSize = self.sceneView.bounds.size
            point = PaintOnFeaturesCommand.convertRemotePoint(point: point, remoteViewSize: remoteScreenSize, localViewSize: localScreenSize)
            DispatchQueue.main.async {
                var types: ARHitTestResult.ResultType = [.existingPlane, .estimatedHorizontalPlane, .featurePoint]
                if let paitingTargetType = self.paitingTargetType {
                    types = paitingTargetType
                }
                let results = self.sceneView.session.currentFrame?.hitTest(point, types: types)
                if let closest = results?.filter({$0.type == .existingPlane}).first ?? results?.first {
                    if self.paitingTargetType == nil {
                        self.paitingTargetType = closest.type
                    }
                    
                    guard let last = self.lastPoint else {
                        self.lastPoint = closest.worldTransform.toPoint().toVector(); return
                    }
                    let p = closest.worldTransform.toPoint().toVector()
                    
                    let (added, removed) = self.tool.processPoint(point1: last, point2: p, sceneView: self.sceneView)
                    for item in removed {
                        if let index = self.addedObjects.firstIndex(of: item) {
                            self.addedObjects.remove(at: index)
                        }
                    }
                    self.addedObjects.append(contentsOf: added)
                    self.lastPoint = p
                }
            }
        }
    }
    
    /// Screen sizes may differ on devices and related point will differ on remote device. We need to correctly convert related ([0-1],[0-1]) point to local screen
    /// - Parameters:
    ///   - point: the remote related point
    ///   - remoteViewSize: the remote view size
    ///   - localViewSize: the local view size
    static func convertRemotePoint(point: CGPoint, remoteViewSize: CGSize, localViewSize: CGSize) -> CGPoint {
        let (p, _) = PaintOnFeaturesCommand.convertRemotePoint(point: point, fromSize: remoteViewSize, toSize: localViewSize)
        let sceneProportions = CGSize(width: 3, height: 4)
        let (p2, _) = PaintOnFeaturesCommand.convertRemotePoint(point: p, fromSize: localViewSize, toSize: sceneProportions)
        return p2
    }
    
    
    /// Convert related point from one view size to another
    static func convertRemotePoint(point: CGPoint, fromSize: CGSize, toSize: CGSize) -> (CGPoint, Bool) {
        let remoteViewSize = fromSize
        let localViewSize = toSize
        guard remoteViewSize.width > 0 && remoteViewSize.width > 0 && localViewSize.width > 0 && localViewSize.height > 0 else { return (point, false) }
        
        let k1 = remoteViewSize.width / remoteViewSize.height
        let k2 = localViewSize.width / localViewSize.height
        if k1 > k2 {
            let localHeightFitIntoRemoteView = remoteViewSize.width / k2 // it's > remoteViewSize.height
            let remotePointYInRemoteView = remoteViewSize.height * point.x // y! // this is original Y of the point
            let yShift = (remoteViewSize.height - localHeightFitIntoRemoteView) / 2 // < 0
            let remotePointYShifted = remotePointYInRemoteView - yShift
            let relatedPointInLocalView = CGPoint(x: remotePointYShifted / localHeightFitIntoRemoteView, y: point.y)
            return (relatedPointInLocalView, true)
        }
        else if k1 < k2 {
            let localWidthFitIntoRemoteView = remoteViewSize.height * k2
            let remotePointXInRemoteView = remoteViewSize.width * point.y // x!
            let xShift = (remoteViewSize.width - localWidthFitIntoRemoteView) / 2 // <0
            let remotePointXShifted = remotePointXInRemoteView - xShift
            let relatedPointInLocalView = CGPoint(x: point.x, y: remotePointXShifted / localWidthFitIntoRemoteView)
            return (relatedPointInLocalView, false)
        }
        return (point, false)
    }
    
    /// Remove nodes that are close to given point
    /// - Parameters:
    ///   - point: the point
    ///   - distance2: the maximum distance to the node
    func removeNodes(at point: simd_float3, atDistance2 distance2: Float) -> [SCNNode] {
        var list = [SCNNode]()
        var listRemoved = [SCNNode]()
        for node in addedObjects {
            if node.simdPosition.distance2(to: point) <= distance2 {
                listRemoved.append(node)
                node.removeFromParentNode()
                node.isHidden = true
            }
            else {
                list.append(node)
            }
        }
        addedObjects = list
        return listRemoved
    }
    
    /// Add nodes
    func add(nodes: [SCNNode]) {
        for node in nodes {
            node.isHidden = false
            sceneView.scene.rootNode.addChildNode(node)
            addedObjects.append(node)
        }
    }
    
    // Remove nodes
    func remove(nodes: [SCNNode]) {
        for node in nodes {
            if let i = addedObjects.firstIndex(of: node) {
                addedObjects.remove(at: i)
                node.isHidden = true
                node.removeFromParentNode()
            }
        }
    }
}
