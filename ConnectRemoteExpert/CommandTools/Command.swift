//
//  Command.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 4/18/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

/// Command protocol
protocol Command {
    
    /// Initial application of command
    func apply()
    
    /// Undo the command
    func undo()
    
    /// Apply the command (redo)
    func redo()
}
