//
//  ToolSettings.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 7/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation

protocol ToolSettings {
    
    associatedtype Settings: ToolSettings
    
    func encode() -> String
    
    static func decode(_ string: String) -> Settings
    
    static func match(_ string: String) -> Bool
}

