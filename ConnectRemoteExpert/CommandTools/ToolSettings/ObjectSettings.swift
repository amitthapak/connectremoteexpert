//
//  ObjectSettings.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 7/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import CoreGraphics

struct ObjectSettings: ToolSettings {
    
    let objectIndex: Int
    let width: CGFloat
    let alpha: CGFloat
    
    typealias Settings = ObjectSettings
    
    func encode() -> String {
        return "object-index:\(objectIndex);object-width:\(width);object-alpha:\(alpha)"
    }
    
    static func decode(_ string: String) -> ObjectSettings {
        let a = string.split(separator: ";")
        var objectIndex: Int = 0
        var width: CGFloat = DrawingConstants.lineWidth
        var alpha: CGFloat = 1
        if let number = generalNumberFormatter.number(from: a[0].replacingOccurrences(of: "object-index:", with: "")) {
            objectIndex = number.intValue
        }
        if let number = generalNumberFormatter.number(from: a[1].replacingOccurrences(of: "object-width:", with: "")) {
            width = CGFloat(exactly: number) ?? DrawingConstants.lineWidth
        }
        if let number = generalNumberFormatter.number(from: a[2].replacingOccurrences(of: "object-alpha:", with: "")) {
            alpha = CGFloat(exactly: number) ?? 1
        }
        return ObjectSettings(objectIndex: objectIndex, width: width, alpha: alpha)
    }
    
    static func match(_ string: String) -> Bool {
        return string.contains("object-index:")
    }
}
