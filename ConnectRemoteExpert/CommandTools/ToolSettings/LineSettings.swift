//
//  LineSettings.swift
//  RemoteExpert
//
//  Created by Volkov Alexander on 7/17/20.
//  Copyright © 2020 Volkov Alexander. All rights reserved.
//

import Foundation
import CoreGraphics

struct LineSettings: ToolSettings {
    
    let width: CGFloat
    let alpha: CGFloat
    
    typealias Settings = LineSettings
    
    func encode() -> String {
        return "line-width:\(width);line-alpha:\(alpha)"
    }
    
    static func decode(_ string: String) -> LineSettings {
        let a = string.split(separator: ";")
        var lineWidth: CGFloat = DrawingConstants.lineWidth
        if let number = generalNumberFormatter.number(from: a[0].replacingOccurrences(of: "line-width:", with: "")) {
            lineWidth = CGFloat(exactly: number) ?? DrawingConstants.lineWidth
        }
        var lineColorAlpha: CGFloat = 1
        if let number = generalNumberFormatter.number(from: a[1].replacingOccurrences(of: "line-alpha:", with: "")) {
            lineColorAlpha = CGFloat(exactly: number) ?? 1
        }
        return LineSettings(width: lineWidth, alpha: lineColorAlpha)
    }
    
    static func match(_ string: String) -> Bool {
        return string.contains("line-width:")
    }
}


